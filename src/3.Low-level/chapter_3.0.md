# Las Antiguas Escrituras

*A medida que avanzas por aquel páramo sientes como tus pesadas cada vez se hacen más pesadas; no hay calor o frío, tampoco algo como el dolor o el cansancio, y aun así, sientes como tu misma existencia es cada vez más difícil de preservar, en lo más profundo de tu núcleo, percibes como si no estuvieras haciendo algo bien, como si no estuvieras cumpliendo con el propósito que esta escrito en lo más profundo de ti. De igual modo esta sensación de negligencia vino acompañado con un instinto, una guía que señalaba el Oeste, solo un poco apartado del camino que habías emprendido. Lo sigues sin titubeo. Si realmente tuvieras aliento lo estarías recuperando en este momento. Rápidamente llegaste a tu primer destino, no tienes una noción del tiempo, pero te pareció que no tomo nada llegar.* 

*Aquel templo en muchos sentidos era particular, no había una parte central visible que fuera más grande que las torres que ahí se erguían, de hecho, no existía edificación alguna que fuera más pequeño o emanara menor importancia. Todas eran iguales, simétricas, con los mismos privilegios y ninguna demasiado alejada de la otra. Las antiguas torres de piedra y hierro sin embargo, tenían un orden, una especie de aura que te indica cual de esas es la primera que debías visitar… Atraviesas el umbral, subes las desgastadas escaleras que a cada paso sueltan un chillido y terminas en la punta del edificio, en lo que parece un gran estudio. Sobre una mesa ya apunto de quebrarse, unas escrituras reposan, esperan ser leídas.*

*Comienzas a leer.*

## Generalidades

Notaras que este capitulo es bastante más largo, ¡pero no te asustes! Cubriremos muchos tópicos al mismo tiempo y eso requiere algo más de texto, algunos de estos conceptos quizás ya los sepas si estas aquí, pero nunca sobra estar seguros de que estamos hablando en los mismos términos y apuntando al mismo sitio. Hay una palabra que resuena fuertemente cuando hablamos de Holochain, y esa palabra es **descentralización**. Por supuesto, dicho concepto es quizás lo más atractivo y por lo que quieras estudiar este framework. No obstante, tenemos que entender el opuesto para así dilucidar porque estamos prefiriendo, y eso es la **centralización**. 

## Centralización

Para ser más exactos, lo más correcto es hablar del modelo cliente-servidor. El modo más común para organizar un sistema que se ejecute en sistemas distribuidos es separando las funciones en dos partes: clientes y servidor. El cliente es un programa que usa los servicios que otros programas proveen. Estos programas que proveen servicios por otra parte son llamados servidor. El cliente efectúa una solicitud por un servicio y el servidor lleva a cabo el servicio solicitado. Los programas cliente manejan la interacción del usuario, y, siendo esto lo más importantes, **gestionan la data, inicializa dicha data o la modifican para eventualmente ser solicitada por el servidor.**

Te doy unos minutos para comprender lo que esta mal aquí. 

<img src="./img/AntiguasEscrituras1.gif" />

¿Listo? Muy bien, digámoslo al mismo tiempo en

3...

2...

1...

**¿Si un servidor céntrico gestiona todas las peticiones, y por consiguiente, tú data, ¿donde queda tu privacidad y que te garantiza la seguridad de esa información si en ese servidor entra un intruso?**

En una entrevista le preguntaron a Mark Zuckerberg respecto a la privacidad, específicamente sobre si Facebook pudiera compartir la data de sus usuarios con quien fuera, él lo dejo muy claro con una breve respuesta: “Técnicamente alguien podría hacer eso...”. Asi que, ¿qué pasa cuando no queremos confiar en nadie? ¿Qué pasa cuando necesitamos tener la completa garantía de que nuestra data esta segura y no controlada por un ente, en este caso el servidor, con privilegios especiales en lo que respecta nuestra información?

Ahí es donde entra la descentralización. 

## Primera Escritura

*Lees sobre un mundo que te es completamente ajeno, un mundo donde ya no existe el yo mismo y todo ha sido consumido por El-Gran-Receptor, aquel que todo lo sabe y todo lo controla. Este ser no solo se ha apoderado de lo tangible, sino también de lo no tangible, cada pequeño tercio de información tiene que pasar por cada solicitud, cada voluntad, todo tiene que ser registrado por el para ser llevado a cabo. Su gran telaraña se extiende por lo largo y ancho del continente e incluso más allá. También aprendes sobre una metrópolis; La Gran Central, lugar donde reside el corazón de  El-Gran-Receptor. Sientes como tu esencia misma te comanda ir a ese lugar, pero aun sigue señalando con más intensidad los escritos en tus manos.*

*Continuas leyendo.*

## Descentralización

Cuando hablamos de la descentralización, hablamos directamente de las aplicaciones peer-to-peer. Aunque esto lo tocaremos en mayor profundidad en la sección teórica del framework. Aunque yendo al grano, Peer-to-peer (P2P) es una arquitectura de aplicaciones distribuidas que particiona las tareas y los trabajos de carga entre peers. Los peers (compañeros llevados al español) tienen exactamente los mismos privilegios, equipotencial a los participantes en la aplicación. Cuando esto sucede se dice que están formando una red peer-to-peer de nodos. 

Los peers hace una porción de los recursos, como potencia de procesamiento, almacenamiento de disco o red de banda ancha, completa y directamente disponible para otros participantes de la red sin la necesidad de una central de coordinaron por servidores o host estables. No conforme con esto, y siendo a lo que Holochain apunta, los sistemas P2P emergentes están yendo más allá de la era de los peers haciendo cosas similares mientras comparten recursos, en su lugar, existen diversos peers que pueden brindar únicos recursos o capacidad hacia una comunidad virtual, y que permitirían lograr grandes tareas que por encima de las que podrían ser cumplidas por peers individuales. Todo esto de la mano con un beneficio mutuo, claro esta. 

## Segunda Escritura

*A medida que pasas las paginas El-Gran-Receptor pierde protagonismo y la narración se cierne sobre un héroe de leyenda, una fuerza opuesta a la centralización del señor de este continente olvidado por el pensamiento independiente. Las escrituras hacen referencia a esta personalidad como El Héroe de la Descentralización, defensor de la libertad y la equidad; portador de Holochain, un arma capaz de crear tecnología y recursos capaces de crecer y desarrollarse con la fuerza de aquellos que residen en el mundo. Sin embargo, esta arma y su paradero es desconocida hasta para El-Gran-Receptor. Los escritos te cuentan que Holochain surgirá cuando más necesaria sea.* 

*No dudas en ir a la siguiente pagina.* 

## Lenguajes de Bajo Nivel

En esta guía no usaremos un lenguaje de bajo nivel como Assembly, sin embargo, usaremos un lenguaje de programación con característica de un lenguaje de bajo nivel como C o C++, este es el caso de Rust, el lenguaje de Holochain. Los lenguajes de bajo nivel son aquellos que estan extremadamente cercas del lenguaje maquina. Estos corren directamente en los procesadores y son extremadamente específicos con su arquitectura. Los lenguajes de bajo nivel son más apropiados para desarrollar nuevos sistemas operativos o para escribir códigos firmware para los micro-controladores. 

Vale, quizás esto ultimo fue demasiado técnico, con lo que te tienes que quedar es que C, C++ y Rust tienen un increíble manejo de memoria, son veloces y tienen la mejor optimización. 

## Rust

Rust es un lenguaje de programación desarrollado por los ingenieros de mozilla, compilado al igual que C o C++ y con el objetivo principal de garantizar no solo un lenguaje potente como es el caso de C, sino también muy seguro como Java como ejemplo más destacado. Rust esta diseñado para desarrollar software de sistemas, donde la interacción con el usuario es prácticamente nula; Esto lo hace excelente para aplicaciones con el modelo cliente-servidor o para necesidades más internas en organizaciones o empresas. 

Ademas, al no poseer un recolector de basura, los desarrolladores deben permanecer constantemente atentos al uso de la memoria.  De esta forma las aplicaciones producto del lenguaje Rust maximizan la eficiencia al usar los recursos de la máquina. El lenguaje dispone de muchas características propias de un lenguaje de bajo nivel, permitiendo trabajar directamente con el procesador, asignando todas las tareas. Todo dentro de un ecosistema seguro empleando Threads, sincronización y acceso a datos concurrentes. 

## Tercera Escritura 

*La resma de hojas esta apunto de acabarse cuando lees sobre la magia de El Héroe de la Descentralización, las artes de Rust, una disciplina arcana muy exigente con sus practicantes. Es sabido que su gran poder viene de la mano con lo meticuloso que es respecto a como se llevan a cabo sus ordenes, sin embargo, el héroe de la leyenda cuenta con Cargo, un asistente que no solo lo instruirá en el uso de Rust, sino también le dará consejo de como llevar a cabo de la mejor manera sus encantamientos.  Una vez pasas a la siguiente pagina todo se vuelve muy críptico, tratas de entender algo de esos garabatos, pero fallas completamente.*

*En ese momento escuchas algo.*