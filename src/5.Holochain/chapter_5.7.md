# Desarrollando un GUI

Bienvenido al acercamiento de cómo implementar Holochain a nuestro GUI. Hasta ahora, hemos interactuado con el zome usando `curl` o `hc test`, pero eso no es tan bueno como tener una GUI. Aprenderemos a interactuar con una aplicación Holochain utilizando una página web simple. Con una conexión WebSocket, los datos se pasarán desde y hacia una página web JavaScript / HTML.

Con toda certeza deseas escribir una GUI para tus futuras aplicaciones, por lo que es útil ver cómo conectar un front-end a su back-end zome. Esta no es la única forma de escribir una GUI para una aplicación Holochain, pero debería resultarle familiar si estás acostumbrado a las interfaces web.
Crearemos una carpeta para la GUI

```
  cd ~/holochain/coreconcepts
mkdir gui
cd gui
```

Crearemos un nuevo archivo llamado `index.html` en el editor de preferencia. Preferiblemente en `gui/index.html`. Entonces comienza agregando una plantilla HTML simple a `index.html`.

```html
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />

    <title>Hello GUI</title>
    <meta name="description" content="GUI for a Holochain app" />
  </head>

  <body>
  </body>
</html>
```

Para facilitar las cosas a la vista, puedes agregar la hoja de estilos water.css.
Agrega este enlace a water.css dentro de la etiqueta `<head>`:

```html
   <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.min.css"
    />
```

```html
 <button onclick="hello()" type="button">Say Hello Power</button>
```

## hc-web-client

Es hora de comunicarse con la aplicación que creamos en tutoriales anteriores. Para hacerlo más fácil, puedes utilizar hc-web-client. Es la biblioteca JavaScript de Holochain que le ayuda a configurar fácilmente una conexión WebSocket a tu aplicación. Tener una conexión WebSocket abierta permite que la aplicación envíe mensajes a nuestro GUI. Si bien no tocaremos eso por ahora, es bueno familiarizarse con el proceso.

Para que este proceso sea fácil, ya existe un versión compilada previamente de hc-web-client para que podamos saltar a la acción.

Descárgalo en la raíz del directorio GUI y descomprimelo.

```
  cd gui
curl https://developer.holochain.org/assets/files/hc-web-client-0.5.3.zip > hc-web-client-0.5.3.zip
unzip hc-web-client-0.5.3.zip
rm hc-web-client-0.5.3.zip
```

Los archivos deberíamos encontrar son:

```
gui/hc-web-client-0.5.3/hc-web-client-0.5.3.browser.min.js
gui/hc-web-client-0.5.3/hc-web-client-0.5.3.browser.min.js.map
```

Una vez hecho esto, puedes vincular fácilmente al archivo js compilado agregando la etiqueta `script` dentro de las etiquetas de nuestro `body`.

```html
   <script
      type="text/javascript"
      src="hc-web-client-0.5.3/hc-web-client-0.5.3.browser.min.js"
    ></script>
``` 

## Llamar la función zome

Ahora que hemos vinculado la biblioteca hc-web-client.js, podemos realizar una simple llamada al zome con un poco de JavaScript vanilla. 

Agregaremos lo siguiente dentro de la etiqueta `body`.

```html
   <script type="text/javascript" src="hello.js"></script>
```

Hasta el momento deberíamos llevar esto:

```html
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />

    <title>Hello GUI</title>
    <meta name="description" content="GUI for a Holochain app" />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.min.css"
    />
  </head>

  <body>
  <button onclick="hello()" type="button">Say Hello</button>
    <script
      type="text/javascript"
      src="hc-web-client-0.5.3/hc-web-client-0.5.3.browser.min.js"
    ></script>
    <script type="text/javascript" src="hello.js"></script>
  </body>
</html>
``` 

## Llamamiento del Zome

La función hello se conectará a nuestra aplicación a través de WebSocket, llamará a la función hello zome e imprimirá el resultado en la consola del navegador.
Crearemos un archivo en el mismo directorio que index.html llamado hello.js.
Agregaremos el siguiente código a este archivo que establecerá una conexión de WebSocket a Holochain:

```js
var holochain_connection = holochainclient.connect();
```

Agregue una función de JavaScript hello () para que podamos llamarla desde el HTML:

```js
function hello() {
```

Esperaremos a que Holochain se conecte y luego haga una llamada al zome:

```js
  holochain_connection.then(({callZome, close}) => {
```
Invocaremos el resultado en la consola del navegador: 
```js
   )({args: {}}).then(result => console.log(result));
  });
}
``` 

En su totalidad, el código debería lucir del siguiente modo:

```js
var holochain_connection = holochainclient.connect();
function hello() {
  holochain_connection.then(({callZome, close}) => {
    callZome(
      'test-instance',
      'hello',
      'hello_holo',
    )({args: {}}).then(result => console.log(result));
  });
}
```  

## Configurar un archivo de paquete

El Holochain CLI `hc` puede ejecutar el conductor, nuestra hApp y su GUI. Todo lo que necesitamos hacer es configurar un archivo de paquete para especificar dónde están las cosas.

Crearemos un nuevo archivo en la carpeta raíz `cc_tuts /` de nuestro hApp llamado `bundle.toml` y agregaremos las siguientes líneas.

```toml
bridges = []
```

No hay puentes (conexiones entre zomes separados) en nuestra hApp, por lo que está vacío.

```toml
[[instances]]
name = "cc_tuts"
id = "__cc_tuts"
dna_hash = "QmdATqyBReFQh2bV8J3mPajRKSQ7CZpkZ2S251ypxoAbH6"
uri = "file:dist/cc_tuts.dna.json"
```

Esta es la configuración del GUI. Apunta a la carpeta raíz de nuestro GUI. Si has seguido todos los pasos al pie de la letra, debería encontrarse un nivel arriba (../) y en una carpeta llamada `gui`.

```toml
[[UIs]]
name = "CC Tuts"
id = "cc_tuts_ui"
uri = "dir:../gui"
```

Esto vincula el GUI con el DNA.

```toml
[[UIs.instance_references]]
ui_handle = "test-instance"
instance_id = "__cc_tuts"
```

Juntandolo todo, el código acabaria asi: 

```toml
bridges = []

[[instances]]
name = "cc_tuts"
id = "__cc_tuts"
dna_hash = "QmdATqyBReFQh2bV8J3mPajRKSQ7CZpkZ2S251ypxoAbH6"
uri = "file:dist/cc_tuts.dna.json"

[[UIs]]
name = "CC Tuts"
id = "cc_tuts_ui"
uri = "dir:../gui"

[[UIs.instance_references]]
ui_handle = "test-instance"
instance_id = "__cc_tuts"
}
```

## Ejecutar el paquete

Asegúrate de estar en el directorio `cc_tuts /` de nuestro hApp y no en el directorio GUI. Volvemos a nix-shell compilamos y ejecutamos el hApp.

Compilar la aplicación.

```
hc package
```

Correr la aplicación. 

```
hc run
```

## Ejecutar Llamamiento

Abriremos el navegador y nos dirigiremos a `127.0.0.1:8888`
La consola de desarrollador nos debería decir esto: 

<img src="./img/GUI1.png" />

¡Oh! ¡Lo hemos logrado! Estamos elaborando una aplicación web con nada menos que Holochain. Excelente trabajo.

## Renderizar la salida

Sería mejor ver el resultado de la llamada `hello_holo` en la página, así que agreguemos algún lugar para mostrarlo.

```html
   <div>Response: <span id="output"></span></div>
``` 

El `id = "output"` es lo que usaremos para actualizar este elemento desde una función de JavaScript.

## Agregando la función para mostrar el output

De vuelta en el archivo `hello.js`, agregaremos las siguientes líneas debajo de la función de saludo.

```js
function show_output(result) {
  ```

Obtenemos el elemento en el que vamos a insertar la salida:

```js
 var span = document.getElementById('output');
```

Convertimos el resultado de la función zome a JSON.

```js
 var output = JSON.parse(result);
```

Establecemos el contenido del elemento en el resultado de la función zome:

```js
 span.textContent = output.Ok;
}
``` 

Finalmente, actualizamos la función hello para llamar la nueva función `show_output` en lugar de console.log().

```js
-    )({args: {}}).then(result => console.log(result));
+    )({args: {}}).then(result => show_output(result));
```

Junto, seria asi: 

```js
var holochain_connection = holochainclient.connect();
function hello() {
  holochain_connection.then(({callZome, close}) => {
    callZome(
      'test-instance',
      'hello',
      'hello_holo',
    )({args: {}}).then(result => show_output(result));
  });
}
function show_output(result) {
  var span = document.getElementById('output');
  var output = JSON.parse(result);
  span.textContent = output.Ok;
}
``` 

Nuevamente lo compilamos y lo ejecutamos para atisbar cualquier error. 

## Comprobamos que el output funcione

Dirígete a `127.0.0.1:8888` en tu navegador web (es posible que debas actualizar) y deberías ver esto:

<img src="./img/GUI2.png" />

Ahora, presione el botón **Say Hello**: 

<img src="./img/GUI3.png" />

¡Felicidades héroe! Ahora no solo tienes la comprensión de que caracteriza a una hApp, sino que eres capaz de crear GUI que se comunique con tu codigo Holochain. 

## En Resumen

- Puedes usar interfaces web regulares para conectarte a un conductor a través de WebSocket.
- La interfaz web más simple requiere JavaScript y HTML.
- Las funciones Zome se pueden llamar desde la GUI de manera similar a curl.




