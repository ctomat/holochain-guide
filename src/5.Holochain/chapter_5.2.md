# La cadena de origen: un almacén de datos personales

Te haré una promesa, una vez terminemos con esta parte pasaremos nuevamente a programar, pero tienes que quedarte hasta el final. El estudio teórico sin demostraciones prácticas de poco nos servirá, por lo que esta es la última lección enteramente teórica que veremos de Holochain. Cada vez estamos más cerca del final y solo puedo estar orgulloso de que hayas llegado hasta aquí. 

Cada usuario en la red de Holochain crea y almacena su propia data en una especie de diario llamado source chain o cadena de origen. Cada entrada de este diario posee una firma criptográfica por su autor y es inmutable una vez escrita. Cuando entendemos cómo los agentes y su respectiva data son representados, ya contamos con un conocimientos fundamentales para crear una experiencia de usuario apropiada que toma ventaja del diseño de agente céntrico. 

Echemos un vistazo a los nodos de forma individual y dilucidar qué ocurre desde la perspectiva del usuario. 
Volviendo a lo básico, dijimos que uno de los pilares de Holochain en la integridad intrínseca de la data. La primera capa de este pilar es la llave pública criptográfica, que permite al usuario crear y autenticar su propio identificador sin una base de datos de contraseñas central. Si alguna vez haz usado SSH, lo entenderás rápidamente. 

Cuando nos unimos a la red de una hApp, podemos crear un identificador para nosotros generando un par de claves públicas y privadas. Este par de llaves nos permitirá hacer unas cuantas cosas: 

- Identificarnos
- Probar que eres el autor de tu data. 
- Permita que otros detecten la manipulación de tus datos por parte de terceros.
- Ver datos destinados solo a tus ojos
- Tu conductor generada y almacenará todas tus claves en un archivo cifrado protegido con contraseña. 

Estudiemos primero las llaves privadas y sus funciones. Sabemos que estas llaves son usadas para: 

- Mantenerse en secreto dentro de tu dispositivo.
- Actúa como una contraseña, solo tú la tiene y es necesaria para probar la propiedad que tienes de tus llaves públicas. 
- Actúa también como sello real, crea firmas digitales imposibles de falsificar y a prueba de manipulaciones en tus datos.
- Actúa como una clave de buzón: abre mensajes sellados con tu clave pública.

Por otra parte, las llaves públicas cumplen una serie de funciones distintas:

- Compartir con todos los nodos (pares) en tu red. 
- Actúa como una identificación para ser reconocible por los otros usuarios. 
- Permite a otros verificar la integridad de tus firmas. 
- Actúa como una ranura de correo: permite que otros cifren y envíen datos solo para tu persona.

La siguiente capa de este pilar es un diario cronológico de cada pieza de la data del usuario que has creado. Conservando los permisos de escritura solo para la autoridad de ese usuario; existe en el dispositivo y cada entrada debe ser firmada por su llave privada correspondiente. Como te imaginarás, este es la cadena de origen (source chain) porque cada parte de la data dentro de una aplicacion Holochain comienza ahí. 

Este diario comienza con dos entradas especiales del sistema llamadas entradas "génesis":

1. El hash del DNA. El DNA constituye las “reglas de juego” para todos en la aplicación, esta entrada muestra que quien entre a la red, ha visto y acepta cumplir esas reglas.
2. La identificación de agente. Contiene la clave pública como registro de tu identidad digital. Las firmas en todas las entradas posteriores deben coincidir con esta clave pública para que sean válidas. Esta entrada además puede contener información extra necesaria para poder acceder a la red; algo como una invitación o unas cuotas pagas.  

Después prosiguen las entradas de aplicaciones o datos de usuario. Como desarrolladores, somos aquellos que definen el formato y las reglas de validación por cada tipo de aplicación en nuestro DNA con que tiene que lidiar. Una entrada realmente puede contener cualquier cosa que pueda ser almacenado en un string, pero la mayoría de las veces será mejor darle estructura usando JSON. Nuestro SDK le brinda las herramientas para convertir automáticamente de estructuras de datos Rust a JSON y viceversa.

>JSON (JavaScript Object Notation - Notación de Objetos de JavaScript) es un formato ligero de intercambio de datos. Leerlo y escribirlo es simple para humanos, mientras que para las máquinas es simple interpretarlo y generarlo.

Bueno, quizás, solo quizás, es posible que aparezcan otros tipos especiales de entrada del sistema, pero los veremos más adelante. Por el momento guarda el secreto hasta que lleguemos ahí. 

Una entrada en tu source chain no debería ser modificada una vez que se haya confirmado. Esto es importante, porque tu cadena es un registro de todas las cosas que han sucedido dentro de la aplicación, y es posible que los pares (otros nodos que representan otros usuarios) deban verificar este registro para validar una entrada. En el caso de que la integridad de nuestra data sea vital, ¿qué pasaría si terceros tratarán de sabotearla? ¿qué sucedería si trataran de husmear las cartas de amor que te han escrito las doncellas, o las propuestas de negocio que seguro deben abundar?

La respuesta corta es que no mucho. 

1. Cuando el DNA quiere crear una entrada para ti, ante todo valida su contenido acorde a las reglas definidas para su tipo. Esto asegura la producción accidental de mala data. 
2. Solo entonces le solicita a tu conductor firmar la entrada con tu llave privada. 
Entonces el conductor agrega la firma a un encabezado y la adjunta a la entrada.
3. Por último, guarda la entrada como el siguiente elemento en su cadena de origen.

Como una firma escrita en tinta, la firma garantiza que fuiste tu quien creó la entrada y nadie más. Se basa en matemáticas complejas, por lo que es verificable es imposible de falsificar. También es válido solo para el contenido de esa entrada; si un tercero modifica incluso un solo carácter, la firma se rompe. 

Esto nos permite detectar ataques de intermediario en los datos de entrada, pero aún no nos dice si alguien ha alterado el orden de las entradas en el source chain.

Echemos un vistazo más de cerca al encabezado. Junto con la firma, incluye el hash del encabezado anterior, una marca de tiempo y el tipo de entrada.

<img src="https://i.imgur.com/3AOXfVf.png" alt="drawing" width="800"/>

Miremos un poco más cerca, específicamente a la primera línea del encabezado. Esto es lo que asegura la integridad del source chain en su totalidad. Apunta al encabezado de la entrada anterior, que apunta al encabezado de la entrada anterior a ese, y así sucesivamente. Con un diario de papel, es obvio cuando alguien arranca una página, pega una página nueva o pega una hoja de papel sobre una página existente. Esta cadena de hash de encabezado es el equivalente digital.

Es completamente justo preguntarse: ¿pero qué pasa si soy yo quien quiere modificar algo? Siguiendo la teoría planteada, con tu llave privada deberías ser capaz de desechar entradas indeseadas, por tanto, también deberías ser capaz de crear nuevas entradas. Y sí, podrías, para algunas aplicaciones, esto no importaría mucho, pero se vuelve bastante serio cuando el diario contiene cosas como transacciones financieras o papeletas. La respuesta de Holochain a esto es todavía más concisa: alguien lo notara.

## Oscuridad

*Te encuentras en la zona de entrenamiento rodeado por energía en forma de lazos entrelazados. Te sorprendió la composición de todo; se resumía en partes pequeñas que juntas hacían algo más grande. La magia que usabas en este momento tenía una identidad indiscutible que completa sabes que se llama DNA, el mismo núcleo de tu poder. Los glifos de aquellos sortilegios se conocían como Zomes, y son fragmentos que contenían la información de lo que querías que hiciera cada hechizo. Todo fluía a través de los conductores que creabas, aquello que mantenía funcionando tus programas.*

*«Es… increíble.»*

*Cargo brillaba de interés. Tú también lo harías si pudieras. Sostienes tu arma con tus dos armas. Percibes su integridad, es una espada sólida compuesta por varias capas. Con solo verla aprendes cómo funciona, cuál es el filo y cómo empuñarla. Incluso podrías asegurar que sabrias como replicarla. Entiendes una parte importante de tu poder; compartirlo. Te interrumpe la risa de Mozilla.*

*─ Gran avance, si sigues asi, pronto…*

*Y entonces todo se estremeció. La pared se derrumba y abre paso a los esbirros de El-Que-Todo-Lo-Controla. Te pones en guardia pero notas que son demasiados, muchos más que los que los que enfrentaste en el templo. Mozilla se pone delante de ti interponiéndose en el camino de los esbirros.*

*─ ¡Ve por la rendija! Es la vía de emergencia.*

*Sin poder decir más, se dispuso a combatir las fuerzas de la centralización. Cargo con un agarre invisible te jala hacia donde indico la bruja. Te resistes al principio pero algo te detiene. Una idea. Eres el único que sabe de Holochain, eres el único que puede enseñarlo. No puedes arriesgarte aqui.Ya no. Dolido, dejas que Cargo te lleve hacia la ruta de escape. Abres la escotilla y comienzas a bajar por la escalerilla que ahí había. Antes de tocar el suelo, algo te sacude y te lanza al suelo.*

*Quedas a oscuras.*


## En resumen

- Las aplicaciones de Holochain no utilizan inicios de sesión ni bases de datos de contraseñas.
- Los usuarios crean sus propios identificadores digitales como pares de claves públicas / privadas criptográficas. Estas dos claves juntas demuestran que poseen su identidad en línea.
- Los usuarios comparten sus claves públicas con otros participantes en la misma aplicación.
- Los usuarios prueban la autoría de sus datos mediante firmas digitales imposibles de falsificar creadas con tu clave privada. La manipulación de datos de terceros se detecta utilizando la clave pública del autor para verificar la firma.






