# Testeando el Poder

Es hora de desarrollar un test para la aplicación, esto termina siendo imprescindible dentro de Holochain y para asegurar la alta calidad de nuestro producto. Como es natural, continuaremos con la práctica pasada, por lo tanto es recomendable que la hagas antes. Aprenderemos a usar las pruebas de Tryorama para poner a prueba nuestra aplicación.

## Entendiendo el Test

Cuando corrimos `hc init` en la práctica anterior, Holochain género unos tests para nosotros. Estos tests están escritos en Javascript junto al framework de testeo incorporado con Holochain, Tryorama y una prueba de estrés llamada Tape. Tryorama nos permite escribir suites de prueba sobre el comportamiento de múltiples nodos Holochain que están conectados en red, al tiempo que garantiza que los nodos de prueba en diferentes pruebas no se unen accidentalmente a una red. 
Abriremos el `cc_tuts/test/index.js` en nuestro editor de código. Podrás apreciar que este archivo es bastante menos intimidante que el zome.  

```
/// NB: The tryorama config patterns are still not quite stabilized.
/// See the tryorama README [https://github.com/holochain/tryorama]
/// for a potentially more accurate example

const path = require('path');

const {
  Orchestrator,
  Config,
  combine,
  singleConductor,
  localOnly,
  tapeExecutor,
} = require('@holochain/tryorama');
``` 

Este es un registrador de errores general que le permitirá saber si una Promesa falla y no hay un controlador de errores para detectarla. Las promesas son una forma de simplificar el código asincrónico complejo y Tryorama usa muchas de ellas. A medida que trabajemos se comprenderá mejor su concepto. 

```
process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.error('got unhandledRejection:', error);
});
```

La ruta del DNA compilado:

```
const dnaPath = path.join(__dirname, '../dist/cc_tuts.dna.json');
```

Configura un escenario de prueba. Esto crea dos agentes: Alice y Bob.

```
const orchestrator = new Orchestrator({
  middleware: combine(
    // use the tape harness to run the tests, injects the tape API into each scenario
    // as the second argument
    tapeExecutor(require('tape')),

    // specify that all "players" in the test are on the local machine, rather than
    // on remote machines
    localOnly,

    // squash all instances from all conductors down into a single conductor,
    // for in-memory testing purposes.
    // Remove this middleware for other "real" network types which can actually
    // send messages across conductors
    singleConductor,
  ),
});

const dna = Config.dna(dnaPath, 'scaffold-test');
const conductorConfig = Config.gen({myInstanceName: dna});
``` 

Elimina el singleConductor ya que estamos haciendo pruebas reales con una red:

```
const orchestrator = new Orchestrator({
  middleware: combine(
    // use the tape harness to run the tests, injects the tape API into each scenario
    // as the second argument
    tapeExecutor(require('tape')),

    // specify that all "players" in the test are on the local machine, rather than
    // on remote machines
    localOnly,
-
-    // squash all instances from all conductors down into a single conductor,
-    // for in-memory testing purposes.
-    // Remove this middleware for other "real" network types which can actually
-    // send messages across conductors
-    singleConductor,
  ),
});

``` 

A lo largo de esta segunda práctica, usaremos sim2h cómo nuestra red porque será la configuración más útil para la mayoría de los desarrolladores. Agregaremos la red sim2h a la prueba.


```
const dna = Config.dna(dnaPath, 'cc_tuts');
const config = Config.gen(
{
    cc_tuts: dna,
},
{
  network: {
   type: 'sim2h',
   sim2h_url: 'ws://localhost:9000',
  },
 },
);

```
Esta es la prueba que Holochain generó basándose en la estructura my_entry y las funciones zome que trabajan con ella. Los eliminamos en nuestra práctica anterior, así que eliminaremos la prueba también.

Quitaremos la siguiente sección:
```

-orchestrator.registerScenario('description of example test', async (s, t) => {
-  const {alice, bob} = await s.players(
-    {alice: conductorConfig, bob: conductorConfig},
-    true,
-  );
-
-  // Make a call to a Zome function
-  // indicating the function, and passing it an input
-  const addr = await alice.call(
-    'myInstanceName',
-    'my_zome',
-    'create_my_entry',
-    {entry: {content: 'sample content'}},
-  );
-
-  // Wait for all network activity to settle
-  await s.consistency();
-
-  const result = await bob.call('myInstanceName', 'my_zome', 'get_my_entry', {
-    address: addr.Ok,
-  });
-
-  // check for equality of the actual and expected results
-  t.deepEqual(result, {
-    Ok: {App: ['my_entry', '{"content":"sample content"}']},
-  });
-});
```

## Creando escenarios de prueba

Las pruebas se organizan mediante la creación de escenarios. Piensa en ellos como una serie de acciones que el usuario, o grupo de usuarios, realiza al interactuar con nuestra aplicación. Para esta prueba, simplemente deseamos que el usuario de Alice llame a la función `power_words` zome y verifique que obtiene el resultado `Power!`. Nos dirigiremos a la línea posterior donde se ubica `orchestrator.run() y comenzaremos registrando un escenario de pruebas para comprobar que `power_words()` retorne el valor correcto.

```
 orchestrator.registerScenario('Test power_words', async (s, t) => {
```

Creamos los agentes Alice y Bob (Usaremos a Bob luego): 

```
  const {alice, bob} = await s.players({alice: config, bob: config}, true);
```

Haremos un llamado a la función del zome `power_words` sin pasar argumento alguno. 

```
  const result = await alice.call('cc_tuts', 'hello', 'power_words', {});
``` 

Asegurate que el resultado esté bien

``` 
  t.ok(result.Ok);
``` 

Comprueba que el resultado concuerde con los esperado:

```
 t.deepEqual(result, {Ok: 'Power!'});
});
```

Y esta última línea ejecutará la prueba que configuramos.

```
orchestrator.run();
```

## Ejecutar sim2h

Deberemos ejecutar el servidor sim2h localmente antes de poder ejecutar las pruebas. Esta es la centralita que realiza el enrutamiento y eventualmente será innecesaria, pero actualmente es útil para el desarrollo. Para ejecutar el servidor, abriremos un nuevo nix-shell en una terminal diferente y dispondremos de este comando

```
sim2h_server
```

## Ejecutar la prueba

Dentro del directorio de nuestro proyecto, correremos el test de este modo: 

```
  hc test
```

Esto compilara y ejecutara el escenario de prueba que escribimos. Veremos un inmenso output en la terminal, sin embargo, lo que nos interesa son las últimas líneas. 

```
# tests 2
# pass  2

# ok
```

## Determinacion

*El poder provoca una explosion de luz que te baña y te hace cada vez más fuerte. Admiras toda tu armadura que es casi como una bengala. Cargo siempre a tu lado desde casi el primer momento luce opaco en comparación a ti. Tu percepción amplificada te permite ver más allá de lo que esconde la oscuridad. Atisbas como unos secuaces de El-Que-Todo-Lo-Controla se acercan a tu posición, pero no puedes perder tiempo con ellos.*

*Te elevas atravesando el techo como si fueras inmaterial hasta volver a la metrópolis. Conectado y a la vez independiente, siendo un nodo, una parte de algo más grande. Miras hacia la torre más alta que a la vez es el centro de la ciudad y te diriges ahí.*

*«Oh, oh, oh, está sucediendo, no puedo creer que está sucediendo pero lo está haciendo.»*

*Le asientes con una sonrisa.*

*«¿Estás seguro de que estás listo? Aún falta mucho por aprender… no, supongo que es verdad, no puedes aprenderlo todo antes de confrontarlo, es imposible.»*

*Era la hora. Te estrellas con la planta más alta de la torre.* 


