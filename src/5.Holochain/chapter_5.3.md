# ¡Libera tu Poder!

## Despiertas

*Despiertas*.

*La oscuridad te imbuía pero no era lo bastante densa para que no pudieras ver nada en absoluto. Miras en dirección a la escotilla por donde escapaste. Ahí, piedra y metal habían colapsado bloqueandolo completamente. De repente, Cargo se manifestó, iluminando tu alrededor, permitiendo atisbar más allá. Una serie de túneles se extendían hasta más allá de lo que el fulgor de Cargo jamás podría alcanzar.*

*«Tenemos que seguir moviéndonos, héroe. Si nos quedamos aquí eventualmente ellos bajarán aquí.»*

*Lo entiendes, asientes más para ti que para Cargo.*

*«Mozilla tenía razón, mientras estemos aquí Él tendrá que limitarse a no hacer mucho revuelo. Nos alejaremos de aquí y entonces podrás seguir preparándote.»*

## Lo que nos atañe

Es por fin la hora de liberar todo tu poder héroe, este es el verdadero primer pasado para cumplir tu destino. Haremos el clásico Hola Mundo para tomar contacto e inmediatamente después hablaremos de los test, una característica particularmente esencial del framework. Con todo lo que hemos aprendido hasta este momento, te resultará fácil crear aplicaciones distribuidas con Holochain. 

Aprenderemos a como crear un zome de Holochain con una función invocable, compilarlo en un ADN y ejecutarlo en el conductor de Holochain. Una que se esté ejecutando, veremos como llamar una función zome usando curl. Esto nos ayudará a orientar los conceptos básicos de las hApps. Estamos hablando de los fundamentos sobre los que se desarrollará más adelante, así que es importante tener una comprensión clara y sólida.

## Configuración

Antes de comenzar es importante que comprobemos que todo en nuestro entorno está preparado.

1. Si no has completado la instalación de Holochain, héroe, es buen momento para hacerlo. Esto no proveerá de lo que necesitamos para desarrollar la aplicación, el entorno incluye la herramienta de desarrollo Holochain `hc`. 
2. Abre la terminal o consola (si estás usando Windows 10, asegurate de abrir la terminal de Ubuntu, no la línea de comandos o el Powershell)
3. Y por supuesto, necesitamos entrar en el entorno de desarrollo en cuestión. Recordarás este comando de la lección donde instalamos Holochain. 

## Iniciemos una nueva aplicación

Crearemos una nueva carpeta en donde almacenaremos todas nuestras futuras aplicaciones de Holochain. Bastará con `~/holochain/` Then, create a coreconcepts folder for this tutorial series:

```
cd ~/holochain
mkdir coreconcepts
cd coreconcepts
```

¡Muy bien! Es nuestra hora de abrazar el poder e ingresar nuestra línea de comando de Holochain. Iniciaremos nuestro aplicación dentro de nuestra carpeta `coreconcepts`

```
hc init cc_tuts
cd cc_tuts
```

### Compilado 

>Todo comando `hc` y `holochain` debería ser ejecutado desde la raíz del proyecto (por ejemplo `cc_tuts/`), excepto naturalmente cuando usamos `hc init`, porque la raíz no existe en ese punto. 

Siempre es bueno compilar con frecuencia tu aplicación y atisbar cualquier error desde etapas tempranas. Usaremos el siguiente comando: 

```hc package```
Empaquetar tu aplicación significa que estás compilando el código en un archivo DNA y tenerlo listo para ser corrido. 

```
Created DNA package file at "/Users/username/holochain/testing_tuts/hello_holo/dist/hello_holo.dna.json"
DNA hash: QmY7rhg4sf6xqQMRL1u1CnXVgmamTfxC59c9RaoFqM2eRs
```

Si todo ha resultado correctamente, deberías ver algo así en tu terminal. 

## Generar un Zome

La aplicación en este momento no hace mucho realmente, esto es porque necesitamos un zome. Recordemos que un zome es la forma de Holochain de organizar código dentro de efectivas unidades que llevan a cabo tareas. En este ejemplo queremos imprimir un string, así que haremos eso. 
Generaremos un zome llamado `hello` dentro de la carpeta que le corresponde y posteriormente lo compilaremos. 

```
hc generate zomes/hello rust-proc
hc package
```

> La primera vez que generamos un zome y dependiendo de las prestaciones de nuestro ordenador, es común que tome bastante tiempo. La siguiente vez será mucho más rápido, pero de momento, mientras esperas siéntete libre de seguir leyendo.
```

> cargo build --release --target=wasm32-unknown-unknown --target-dir=target
   Compiling hello v0.1.0 (/Users/username/holochain/coreconcepts/hello_hollo/zomes/hello/code)
    Finished release [optimized] target(s) in 11.95s
> cargo build --release --target=wasm32-unknown-unknown --target-dir=target
    Finished release [optimized] target(s) in 0.50s
Created DNA package file at "/Users/username/holochain/coreconcepts/hello_hollo/dist/hello_hollo.dna.json"
DNA hash: QmTVcDkmNsoGDDbpx81zFr1RBKxMLWRBA7eNiiMtvxJJSF
```

## Estructura del proyecto

Miremos la estructura de nuestras carpetas dentro del proyecto. 

<img src="https://developer.holochain.org/docs/img/folder_layout.png" alt="drawing" width="800"/>

El zome en esencia es un proyecto de Rust, y usa algo que se mencionó únicamente en el principio de la guía, los macros. Esto no es una implicación de que vamos a profundizar en esta funcionalidad, solo rascaremos lo necesario para desarrollar nuestras hApps. Pero entonces, ¿qué es un macro dentro de Rust? 
Básicamente, los macros son una forma de escribir código que escribe otro código, lo que se conoce como metaprogramación. La metaprogramación es útil para reducir la cantidad de código que tenemos que escribir y mantener, que también es uno de los roles de las funciones. Sin embargo, las macros tienen algunos poderes adicionales que las funciones no tienen.

El archivo principal que vamos a estar editando es: `hello_hollo/zomes/code/src/lib.rs`. Vamos a darle un vistazo al código generado, abre `lib.rs` con tu editor de preferencia, tan solo no te asustes cuando…
Oh, sí bueno, no es la bienvenida más cálida. Sentirás que no entiendes nada en absoluto de esa línea, “Sentirás” pero yo te confirmo que con lo que hemos estudiado y una ayuda lo entenderás rápidamente. Las siguientes líneas importan el HDK de Holochain. Nos estamos comunicando con Rust, le expresamos que: "Oye, necesito cosas de todas estas cajas para hacer mi trabajo".

```
#![feature(proc_macro_hygiene)]

use hdk::prelude::*;
use hdk_proc_macros::zome;
```

Usamos una palabra interesante, “cajas”, lo que en inglés es `crates` que a su vez es una funcionalidad más de Rust de la que no hemos hablado. Un crate es una librería o binario. La raíz del crate es un archivo fuente que el mismo compilador de Rust inicia y constituye al módulo raíz del susodicho crate.

Es decir, nos permiten agregar nuevas funcionalidades hechas por otros programadores o por el mismo equipo de Rust a nuestro programa. Los crates vienen almacenados en paquetes que podemos instalar con cargo. Un paquete son uno o más crates que proporcionan un conjunto de funciones. Los paquetes vienen en forma de archivos `.lib` así como nuestro zome, el cual no es de terminación .rs por el hecho de no ejecutado del mismo modo que un proyecto normal en Rust. Holochain se encarga de ejecutarlo y lo unico que necesita es que le proporcionemos el código en forma de libreria. 

Cada parte del codigo que estas viendo en este momento será explicada, pero de momento nos limitaremos a estudiar los fragmentos que necesitamos para nuestra actual tarea. Para evitar confusiones y centrarnos en lo que importa en este exacto momento, prescindiremos de ciertas partes como esta que vamos a remover.  

```
#[derive(Serialize, Deserialize, Debug, DefaultJson, Clone)]
pub struct MyEntry {
    content: String,
}
```

El módulo `my_zome` es donde todo tu código vive. Por otra parte `#[zome]` es un macro procedural. ¿Otro término nuevo de Rust que no tocamos antes? Es correcto, estamos en racha pero quédate conmigo porque esto es fácil. Las macros de procedimiento permiten crear extensiones de sintaxis como ejecución de una función. Podemos ejecutar código en el periodo de compilación que opera sobre la sintaxis de Rust, consumiendo y produciendo la sintaxis de Rust por igual.

En este caso, `#[zome]` que dice que el siguiente módulo define todas las cosas que Holochain debe saber sobre este zome. Nos ahorrada escribir mucho código. Modificaremos `hello_zome` y pondremos el nuevo nombre de `my_power`

```
#[zome]
-mod my_zome {
+mod my_power {
```

La función `init` se ejecuta cuando un usuario corre la aplicación por primera vez. Cada zome define esta función para que podamos realizar algunas tareas de configuración inicial, pero en este zome no hace nada.

```
   #[init]
    fn init() {
```

Retorna las ejecuciones exitosas con valores vacíos `()`. ¡Así es! Estamos retornando datos de tipo unitario. Volvemos al terreno conocido. 

```
       Ok(())
    }
```

Esta función requerida también se ejecuta al inicio de la aplicación, una vez por el nuevo usuario y una vez por los pares existentes. Su finalidad es comprobar que el usuario puede unirse a la red. En este caso, les da a todos un pase gratuito.

```
   #[validate_agent]
    pub fn validate_agent(validation_data: EntryValidationData<AgentId>) {
        Ok(())
    }
```

Removeremos también las siguientes líneas de código porque de momento no las necesitamos. 

```
-      #[entry_def]
-      fn my_entry_def() -> ValidatingEntryType {
-         entry!(
-             name: "my_entry",
-             description: "this is a same entry defintion",
-             sharing: Sharing::Public,
-             validation_package: || {
-                 hdk::ValidationPackageDefinition::Entry
-             },
-             validation: | _validation_data: hdk::EntryValidationData<MyEntry>| {
-                 Ok(())
-             }
-         )
-     }
-
-     #[zome_fn("hc_public")]
-     fn create_my_entry(entry: MyEntry) -> ZomeApiResult<Address> {
-         let entry = Entry::App("my_entry".into(), entry.into());
-         let address = hdk::commit_entry(&entry)?;
-         Ok(address)
-     }
-
-     #[zome_fn("hc_public")]
-     fn get_my_entry(address: Address) -> ZomeApiResult<Option<Entry>> {
-         hdk::get_entry(&address)
-     }

```

> Recordarás que es bastante común de nuestras prácticas en Rust que se retornaban algún tipo de valor de resultado. También recordarás los enums, donde planteamos bloques de código donde puede ser una cosa o la otra, ¡nada de eso ha cambiado! Tenemos un `Ok (some_value)` para mostrar que la función se realizó correctamente o `Err (some_error)` para informar un error. Se espera que las funciones de Holochain requeridas, como init y validadores, devuelvan un tipo de resultado especial llamado ZomeApiResult, que transporta datos entre nuestra aplicación y el conductor. Es una estructura útil y poderosa tal como demostramos en secciones pasadas, por lo que tiene sentido usarla también en las funciones de API que escribamos.

## ¡Libera el poder! 

El paso final, le diremos al zome que nos retorne un string que contenga `Power!` desde una funcion pública. 
Posicionarnos en la función `validate_agent`: 

```
   pub fn validate_agent(validation_data: EntryValidationData<AgentId>) {
      Ok(())
    }
``` 

Justo por debajo comenzaremos a escribir nuestra función pública. La macro de procedimiento `hc_public` convertirá la función directamente debajo de ella en una función pública que las GUI, otros zomes y DNA pueden llamar. Toma nota del nombre de la función, los parámetros que acepta y el tipo de valor que devuelve, esta es la razón por la que Holochain puede llamarlo correctamente.
Agregamos el macro público `hc_public`

```
   #[zome_fn("hc_public")]
```

La función `power_words` no recibirá ningún argumento y retornara un resultado del tipo Holochain. Le diremos a Rust que si el resultado es `Ok` y todo ha salido como debe, contendrá un string.

```
   pub fn power_words() -> ZomeApiResult<String> {
       Ok("Power!".into())
    }
```

Retornamos un valor que contiene el texto que queremos imprimir. `into()` es una pequeña funcionalidad propia de Rust que nos permitirá convertir al instante el valor de retorno en un string. Sin esto, Holochain retornaria un tipo slice. Una estructura de datos que aun no tomara relevancia. Por último compilaremos. 

```hc package```

## Hablar con nuestra aplicación a través de HTTP.

Para interactuar con nuestra aplicación, podemos disponer del modo HTTP. Ejecutaremos este nuevo comando.

 ```hc run -i http```

Podemos enviar un mensaje POST a nuestra aplicación usando curl que nos permite hacer solicitudes HTTP como las que hemos usado para instalar Rust y Nix. Abre una nueva terminal sin cerrar la que se está ejecutando, entra a Nix también desde esa terminal y dirígete a la carpeta del proyecto. Solo entonces ejecuta la siguiente línea. 

```
curl -X POST -H "Content-Type: application/json" -d '{"id": "0", "jsonrpc": "2.0", "method": "call", "params": {"instance_id": "test-instance", "zome": "hello", "function": "power_words", "args": {} }}' http://127.0.0.1:8888
``` 

Si lees con atención el comando, notarás que hay varios nombres propios de las aplicaciones que estamos haciendo, así que si tu práctica tiene nombres diferentes a los empleados en esta guía, tendrás que adaptarlo. Está lejos de ser difícil y siempre es una buena práctica leer lo que vas a ejecutar en la terminal antes de presionar el ENTER. La consola debería retornar lo siguiente: 

```
{"jsonrpc":"2.0","result":"{\"Ok\":\"Power!\"}","id":"0"}
```

¡Ahí tenemos nuestro string! Felicitaciones, has creado tu primera aplicación en Holochain. Pasaremos directamente a ver como elaborar test para nuestra nueva hApp. ¡Aún no terminamos!

## Verdadero Despertar

*Entre los interminables túneles y los pasajes que cada vez tenían más difulcaciones, específicamente en el corazón de aquella red colosal de conexiones, encuentras un domo central lo bastante bien iluminado con farolas en cada extremo como para que puedas practicar.*

*Cargo revolotea por todo el sitio iluminando precavidamente cada esquina que las farolas no pudieran alcanzar. Una vez estando seguros te sientas en el suelo a meditar qué harás a continuación.*

*Pasa el tiempo.*

*Y entonces levantas la mirada decidido. Tu recorrido ha sido largo, demasiado como para rendirse en este momento. Desenvainas tu espada y te concentras.*

*Llamando su poder…*

*Y te responde.* 








