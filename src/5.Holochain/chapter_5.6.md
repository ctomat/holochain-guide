# Enlaces

## Resistir

*Sigues resistiendo.*

*El choque de voluntades ha sido más largo de lo que esperabas, has estado plantado por mucho tiempo contrarrestando la fuerza de tu oponente. Con gran brío empujas a su dirección y comienzas a avanzar lentamente. Recuerdas cada suceso que te enlaza con este momento.*

Recuerdas los enlaces.*

## Los Cuatro Topicos

Ahora podemos estudiar antes de nuestra siguiente práctica con la extensión por decirlo de un modo de los DHT. Las entradas en el DHT están conectadas entre sí mediante enlaces unidireccionales. Nos permiten crear una base de datos de gráficos en el DHT, lo que hace que la información sea fácil de descubrir. 

Los cuatro tópicos de esta lección son: 

1. Por qué es difícil encontrar datos en un DHT.
2. Cómo convertirlo en una base de datos gráfica lo hace fácil.
3. Puntos de partida que puede utilizar para descubrir datos.
4. Cómo se ve en la vida real.

## La dificultad de buscar datos

El DHT tiene una gran ventaja: puede ignorar la ubicación física de los datos y solicitarlos por su dirección de contenido. Esto significa que no hay URL rotos, recursos no disponibles o sorpresas desagradables sobre el contenido de la entrada.

Sin embargo, dificulta la búsqueda de los datos que busca. Las direcciones de entrada son solo números aleatorios, por lo que no le dan ningún detalle sobre lo que contienen sus entradas. Si todos los datos estuvieran todos en una máquina, podría escanearlos rápidamente en busca de una subcadena en particular. Pero eso sería bastante lento en un sistema distribuido, donde todos tienen un poco de todo el conjunto de datos.

Esto crea el problema del huevo y la gallina. Para recuperar una entrada, debemos conocer su dirección. Pero solo conocemos la dirección si:

- Ya tienes el contenido de la entrada y puedes calcular su hash.
- Le das una dirección y se le dice que su entrada tiene lo que está buscando.

La opción 1 es un despropósito total, por lo que nos deja con la opción 2. Pero, ¿cómo hacemos esto?

## Creación de una base de datos de gráficos distribuidos.

Holochain nos permite vincular dos entradas. Esto le permite conectar cosas conocidas con cosas desconocidas, que luego se convierten en cosas conocidas que se vinculan con cosas más desconocidas, y así sucesivamente. El DHT de su aplicación se convierte en una base de datos de gráficos.

Un enlace se almacena en el DHT como metadatos adjuntos a su base, la entrada desde la que estaba vinculado. La dirección de la base es todo lo que necesita para obtener la lista de entradas vinculadas.

Un enlace se almacena en el DHT como metadatos adjuntos a su base, la entrada desde la que estaba vinculado. La dirección de la base es todo lo que necesitamos para obtener la lista de entradas vinculadas.

Un enlace tiene un tipo, como una entrada de aplicación. También puede tener una etiqueta que contenga metadatos adicionales sobre la relación o el objetivo. Podemos poner lo que queramos en esta etiqueta y luego recuperarlo o filtrarlo en una consulta de enlace. Esto es útil para reducir las búsquedas de DHT.

## Punto de Partida

¿Qué tipo de "cosas conocidas" en el DHT se pueden utilizar como puntos de partida? Bueno, afortunadamente la respuesta no se limita a una única alternativa.
Todo el mundo conoce el hash del DNA porque es la primera entrada en su cadena de origen. 
Todos conocen el hash de la entrada de ID de su propio agente porque es la segunda entrada en su cadena de origen.

Podemos crear una entrada que contenga una cadena corta que sea fácil de descubrir, llamada "ancla". El valor de un ancla puede estar codificado en la aplicación o ingresado por el usuario.

> Si bien, en teoría, puedes vincular desde entradas conocidas como el hash de ADN o cadenas codificadas de forma rígida, no se recomienda. Un cierto vecindario de nodos será responsable de almacenar todos los enlaces en esas entradas, creando un "punto caliente" de DHT que gravará desproporcionadamente los recursos de ese vecindario a medida que el DHT crece. Hay otros enfoques, como dividir un ancla en muchos, según el contenido de los objetivos de sus enlaces. Para un ejemplo trivial, en lugar de un ancla `all_usernames`, considera crear uno para cada letra del alfabeto: `usernames_a`, `usernames_b`, etc. Consulte nuestra biblioteca de conjuntos de cubos para obtener un enfoque aún mejor.

Pasemos con un ejemplo más ilustrativo. Alice es una barda y poeta que sobresale en el laúd y quiere compartir su música con el mundo. Ella se une a la aplicación y elige registrar el nombre de usuario @alice_laud. Comprobando si ya se ha tomado su dirección y buscando una entrada DHT de nombre de usuario existente con esa dirección, podemos descubrir que esa entrada no existe, por lo que la pública y la vincula a la entrada de su ID de agente. Ahora, quienes conocen su nombre de usuario pueden encontrar su ID de agente.

Alice quiere aparecer en el directorio público de bardos, por lo que vincula la entrada de su nombre de usuario a un ancla `all_usernames`. Este ancla ya existe en DHT y su valor está codificado en la aplicación. Cualquiera puede consultar este ancla para obtener una lista completa de nombres de usuario.

Alice crea una lista para su nuevo conjunto de poesías y la vincula a la entrada de su ID de agente. Ahora los oyentes que conocen su ID de agente pueden encontrar todas las obras que ha publicado.

Ella sube todas las pistas y las vincula al conjunto de poesías, quiere que la gente pueda encontrar sus creaciones por género, así que selecciona tres géneros aplicables y vincula su álbum a ellos. Estos géneros ya están vinculados al ancla `all_usernames`, cuyo valor está codificado en la aplicación. Los oyentes pueden consultar este ancla para obtener la lista completa de géneros.
Las entradas de Alice, ahora vinculadas entre sí y con otras entradas existentes en el DHT, forman un gráfico que permite a los oyentes descubrirla a ella y a su poesía desde varios puntos de partida diferentes. ¡Genial!

## En Resumen

- No es posible realizar consultas arbitrarias en un DHT, porque las entradas están dispersas en muchos nodos y solo se pueden recuperar por sus direcciones.
- Los enlaces le permiten conectar una entrada conocida (la base) a una entrada desconocida (el objetivo) para crear una base de datos gráfica en el DHT.
- Los enlaces se almacenan en la base y se pueden recuperar con la dirección de su base.
- Los enlaces son unidireccionales; crea una relación bidireccional con dos enlaces.
- Los enlaces pueden tener una etiqueta de cadena arbitraria que permite filtrar resultados o precargar información sobre sus objetivos.
- Una entrada de anclaje es una entrada cuya dirección es fácil de calcular porque su valor es fácil de descubrir, como un nombre de usuario o una cadena codificada.
