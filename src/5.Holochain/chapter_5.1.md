# Arquitectura de las aplicaciones

Ya teniendo todos los componentes que necesitamos para trabajar instalados, comenzaremos a cavar profundamente en los conceptos de Holochain partiendo desde su arquitectura. Las aplicaciones creadas con Holochain tienen un alto carácter modular. Esto permite que sea particularmente fácil compartir código y componer pequeñas piezas que juntas se convierten en un gran todo. Cada parte funcional de una aplicación Holochain, llamada DNA, tiene su propio grupo de reglas, red privada y bases de datos distribuidas.

Hasta ahora siempre hemos aprendido a partir de la demostración, sin embargo, ahora estamos estudiando parrafos y parrafos de texto carentes de ejemplos. Esto no se debe ni mucho menos a un cambio de filosofía, sino a una profunda necesidad de entender los conceptos presentados. Una buena comprensión de los componentes de la pila tecnológica te equipará para diseñar una aplicación bien estructurada y mantenible. Debido a que Holochain es probablemente diferente a lo que normalmente acostumbramos, es bueno cambiar nuestra forma de pensar cuanto antes.

## Más de lo que jamas has leido

*La página ante ti describe un gran círculo como el de tu peto. Sientes que te llama, que te invita a acercarte. Extiendes tu mano hacia la página y la dejas caer sobre las hojas. El libro explota en una luz cegadora, bañandote con su energía interminable.*

*Parpadeas rápidamente tratando de recuperar el sentido de la visión. Te sientes diferente, como si fueras capaz de hacer mucho más, aun más competente de lo que eras antes.*

*─ Eso definitivamente es un comienzo. Ahora solo tenemos que averiguar cómo darle un uso cuanto antes.*

*Arqueas una ceja.*

*«Creo que ya es momento de ilustrar mejor el destino a nuestro héroe.»*

*─ Bien, lo haré yo. ─Bufo Mozilla.─ Veo en tus ojos héroe, que de algún modo ya sabes instintivamente que es lo que has venido a hacer, pero yo te daré una visión más clara.*

*» El-Que-Todo-Lo-Controla es como lo conocen. Al principio llegó a este mundo como una forma de facilitar nuestro día a día. Podíamos crear cosas increíbles con relativa facilidad. Desarrollamos más y más proyectos sin preguntarnos lo más importante: ¿A donde iba todo lo que creemos? ¿Quien lo manejaba? ¿Quién lo supervisaba? Al final nos dimos cuenta de que realmente no éramos dueño de nada de lo que usábamos. Pero entonces fue tarde.*

*» Lo controlaba todo, y ni siquiera nos importaba. El caso es que era fácil y alguien más hacía el trabajo duro por nosotros. Perjudicamos la seguridad de nuestras creaciones, lo frágiles que eran y que cada vez costaba más mantenerlas. En fin, el caso es que somos un puñado de locos que quiere cambiar el mundo creyendo en una profecía la cual eres tú.*

*Tu mandíbula prácticamente cayó al suelo de la impresión. En forma de gesto te señalas con el dedo.*

*─ Así es, tienes que hacerlo tú porque eres el único que puede traer una solución factible sobre la mesa. ─Rezongo Mozilla mientras le daba unos toques al libro que custodiabas.─ Mira, si lo quieres dejar aquí, es entendible. Antes de arriesgarte contra tu voluntad, prefiero que preservemos lo que aprendas.*

*Realmente no querías darle muchas vueltas a esa gran decisión. Por el momento, solo te interesa seguir leyendo.*


## Integridad centrada en el agente: interior, exterior e intermedio

¿Y qué son esas pilas de tecnología? Antes de que hablemos de eso, hablemos de espadas. Una forma heroica de explicarlo. Veamos Holochain como el metal que usamos para forjar nuestra espada, es resistente, ligero y fácil acoplar a nuestras preferencias. La espada separa al mundo en dos espacios diferentes; el tuyo y los que están opuestos a tu filo. Por encima de Holochain está la aplicación. Cada aplicación tiene una forma diferente como una espada donde su filo y disposición del metal varía.

Posicionemosnos de tu lado. Eres el portador del arma, el agente, tienes el poder de recibir la información de lo que sucede alrededor y actuar en función a ello. Juntos, tu copia de la rutina de Holochain y tu aplicación media entre tu persona y el resto de las personas que también tienen Holochain, es decir, individuos que también están armados. 

Del otro lado, frente a ti, más allá del alcance de tu espada, hay personas usando la misma aplicación, como tú, al pertenecer al mismo bando, tienen espadas iguales, la misma aplicación. Holochain se encarga de que puedas interactuar con ellos también, compartiendo información de tácticas y formaciones que deben obedecer, “las reglas del juego que explicamos antes”. Nuevamente, la naturaleza de tu arma, la aplicación, define qué movimiento se considera válido; cuál lado es el afiliado, si el corte es mejor que sea horizontal o vertical. Estamos definiendo como debería ser la data. 

Holochain crea una ‘doble membrana’ para cada participante, creando un puente entre su mundo y el espacio digital que comparten con los demás. Asegura la integridad de la información que pasa tanto por el interior como por el exterior. Esto permite a las personas hacer de forma segura las cosas que son importantes para ellos, sin tener que depender de una autoridad central.

Las espadas construidas con Holochain son muy poderosas, héroe, y tú tienes una en tus manos.

## Pilas de tecnología

<img src="https://i.imgur.com/VVAX0Jc.png" alt="drawing" width="800"/>

Los zomes son módulos de código, como hemos hablado, Holochain es marcadamente modular. Cada Zome define la lógica central de nuestra aplicación. Estos contienen funciones para iniciar cuentas de usuarios; almacenar, validar, entregar data y responder mensajes de otros usuarios. Puedes encontrar algunas de estas funciones dentro del API de los zomes. 

<img src="https://i.imgur.com/RMnObHc.png" alt="drawing" width="800"/>

Uno o más zomes se combinan en un DNA que define la funcionalidad básica y “las reglas del juego” por la porción funcional de la aplicación. Puedes visualizarlo como pequeños servicios. El DNA que se está ejecutando es el agente personal del usuario, cada pieza de data que es creada o enviada, lo hace también de la perspectiva del usuario.  

<img src="https://i.imgur.com/ogtDACY.png" alt="drawing" width="800"/>

La instancia DNA de un usuario puede comunicarse con las APIs de otros a través de una conexión. Esto permite una composición de funcionalidades que son necesarias para una aplicación completamente funcional. Un cliente en el dispositivo del usuario, como una GUI o un script de utilidad, se comunica con las API de DNA a través de un procedimiento remoto ligero llamado interfaz RPC. 

>RPC es cuando un programa de computadora ocasiona una subrutina que se ejecuta en un diferente espacio (normalmente en otra computadora o red compartida), con la particular que fue codificada como si fuera un procedimiento local siendo llamado, todo sin que el programador explícitamente escriba esos detalles para la interacción remota. Es una técnica muy utilizada en el modelo cliente/servidor, pero esto no quiere decir que no lo podemos aprovechar estando apuntando a un modelo descentralizado. 

El cliente es como el frontend de las aplicaciones tradicionales y puede escribir con cualquier lenguaje, caja de herramientas o framework que sea de tu preferencia. La combinación del cliente junto al DNA crea tu hApp.

Todos los DNA están alojados en un conductor del usuario, una rutina que almacena y ejecuta código de DNA, administra el flujo y el almacenamiento de datos y maneja las conexiones entre los componentes de la pila. Puede pensar en los conductores como un servidor de aplicaciones web.

Cada conductor es un nodo en una red peer-to-peer de agentes que utilizan la misma aplicación. Cada DNA de la hApp tiene su propia red privada y un almacén de datos distribuidos. El conductor maneja la comunicación y el intercambio de datos entre nodos.

Los componentes funcionales y las capas arquitectónicas disfrutan de una separación limpia. Podemos combinar, aumentar o reemplazar piezas existentes. Esto le brinda mucha flexibilidad y permite a sus usuarios apropiarse de su experiencia.

>Todas las funciones de nuestro DNA comienzan con un estado de memoria nuevo que se borra una vez que finaliza la función. El único lugar donde se mantiene dicho estado de forma persistente es en el almacenamiento del dispositivo del usuario. 

## En resumen

- Podemos ver que Holochain es diferente de las típicas aplicaciones y sus componentes. En resumidas cuentas:
Cada usuario tiene su propia copia del frontend (cliente), backend (DNA) y servidor (conductor) de la aplicación.
- El conductor almacena el código del DNA, mediando todo el acceso a los recursos del dispositivo, incluidas las redes y el almacenamiento.
- Todo el código se ejecuta en nombre y desde la perspectiva del usuario individual.
- Los usuarios se comunican y comparten datos directamente entre sí en lugar de a través de un servidor central o una red de validación de blockchain.
- Holochain tiene opiniones sobre los datos: maneja todo el almacenamiento y la recuperación (Aprenderemos esto más adelante). 
- Las funciones de DNA no mantienen ningún estado en la memoria entre llamadas.
- La lógica de persistencia y la lógica empresarial central se mezclan en el DNA, porque en el fondo, Holochain es un marco para la validación de datos.
- Sin embargo, normalmente no necesitas mucho código en tu DNA, solo lo suficiente para codificar las "reglas del juego".
- Al igual que con los microservicios (zomes), Holochain se presta a combinar componentes pequeños y reutilizables en grandes aplicaciones.






