# El Descubrimiento de la Descentralizacion

## Un Gran Centro

*Habiendo viajado por tanto tiempo entre llanuras desoladas, sentiste la sorpresa de ver aquella gran metrópolis como un yunque desplomándose sobre tu cabeza. Edificios altos y brillantes con calles interconectadas perfectamente. Los ases de luz apenas visibles antes por ti, ahora eran gruesas líneas compuestas por cientos de estrellas irradiando una fría luz azulada como la iluminación de la misma ciudad a la que te adentrabas.*

*─ Por aquí… Tenemos que movernos rápido. ─Susurro Mozilla*

*Mozilla los lleva a ti y a Cargo por las angostas calles de la Metrópolis. Había gente, o algo así. Los individuos que transitaban por la ciudad solo hacían eso, transitar. Sin saber cuál era exactamente su destino, distengiblemente ciegos. No reparaban en ti por asomo.*

*«Hace tiempo que ya no ven, dejaron de hacerlo hace mucho tiempo permitiendo que El se encargara de todo.»*

*Terminaste en un callejón con una puerta de metal a mano derecha. Mozilla tocó un botón y la lámina de metal se deslizó a un lado. Entras a la base oculta dividida en tres areas; un recibidor amueblado, una sala de prácticas y otra destinada para estudiar.*

## Recordando Holochain

Aunque ya se ha definido con anterioridad este Framework, nunca está de más volver a hacer las presentaciones, estando ya a las puertas de explorar Holochain. Holochain es un Framework de código abierto y un protocolo de red. Nos permite crear aplicaciones que realmente prescindan  de servidores, disponiendo a la vez de seguridad, fiabilidad y rendimiento. Cada usuario ejecuta la aplicación en su propio dispositivo, crear un almacenamiento para su propia data, y se comunica directamente con otros usuarios.*

*Estudiamos en las primeras partes de esta guia como Holochain es diferente de las arquitecturas centralizadas, y como esta aplicación puede hacer a tus aplicaciones más robustas, seguras y económicas.

## El problema con las arquitecturas centralizadas

Estamos familiarizados con la arquitectura cliente/servidor como cualquier desarrollador lo hace. Es confortable, permite un fácil mantenimiento, y es muy bien soportado por el mercado de proveedores de servicio. Además facilita para el desarrollador controlar los permisos a la hora de acceder aplicando reglamentos empresariales. Sin embargo, como epicentro de procesamiento y almacenamiento tiene varias vulnerabilidades. 

- Es un objetivo bastante atractivo para los hackers.
- Los dispositivos y redes antiguas o mal configuradas pueden derrumbar la aplicación. 
- Los costos de hosting escalan con las visitas y el tráfico de la aplicación, es decir, te vuelves victima de tu propio suceso. 
- Almacenar data privada de usuarios incrementa tu responsabilidad legal. 

## Solución Actual

Por un lado tenemos el hosting de plataformas en la nube que nos ofrece un escalado horizontal. Un sistema escala horizontalmente si al agregar más nodos al mismo, el rendimiento de éste mejora. Se basa en mantener el coste de desarrollo y aplicación adaptándose en todo momento a su continuo crecimiento, pero es un sistema más complejo de implementar y administrar. Este tipo de escalabilidad se basa en la modularidad de su funcionalidad. Por ello suele estar conformado por una agrupación de equipos que dan soporte a la funcionalidad completa.

Esto suele ser especialmente trabajoso a la hora de mantenerlo mientras: seas responsable de actualizar el sistema operativo; proveer nuevas máquinas, ya que a medida que crezcas, es necesario agregar más máquinas virtuales que sirvan de nodos;  y preservar la seguridad tanto física como virtual de los equipos. 
La computación sin servidor nos libera de muchos de esos detalles y nos permite enfocarnos en el núcleo de nuestra aplicación. Solo tenemos que escoger las partes que lo conformarán, enlazarlos juntos con funciones y ejecutarlos. Sin embargo, es sólo una abstracción; sigue corriendo en un hardware alquilado con un costo creciente y problemas de centralización. 

El esfuerzo de la computación distribuida, como blockchain, trata de resolver este problema creando una red de participantes que se mantiene por un grupo de data central que es mantenido por cada individuo que usa la tecnología. Cada usuario ayuda preservando la disponibilidad e integridad de la data. Las típicas vulnerabilidades centralizadas son eliminadas. No obstante, se vuelve costoso de replicar, revisar y llegar a un consenso en el contenido del grupo de data principal, algunas veces por diseño. 

Esto deja muy mal parado el rendimiento y crea lagunas. Por no decir que termina tendiendo más a la centralización que a la descentralización, pues los participantes están separados por “grandes nodos” que tienen el poder, reputación o capital para participar, mientras que los clientes de menor peso tienen que solicitar a estos grandes nodos que hagan las cosas por ellos. Esencialmente lo mismo que el modelo cliente/servidor. 

## Cómo Holochain hace diferentes las cosas. 

Holochain tiene una aproximación al problema bastante singular a partir de un grupo diferente de asunciones. Estas asunciones las podemos ver en la misma realidad que nos ofrece importantes lesiones: Los agentes en el mundo físico interactuando entre ellos perfectamente sin una visión absoluta, ordenada y total de los eventos. No necesitamos de un servidor. 

Comienza por los servidores, no por los servidores o data como el componente primario del sistema. La aplicación es modelada a partir de la perspectiva del usuario. A esto se le conoce como computación centrada en el agente. Gracias al poder de la rutina de Holochain, cada usuario corre su propia copia del backend, controla su identidad y almacena su propia data privada y pública. Una red peer-to-peer por cada aplicación significa que los usuarios se pueden encontrar y comunicar directamente. 

En ese momento Holochain pregunta que tipo garantías de integridad de data necesitan las personas para que así puedan interactuar de manera significativa y segura con el otro.Parte del problema está resuelto entonces, todos tienen las “reglas de juego” en su copia del código, pueden verificar que sus peers están jugando el juego correctamente solamente revisando la data que crearon. Encima de todo esto, se agrega seguridad criptográfica y bloqueos contra la manipulación. 

Este es el primer pilar de Holochain: integridad intrínseca de los datos.

De cualquier modo, solo una parte del problema ha sido cubierto. No es particularmente resistente; la data se puede perder cuando la gente se desconecta. Obliga también a todos los implicados a hacer muchísimo por ellos mismo antes de poder encontrar y validar data. Esto nos lleva al siguiente pilar; La replicación y validación entre pares. 

Cada pieza de datos públicos es atestiguada, validada y respaldada por una selección aleatoria de dispositivos. Juntos, todos los participantes que cooperan detectan datos modificados o no válidos, difunden pruebas de actores o validadores corruptos y toman medidas para contrarrestar las amenazas.
Estos en apariencia simples bloques de construcción están creando algo masivo y robusto: un organismo social multicelular con memoria y sistema inmunológico. Imita la forma en que los sistemas biológicos han logrado prosperar frente a nuevas amenazas durante millones de años.

Las bases de Holochain en efecto son sencillas, pero las consecuencias de su diseño pueden generar desafíos que jamás hubiéramos imaginado. Todos estas nuevas problemáticas tienen en cierto modo un paralelismo con nuestro mundo real, haciendo que el mundo de internet que antes parece un plano completamente ajeno a las reglas del nuesto se convierta en una ente más humano. Además, algunos de los problemas más complicados de la computación distribuida son manejados por la propia Holochain en la capa "subconsciente".

## En Resumen

- Las arquitecturas centralizadas tradicionales son fáciles de entender, mantener y seguras, pero crean puntos centrales de falla.
- Holochain da la vuelta a la arquitectura de las aplicaciones: los usuarios están en el centro de su presencia en línea, a cargo de su propia identidad, datos y procesamiento.
- Dentro de las aplicaciones de Holochain, las capas superficiales del procesamiento, almacenamiento y seguridad se distribuye por toda la red. Esto reduce los puntos centrales de falla, los cuellos de botella y los objetivos de ataque atractivos.
- Los dos pilares de la integridad de las aplicaciones son la integridad intrínseca de los datos y la replicación / validación entre pares.
- No existe una base de datos global única; los datos provienen de muchas fuentes individuales.
- Cada participante de una aplicación proporciona sus propios recursos informáticos y de almacenamiento.
- Cada participante también valida y almacena una pequeña parte de los datos de otros participantes.
- El todo es más grande que la suma de sus partes: muchos agentes, que juegan con reglas simples, se combinan para formar un organismo social que mantiene su propia salud.

## Instalar Holochain

¡Tu poder está despertando poco! No podemos desaprovechar esta oportunidad para cosechar la semilla de tu poder, héroe. Es momento de instalar Holochain con nix-shell. En breve seguiremos con los conceptos básicos del framework, por otra parte, ningún mal hace ya tener todo configurado para el momento en el que saltemos a los ejemplos… 

La pregunta más apropiada a este punto sería: ¿Qué es Nix? Bueno, si nos remontamos a la explicación que conseguimos en la wiki de la tecnología, tendríamos el siguiente texto:

*“Nix es un administrador de paquetes y un sistema de compilación que analiza instrucciones de compilación reproducibles especificadas en Nix Expression Language, un lenguaje funcional puro con evaluación diferida. Las expresiones de Nix son funciones puras que toman dependencias como argumentos y producen derivaciones que especifican un entorno de compilación reproducible para el paquete. Nix almacena los resultados de la compilación en direcciones únicas especificadas por un hash del árbol de dependencias completo, creando un almacén de paquetes inmutable que permite actualizaciones atómicas, reversiones e instalación simultánea de diferentes versiones de un paquete, esencialmente eliminando el infierno de dependencias.”*

No hay que dejarse intimidar por estos conceptos, héroe, solo hay que dividirlo en partes más manejables. En primer lugar nos dice que es un administrador de paquetes, esto quiere decir que es algo que nos permite instalar, desinstalar y manipular paquetes que no son otra cosa que programas o funcionalidades que podemos aprovechar en nuestras computadoras. 

Además nos explica que es un sistema de compilación que analiza instrucciones de compilación reproducibles especificadas en Nix Expression Language. Significa que no solo sabe administrar los paquetes, sino que almacena como estos paquetes han sido instalados y así hacerlo reproducible. Podríamos decir que se asemeja a un controlador de versiones controlado por su propio lenguaje llamado Nix Expression Language. 

«Nix almacena los resultados de la compilación en direcciones únicas especificadas por un hash del árbol de dependencias completo, creando un almacén de paquetes inmutable que permite actualizaciones atómicas, reversiones e instalación simultánea de diferentes versiones de un paquete» ¡Muy bien! Aquí está la clave, Nix como cualquier gestor de paquetes es capaz de realizar toda clase de funciones con los susodichos, pero el concepto además nos explica que crea un entorno de ejecución para estos paquetes que podemos replicar.
Es fácil discernir con algo de experiencia en Javascript qué es esto es tal como si de Node.js se tratase. Nix lo que nos permitirá es trabajar con diferentes versiones de Holochain. Algo vital considerando que el framework está en una fase de desarrollo alfa a día de hoy.Por lo que poder retroceder en caso de una actualización rota es más que bienvenido.   

> ¿Qué no sabes qué es Node.js? Solo tomará un segundo arreglar eso; Node.js es un entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor (pero no limitándose a ello) basado en el lenguaje de programación JavaScript. Este viene con un gestor de paquetes llamado npm, y en conjunto hacen algo similar a lo que está haciendo Nix: crear un entorno donde podamos instalar paquetes y controlar sus versiones.

La instalación recomendada de Nix es exactamente igual a la que podemos encontrar con Rust al comienzo de esta guía. Solo necesitamos curl para llevar a cabo esta tarea. Si te encuentras siguiendo esta guia desde una computadora cargada con Windows, la instalación es algo diferente y en breves se explicara. En la terminal ingresamos la línea de comando que nos proporciona el sitio oficial de Nix. 

```
curl -L https://nixos.org/nix/install | sh
```

Posteriormente tenemos dos opciones, desconectarnos y volvernos a conectar en nuestra cuenta de usuario, o, para no tener que hacer algo tan tedioso, solo debemos correr el siguiente comando para que nuestra terminal reconozca la nueva instalación.

```. ~/.nix-profile/etc/profile.d/nix.sh```

Ingresamos esta última línea en la consola para asegurarnos de que Nix ya esté listo para ser utilizado.

```nix-shell --version```

El resultado debería arrojarnos la versión que tenemos instalada de Nix. 

```nix-shell (Nix) 2.3.7```

Ahora que hemos instalado Nix, somos capaces de ejecutar un shell de desarrollo que contiene todos los requisitos previos, incluidos las versiones correctas de Rust y Node.js y las herramientas de Holochain. Este shell no interferirá con la configuración actual de tu sistema, por lo que no tienes nada que temer a la hora de experimentar. Ejecuta este comando:

```nix-shell https://holochain.love```

Desde este momento, siempre que quieras utilizar Holochain, tendrás que correr esta línea de comando. Una vez termina la instalación de la primera vez que ejecutamos la línea, veremos que nuestro prompt ha cambiado y ahora vemos esto en nuestra terminal. 

```[nix-shell:```

Al igual que con Nix, comprobaremos que la instalación de Holochain está funcionando correctamente. Tenemos dos alternativas:

```hc --version```

o

```holochain --version```

Deberiamos de ver algo asi:

```hc 0.0.51-alpha1```

o asi.

```holochain 0.0.51-alpha1```

En adelante no necesitamos preocuparnos por actualizar o desinstalar. Cuando ingresemos al nix-shell para trabajar con Holochain, el gestor buscará la última versión aprobada, descargara cualquier actualización y limpiara la configuración cuando terminemos. Un entorno seguro y controlado para aproximarnos a cualquier nueva tecnología aún en desarrollo.  

