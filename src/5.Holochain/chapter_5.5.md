# El DHT: una base de datos pública distribuida

## Inesperado

*Su forma era… inesperada.*

*Dentro de la planta más alta de la torre se encontraba una torre aún más pequeña con luz relampagueante. Lo encaras con el pecho en alto listo para la batalla. Su fuerza era abrumadora, naturalmente, lo controlaba todo. Celeste era el color de la energía que irradiaba, sin embargo, en lo que te reconocio sus colores rápidamente cambiaron a rojo.*

*Sin perder más tiempo te embiste con su convicción. Tratas de mantenerte firme pero la reacción El-Que-Todo-Lo-Controla te sorprende. Estás a punto de ceder.*

*Pero recuerdas cómo defenderte.*

*Recuerdas DHT.*

## Pilares de DHT

Habiendo terminado el desarrollo del test de nuestra primera hApp, ahora podemos hablar de otros dos conceptos de vital importancia; DHT y Links. Comenzaremos con los DHT en donde aprenderemos:

1. Las desventajas y los riesgos de los datos propios
2. Cómo se almacenan los datos públicos
3. Cómo los nodos almacenan y recuperan datos en una base de datos distribuida
4. ¿Qué sucede cuando la red se cae?
5. Cómo se validan los datos públicos

Si no está seguro de sí una red distribuida puede proporcionar las mismas garantías de integridad, rendimiento y tiempo de actividad que un sistema basado en servidor bajo nuestro control, esta lección te dará una mejor idea de cómo Holochain aborda estos problemas. También comprenderemos mejor cómo se almacenan los datos del usuario, lo que nos ayudará a pensar en cómo diseñar su capa de persistencia.

## Los datos propios no son suficientes

Hablemos de nuestra cadena de origen nuevamente. Te pertenece, vive en tu dispositivo y puedes optar por mantenerlo en privado.

Sin embargo, el valor de la mayoría de las aplicaciones proviene de su capacidad para conectar a las personas entre sí. Las herramientas de correo electrónico, redes sociales y colaboración en equipo no serían muy útiles si guardara todo su trabajo para nosotros. Los datos que residen en nuestra máquina tampoco están muy disponibles; tan pronto como se desconecta, nadie más puede acceder a ellos. La mayoría de los usuarios no quieren ejecutar sus propios servidores, por lo que debe haber una forma de hacer que los datos públicos se queden.

Este es también el punto en el que nos encontramos con problemas de integridad en un sistema peer-to-peer. Desafortunadamente, cuando todos son responsables de sus propios datos, pueden jugar con ellos de la forma que quieran. En secciones pasadas, aprendimos que la cadena de origen firmada es resistente a la manipulación de terceros, pero no a la manipulación por parte de su propietario. Un agente puede borrar una transacción o un voto y hacer que parezca que nunca sucedió.
Y finalmente, parece ineficaz hacer que cada nodo verifique la exactitud de todos los datos que encuentra. Seguramente podríamos acelerar las cosas si sabemos que varias personas están accediendo al mismo dato.

Si no estás seguro de si una red distribuida puede proporcionar las mismas garantías de integridad, rendimiento y tiempo de actividad que un sistema basado en servidor bajo nuestro control, esta lección te dará una mejor idea de cómo Holochain aborda estos problemas. También comprenderemos mejor cómo se almacenan los datos del usuario, lo que nos ayudará a pensar en cómo diseñar su capa de persistencia.

## La tabla hash distribuida, un almacén de datos público

<img src="./img/DHT1.png" alt="DHT1" width="300"/>

En una red Holochain, se comparten los encabezados de la cadena de origen y las entradas públicas con una selección aleatoria de sus pares, que son testigos, validan y mantienen copias de ellos. Cuando confirmas una entrada privada, permanece en nuestra cadena de origen, pero el encabezado aún se comparte. Cada DNA tiene su propia red privada y encriptada de pares que chismean entre ellos sobre nuevos pares, nuevas entradas de datos, datos no válidos y el estado de la red.

## Encontrar pares y datos en una base de datos distribuida

Las bases de datos distribuidas tienen un problema de rendimiento: a menos que tengas tiempo para hablar con cada nodo, es muy difícil encontrar los datos que estás buscando. Así es como los DHT manejan esto:

1. Le dan a cada nodo una dirección aleatoria.
2. Cuando se crea una entrada, calculan el hash de su contenido. Esta se convierte en su dirección o clave.
3. Almacenan la entrada en el nodo cuya dirección es numéricamente más cercana a la dirección de la entrada.
4. Cuando un nodo tiene una dirección de entrada y quiere el contenido, consulta el nodo más cercano a la dirección de entrada.
5. Cada nodo conoce a sus vecinos y a algunos conocidos lejanos. Usando estas conexiones, pueden encontrar cualquier otro nodo en el DHT con solo unos pocos saltos. Esto hace que la búsqueda de datos sea bastante rápida.

Cada nodo conoce a sus vecinos y a algunos conocidos lejanos. Usando estas conexiones, pueden encontrar cualquier otro nodo en el DHT con solo unos pocos saltos. Esto hace que la búsqueda de datos sea bastante rápida.

<img src="./img/DHT2.png" alt="DHT1" width="300"/>

Veamos cómo funciona esto con un espacio de direcciones muy pequeño en el que las direcciones son letras del alfabeto.

<img src="./img/DHT3.png" alt="DHT2" width="300"/>

Alice vive en la dirección A. Sus vecinos de la izquierda son Diana y Fred, y sus vecinos de la derecha son Zoe y Walter.

<img src="./img/DHT4.png" alt="DHT3" width="300"/>

Alice crea una entrada que contiene la palabra "molécula", cuya dirección es M.

<img src="./img/DHT5.png" alt="DHT4" width="300"/>

De todos los vecinos de Alice, Fred es el más cercano a esa dirección, así que ella le pide que la guarde. Fred no es responsable de esa dirección, por lo que le cuenta a Alice sobre su vecina Louise.

<img src="./img/DHT6.png" alt="DHT5" width="300"/>

Alice comparte la entrada con Louise, cuyo vecindario cubre M, por lo que acepta guardarlo.

<img src="./img/DHT7.png" alt="DHT6" width="300"/>

Louise lo comparte con su vecino, Norman, en caso de que se desconecte.

<img src="./img/DHT8.png" alt="DHT7" width="300"/>

Rosie se entera de que alguien ha publicado una palabra interesante cuya dirección es M. Le pregunta a su vecino, Norman, si la tiene. Louise ya le ha dado una copia, así que se la entrega a Rosie.

## Resiliencia y disponibilidad

El DHT almacena múltiples copias redundantes de cada entrada para que la información esté disponible incluso cuando su autor, o algunos de sus validadores, están fuera de línea. Esto permite que otros accedan a él cuando lo necesiten.

También ayuda a una aplicación a tolerar las interrupciones de la red. Si tu ciudad sufre un desastre natural y se desconecta de Internet, tú y sus vecinos aún pueden usar la aplicación. Es posible que los datos que veas no estén completos o no estén actualizados, pero aún pueden interactuar entre sí. Incluso puedes usar la aplicación cuando estás completamente desconectado.

El autor de una aplicación puede especificar el nivel de redundancia de datos deseado. A esto se le llama factor de resiliencia. Los agentes colaboradores trabajan duro para mantener suficientes copias de cada entrada para satisfacer el factor de resiliencia. Debe establecerse más alto para aplicaciones que requieren mayor seguridad o mejor tolerancia a fallas.

Veamos cómo se desarrolla esto en el mundo real.

1. Una isla está conectada al continente mediante un enlace de radio. Se comunican entre sí mediante una aplicación Holochain.

2. Un huracán atraviesa y arrasa ambas torres de radio. Los isleños no pueden hablar con los continentales y viceversa, pero todos pueden seguir hablando con sus vecinos físicos. No se pierde ninguno de los datos, pero no todos están disponibles para cada lado.

3. En ambos lados, todos los agentes intentan mejorar la resiliencia ampliando sus vecindarios de DHT. Mientras tanto, operan como de costumbre, hablando entre ellos y creando nuevas entradas.

4. Se reconstruyen las torres de radio. La partición de red "se recupera" y los datos nuevos se sincronizan en el DHT.

## Una nube de testigos

Cuando presentamos los conceptos básicos de Holochain, dijimos que el segundo pilar de la confianza es la validación entre pares. Cuando se le pide a un nodo que almacene una entrada, no solo la almacena, sino que también verifica su validez. A medida que la entrada se pasa a más nodos en nuestro vecindario, recopila más firmas que dan fe de su validez.

Antes de almacenar una entrada o encabezado, un validador verifica que:

- La firma de la entrada es correcta.
- El encabezado es parte de una cadena de fuentes ininterrumpida, sin modificar y sin ramificar, lo que le impide reescribir su propio historial.
- El contenido de la entrada se ajusta a las reglas de validación definidas en el DNA.
- Si alguna de estas comprobaciones falla, el validador marca la entrada como inválida y difunde noticias sobre la actividad del agente a sus vecinos. Esto es lo que crea el sistema inmunológico de Holochain.
- El uso de un hash como dirección tiene una buena ventaja: las direcciones de entrada son aleatorias. Las direcciones de nodo también son aleatorias, lo que significa que la selección del validador es imparcial. También distribuye la carga de datos de manera bastante uniforme alrededor del DHT. Lo importante es que el DHT recuerda lo que has publicado, por lo que es muy difícil no cumplir con su palabra. La validación es una parte muy importante de un DNA, quizás la parte más importante. 

## En Resumen

- Algunas entradas de la cadena de fuentes están marcadas como públicas. Los nodos los comparten con sus pares en una base de datos distribuida llamada DHT.
- Los encabezados de la cadena de origen de las entradas públicas y privadas también se comparten con el DHT.
- Una entrada en un DHT se recupera por su dirección única, que es el hash del contenido de la entrada.
- El DHT de Holochain es un DHT de validación que recuerda el resultado de la validación de las entradas existentes. Esto acelera las cosas para todos y permite la detección de malos actores.
- El DHT de Holochain también detecta los intentos de los agentes de revertir sus cadenas de origen y crear historias alternativas.
- Cualquier nodo se puede seleccionar al azar para validar y mantener una entrada, según la proximidad de su dirección a la dirección de la entrada. Esto ayuda a aleatorizar la selección del validador.
- Las noticias de los malos actores se difunden a través de órdenes judiciales, que son entradas especiales que contienen evidencia de corrupción.
- Un DHT puede establecer un factor de resiliencia, o el nivel esperado de redundancia, para cada entrada.
- Un DHT tolera las interrupciones de la red. Puede seguir funcionando como dos redes independientes y, posteriormente, "recuperarse" cuando se repare la red.

