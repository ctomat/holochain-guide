# Dragones y Descentralizacion: Una guía de Holochain

## Y entonces, despiertas. 


*Despiertas en un páramo oscuro y desolado, lo primero que notas es la ausencia de colores, o en todo caso, la homogeneidad de estos. El mundo completo hasta donde alcanza tu visión esta cubierto por una paleta de morados muy tenues, los cuales se ven aun más oscurecidos por la ausencia de un sol, o incluso, una luna que emanara iluminación alguna al vacío paisaje. Sin embargo, en el cielo uva logras atisbar hilos apenas discernibles. Tambien logras ver que estos hilos en una dirección especifica –Presumes el norte.–  dan la impresión de que se vuelven algo más brillosos, naturalmente no estas seguro de esto ultimo, pero tampoco es como si tuviera otra cosa que hacer…*
