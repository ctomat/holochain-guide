# Nuestro Primer Hola Mundo

Ahora que has instalado Rust, vamos a escribir nuestro primer programa en el lenguaje. ¡Como cabría esperar sera un hola mundo para entrar en ambiente con la sintaxis del lenguaje! 

> Nunca está de más recordarte que este libro procede bajo la asunción de que usted, héroe, ya tiene las nociones básicas en programación. Esto quiere decir que no nos pararemos para hablar de IDE o cualquier otra herramienta no directamente relacionada con Rust. ¡Pero no temas! Como el defensor de la descentralización que eres estoy seguro de que esos conocimientos duermen en ti, solo ve a buscarlos y cuando te sientas listo vuelve. 

Tenemos que mantener el orden en nuestros proyectos desde ahora, por lo que es altamente recomendable crear una carpeta donde guardaremos todos los ejercicios a realizar. Dentro de esa carpeta crearemos otra carpeta con el nombre de “hello_world”. Entonces estaremos listos para elaborar nuestras primeras lineas de codigos. 

> En el github de la guía tendrás todos los ejemplos ya realizados para cualquier consulta rápida. ¡Aprovecha! 

Para Linux y Mac, nos acostumbraremos a utilizar la línea de comandos para hacer todo esto aún más eficiente. 

```text
$ mkdir ~/projects
$ cd ~/projects
$ mkdir hello_world
$ cd hello_world
```

¿Windows? Nada que temer, puedes crear tus carpetas del modo habitual o dar un paso adelante como el valiente salvador que eres y usar el CMD. 

```cmd
> mkdir "%USERPROFILE%\projects"
> cd /d "%USERPROFILE%\projects"
> mkdir hello_world
> cd hello_world
```

Seguidamente vamos a crear nuestro primer archivo al cual llamaremos main con la extensión **.rs**. Si quieres ponerle un nombre diferente igual funcionara, solo recuerda que si es más de una palabra estas deben estar separadas por un guión bajo como este sin las comillas: “_”. 

Abriremos el archivo en nuestro editor de preferencia e ingresamos el código descrito en el Listing 1-1

```rust
fn main() {
    println!("Hello, world!");
}
```

Ahora salvaremos el archivo y lo ejecutaremos. Para ello tenemos que usar un poderosísimo hechizo; te lo enseñaré pero tienes que prometer solo enseñarselo a héroes de las descentralización como usted. El conjuro varía un poco dependiendo del OS que usemos.

En Linux y Mac tenemos que tipear:

```text
$ rustc main.rs
$ ./main
Hello, world!
```
Por otra parte, si queremos ejecutar el código desde el CMD lo haremos así `.\main.exe` en lugar de `./main`:

```powershell
> rustc main.rs
> .\main.exe
Hello, world!
```

### ¡Eso ha sido genial...! ¿Como lo hice?

![alt text](https://anotherafterthought.files.wordpress.com/2015/10/harry-potter-potions-class-explosion-seamus-finnigan.gif?w=474&h=200 "Logo Title Text 1")

Vale, aunque no lo parezca, muchas cosas sucedieron en este momento. Comencemos explicando la estructura de Rust para aclarar algo más el panorama. 

```rust
fn main() {

}
```
Primero tenemos esta línea en donde estamos definiendo una función en Rust; pero esta no es una función cualquier y la palabra clave “main” no está ahí únicamente de decoración. Todo aquello que esté contenido en la función main tal como en C++ será lo primero a ser ejecutado por el programa. Esta función no retorna nada en absoluto y los parámetros deben ser declarados dentro del paréntesis. 

>Sobra presentar las llaves como abertura y cierre de una función, pero cabe enfatizar en que es una buena práctica poner la llave de apertura en la misma línea donde es declarada la función tal como se ha hecho en este ejemplo. ¡No te preocupes si olvidas esto, héroe! Dentro de ti tiene un sexto sentido llamado rustfmt. Esta poderosa herramienta al momento de compilar te recordará todas estas buenas prácticas (incluyendo el nombrar los archivos con nombres de más de dos palabras con guiones bajos.)

Dentro de la función main encontramos: 

```rust
    println!("Hello, world!");
```
Es fácil discernir el hecho de que estamos ordenando al programa que imprima en la consola un “Hello, world!”. Pero hay unos detalles importantes dentro de esta simpleza. Primero que todo, Rust está indentado con cuatro espacios, no una sangría. Segundamente, “println!” llama a un macro de Rust. Por lo tanto **no es una función, recuerdalo.** Si quisiéramos llamar a una función en su lugar tendríamos que excluir la exclamación del siguiente modo: “println”. En esta guía no entraremos en detalles con los macros pues solo nos interesa los fundamentos de Rust para pasar cuanto antes a Holochain. 

>Nótese que cada línea termina con un punto y coma, si vienes de lenguajes como Python esto quizás te incomode un poco.

### ¿Y qué fue ese comando que puse antes de ejecutar el programa? 

¡El sortilegio, sí! AL escribir rustc llamamos al compilador de Rust. Quizás este concepto te suene un poco ajeno si tu nicho son los lenguajes dinámicos tales como Javascript, Ruby o Python. Al compilar solo estamos traduciendo nuestras órdenes al lenguaje máquina, y por ende, a un ejecutable funcional. 

¡Eso seria todo por esta sección! Felicitaciones, has completado tu primer programa en Rust y tu poder crece en este momento.


![alt text](https://i.pinimg.com/originals/b3/cb/5a/b3cb5a293121ae02d73aa5fc173f2a1d.gif "Logo Title Text 1")

### Chispas

*A medida que te acercas a tu reflejo más estupefacto te encuentras, es como si por primera vez te estuvieras viendo y reconoces tu auténtico potencia.... Extiendes la mano hasta tocar el cristal. Unas chispas disparadas de la punta de tus dedos iluminan la habitación, y todo tu aquello que te rodea de algún modo responde; una especie de saludo. Es como si reconocieran tus ordenes que se traducen a su idioma. Un hola mundo. Cargo se emociona aún más y volando alrededor de tu cabeza describiendo varios círculos a alta velocidad te dice:*

*«Jojojo, parece que usted aprende a una gran velocidad, señor héroe. ¡Dentro de muy poco seremos capaces de presentarnos con la bruja Mozilla!  Vamos, sigamos a la siguiente torre, es importante apurar tu aprendizaje… tenemos un destino que cumplir. El camino te explicare mejor en como te puedo ayudar.»*

