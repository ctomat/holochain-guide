# Artes Místicas de Rust

*Miras detrás de ti, directamente hacia las escaleras por la que subiste. Una luz oxidada sobresale  del descenso, buscas esconderte pero el resplandor es más veloz. Se posiciona en el centro de la sala, y aun ausente de ojos que te puedan ver, sientes que su atención esta fijada completamente en ti. Lentamente se acerca a ti, te quedas inmóvil esperando a que algo suceda.*

*Y algo sucede. La luz te habla.*

<img src="./img/cargo.gif" />

*«¡Lo sabia! Eres **tú**. No puedo creer que por fin hayas llegado, ¡pero aquí estas! No sabes cuanto te he esperado, hay mucho que hacer, por aprender, por ver y… oh, perdona, me emocione demasiado. ¡Soy Cargo! Tu nuevo guía, haré todo lo que este en mis manos para ayudarte… bueno, sí, sé que no tengo manos, ¡aunque si las tuviera serian muyyyyy grandes! ¿Qué te parece si te enseño a hacer unos cuantos trucos?»*

*Aceptas sin dudarlo.*

## ¡Manos a la obra!

¡Muy bien! Teniendo todo un poco más claro, por fin podremos hablar de código, y sobretodo, a ponernos manos a la obra que para algo estamos aquí. En esta sección comenzaremos con una presentación más formal del lenguaje de programación Rust, luego, haremos un paso a paso de la instalación y sus grandes ventajas, porque veras que Rust no se instala como Python o Ruby. Es un proceso muy sencillo independientemente de tu plataforma, ya sea Windows, Mac o cualquier distribución de Linux. Inmediatamente después procederemos a escribir nuestro primer “Hola Mundo” con Rust, y desde ahí sera un paso lento pero constante a través de sus conceptos básicos, sus grandes funciones como Ownership y Cargo, su gestor de paquetes y compilador que nos dará los mejores consejos para optimizar nuestro código.

