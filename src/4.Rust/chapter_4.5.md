## Innumerables Torres

*No sabrías decir si era solo tu imaginacion o si llegaste a la primera torre absurdamente distraído; pues ahora se erguía decenas de estas edificaciones, haciendo un bosque de piedra y tejas. Mientras vas andando con Cargo das vueltas tratando de advertir las fronteras de tan extraño bioma, pero es en vano, solo puedes ver más y más torres. Tocas a Cargo con tu indice y te inclinas de hombros; Cargo nota tu desconcierto.*

*«¡No tienes nada de qué preocuparte! Yo conozco el camino; ese es mi trabajo después de todo. Cada una de estas torres almacenan mucho conocimiento. Ojala hubiera el tiempo suficiente de verlas todas, sin duda te servirán, pero hay que apurarnos, queda poco tiempo y debes aprender mucho…  »*

# Conceptos Basicos de Programacion

Esta guía sigue desarrollándose bajo la asunción de que ya cuentas con conceptos basicos de programacion. No obstantes, hay aspectos esenciales de Rust que naturalmente serán tratados en esta guia pues es necesario para seguir adelante. Mejor prepárate, héroe, porque aquí comienza el entrenamiento verdadero. 

## Variables y Mutabilidad

Tiene gran importancia entender cómo funcionan las variables de Rust. Estas por defecto son inmutables,  es decir, una vez declarada la variable, no puede ser modificada. Dicha decisión de diseño se debe a la filosofía de Rust donde su prioridad es es la seguridad y optimización. De igual modo, existe la opción de hacer una variable mutable. Veamos en qué casos es más conveniente la inmutabilidad y porque en alguno casos dependemos de una variable cambiante. 

Ilustremos un poco mejor la naturaleza de las variables inmutables a través del siguiente ejemplo. Crearemos un nuevo proyecto llamado **variables** en la ruta donde estés almacenando todos tus proyectos de Rust usando `cargo new variables`. Entonces, en nuestro nuevo **src/main.rs** escribiremos el siguiente codigo. 

```rust
fn main() {
    let x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
```

Guarda  y corre el programa usando `cargo run`.

```
$ cargo run
   Compiling variables v0.1.0 (file:///projects/variables)
error[E0384]: cannot assign twice to immutable variable `x`
 --> src/main.rs:4:5
  |
2 |     let x = 5;
  |         -
  |         |
  |         first assignment to `x`
  |         help: make this binding mutable: `mut x`
3 |     println!("The value of x is: {}", x);
4 |     x = 6;
  |     ^^^^^ cannot assign twice to immutable variable

error: aborting due to previous error

For more information about this error, try `rustc --explain E0384`.
error: could not compile `variables`.

To learn more, run the command again with --verbose.
```

Uy, parece que algo salió mal, ¿a ti qué te parece? 

![alt text](https://media.giphy.com/media/azGJUrx592uc0/giphy.gif "Logo Title Text 1")

¡Tranquilo! Es normal caer unas cuantas veces antes de dominarlo; esto en sí es un excelente ejemplo de cómo el compilador lidia con los errores. El mensaje de error indica lo que está causando el error `cannot assign twice to immutable variable x`, que vendria siendo que no puedes asignar dos variables x tal como asignamos en el código.  En Rust, el compilador garantiza que el estado de una variable no cambie si realmente no puede cambiar. 

Aun así la mutabilidad es muy útil por no decir indispensable. Las variables son inmutables solo por defecto. Puedes hacerlas mutables agregando `mut`frente a el nombre de la variable. Además de permitir que el valor cambie, `mut` deja claro para futuros lectores que esa variable cambie a lo largo del programa y deben estar atentos a ella. 

Por ejemplo, vamos a cambiar *src/main.rs* a:

<span class="filename">Filename: src/main.rs</span>

```rust
fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
```

Corriendo el programa deberíamos tener esto.

```text
$ cargo run
   Compiling variables v0.1.0 (file:///projects/variables)
    Finished dev [unoptimized + debuginfo] target(s) in 0.30s
     Running `target/debug/variables`
The value of x is: 5
The value of x is: 6
```

Fuimos capaces de cambiar el valor de `x` de `5` a `6` cuando `mut` es usado. En algunos casos, queras que las variables sean mutables porque hace el código más conveniente de escribir que si solo hubieran variables inmutables. Recuerda que una variable inmutable **NO** es lo mismo que una constante. Primero, con una constante no podrías usar `mut`  porque estas siempre serán inmutables.  Además las constantes pueden ser declaradas en cualquier scope, incluso el global. Lo cual lo hace muy útil para valores que son usados en muchas partes del código. 


```rust
const MAX_POINTS: u32 = 100_000;
```

De este modo se declaran las constantes en Rust. Su nombre siempre debe estar completamente en mayúsculas. Las constantes son válidas durante toda la ejecución del programa. 

### Shadowing

A las variables inmutables no se le puede cambiar su valor, pero se puede “ensombrecer”. El término correcto es *shadowed* proveniente del inglés. Basicamente, shadowing es una forma diferente de algún modo hacer a una variable `mut`. Usando `let` podemos realizar unas cuantas transformaciones pero manteniendo la variable inmutable luego de esos cambios. 

Sí, vale, no me pongas esa cara porque en breve me explico. Al usar let con el nombre de una variable ya creada, no la estamos modificando realmente, sino que estamos creando una variable completamente nueva que sustituye a la antigua. Por ejemplo, digamos que nuestro programa le pide al usuario que ingrese cuantos espacios desea entre un texto y otro. Nosotros queremos almacenar esa entrada de datos como un número. 

```rust

fn main() {
    let spaces = "   ";
    let spaces = spaces.len();
}
```

Esto es posible porque la primera variable `spaces` es de tipo string, y el segundo `spaces`, resulta ser una completamente nueva variable que tiene el mismo nombre que el primero, ahora es un tipo numérico. Shadowing nos permite no tener que lidiar con varias docenas de variables como podría ser el hipotético caso de tener `spaces_str` y `spaces_num`si no dispusieramos de esta funcionalidad. 

```rust

fn main() {
    let mut spaces = "   ";
    spaces = spaces.len();
}
```
El error dice que no podemos 

```text
$ cargo run
   Compiling variables v0.1.0 (file:///projects/variables)
error[E0308]: mismatched types
 --> src/main.rs:3:14
  |
3 |     spaces = spaces.len();
  |              ^^^^^^^^^^^^ expected `&str`, found `usize`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0308`.
error: could not compile `variables`.

To learn more, run the command again with --verbose.
```

## Magia Estricta 

*No sabrías decir si habías leído antes o si tuviste una existencia pasada, pero si ese fuera el caso, te sientes convencido de que nunca habías estudiado tanto. Nuevos conceptos que no terminas de entender con claridad se te presentan, antes de que puedas aprender el anterior uno nuevo se presenta. Cargo te sirve de tutor a la vez que lampara.*

*«Nada que tu crees cambiará al menos que explícitamente así lo quieras; la magia es mucho más estricta de lo que cree la gran mayoría. Solo los mejores pueden hacerla lucir flexible.»*

*Creas una variable que te sirva para modificar tu entorno, te concentras para darle un numero y lo lanzas sobre el libro que sostienes. No sucede nada.*

*«Con donde quieres que apliquen tus variables también tienes que ser específico. Toma en cuenta dónde exactamente quieres que se manifieste la variable.»*

*Aceptas el consejo y tratas de ejecutar el consejo que te dio Cargo. Esta vez sí sucedió. El libro primero se volvió un borrón para inmediatamente luego de un chasquido convertirse en dos tomos exactamente iguales.*

*«¡Excelente! Simple pero preciso. Ahora trata de convertir el segundo libro en algo más.»*

*Lo intentas pero nada ocurrió, solo eres capaz de sentir como el libro se resistio a la transformación. Cargo aunque carente de expresiones, daba el aura de estar sonriendo. Hizo un movimiento muy similar a asentir, parecía seguro de que eso sucedería.*

*«Aunque lo quisiste explícitamente quisiste convertir el libro en algo completamente distinto; para esos casos primero tienes que hacerlo dejar de ser antes de convertirlo en algo nuevo. Presta atención.»*



## Data Types

Cada valor en Rust es un cierto **tipo de data**, que indica al lenguaje con que está trabajando. Veremos dos tipos de data: escalado y compuesto. Rust es un lenguaje de tipado estático. El compilador puede normalmente figurarse el tipo de data que queremos usar basándose en el valor y como es usado. En casos donde muchos tipos son posibles, como cuando convertimos un String a numerico paseando, debemos darle una notación al lenguaje. 

```rust
let guess: u32 = "42".parse().expect("Not a number!");
```
En el caso de que no lo hiciéramos aquí, Rust desplegará el siguiente error, el cual significa que el compilador necesita más información de nosotros para saber cual tipo de data queremos usar: 

```text
$ cargo build
   Compiling no_type_annotations v0.1.0 (file:///projects/no_type_annotations)
error[E0282]: type annotations needed
 --> src/main.rs:2:9
  |
2 |     let guess = "42".parse().expect("Not a number!");
  |         ^^^^^ consider giving `guess` a type

error: aborting due to previous error

For more information about this error, try `rustc --explain E0282`.
error: could not compile `no_type_annotations`.

To learn more, run the command again with --verbose.
```


## Primero los numeros

*Cargo seguía examinando cada movimiento que hacías. Ajustandolo. Perfeccionandolo.*

*«Hay diferentes tipos de número como es lógico. Algunos son más grandes y otros más pequeños. Tu parte a la hora de utilizar Rust es ajustar los parámetros del hechizo de modo que solo estés usando la cantidad de poder suficiente. Si descuidas eso tus mismos hechizos debilitarán toda tu magia.»*

### Tipos escalado 

Un tipo **escalado** representa un único valor. Rust tiene cuatro tipos de escalado primario: enteros, flotantes, booleanos, y caracteres. Lo que cabría esperar dentro de cualquier lenguaje de programación. Veamos su funcionamiento específico en Rust. 

#### Tipo Entero

Un entero es un número sin componente de fracción. En Rust además tenemos que definir si la variable es marcada o desmarcada. Cada variante puede ser marcada o desmarcada y esto determina su tamaño. Es decir, si una variable necesita tener ser marcada, o, dicho de otro modo, debe tener signo, entonces su tamaño abarca tanto números positivos como negativos. Por otra parte, los desmarcado o los que no requieren signos solo comprenden números positivos. Marcados y desmarcados respectivamente son declarados con `i` o `u`.

<span class="caption">Table 3-1: Integer Types in Rust</span>

| Almacenamiento  | Marcados | Desmarcados |
|---------|---------|----------|
| 8-bit   | `i8`    | `u8`     |
| 16-bit  | `i16`   | `u16`    |
| 32-bit  | `i32`   | `u32`    |
| 64-bit  | `i64`   | `u64`    |
| 128-bit | `i128`  | `u128`   |
| arch    | `isize` | `usize`  |

#### Tipo Flotante

Rust además tiene dos tipos primitivos de números flotantes. Estos son `f32` y `f64`que son para las arquitecturas de 32bits y 64bits respectivamente. Por defecto Rust tomara por defecto el tipo `f64`. Aquí un ejemplo de cómo funcionan los tipo flotantes. 

<span class="filename">Filename: src/main.rs</span>

```rust

fn main() {
    let x = 2.0; // f64

    let y: f32 = 3.0; // f32
}
```

## Más que numeros

*«Hablemos un poco de los elementos que pueden componer a tus hechizos; uno de ellos como comprobaste con el libro pueden ser números, ¡pero ni de lejos está limitado a ese tipo! También servirán letras, palabras, incluso conceptos como verdadero o falso. Cuando entiendas todas estas podrás crear sortilegios bastante más complejos.»*

*Sigues leyendo.*


#### El tipo Boolean

Como en los otros lenguajes de programacion, un tipo Boolean en Rust tiene dos valores posibles: `true` y `false`. Este tipo de variable es especificada usando `bool`. Por ejemplo:

```rust
fn main() {
    let t = true;

    let f: bool = false; // declarado con la anotacion explicita del tipo
}
```

## Verdadero o Falso

*Ahora te encuentras trabajando con un tipo diferente de variable mágica. Te parecía cuanto menos curioso que solo podían ser dos cosas diferentes: Verdadero o Falso.*

*«Con el verdadero y falso podrás diseñar hechizos mucho más inteligentes y con efectos diferentes dependiendo de la situación. Podrás usarlos más adelante, mientras tanto trata con algo diferente, trata con símbolos.»*


#### The Character Type

Hasta ahora hemos trabajado solo con números, sin embargo Rust soporta caracteres tambien. `char` es el tipo alfabético más primitivo. El siguiente código muestra una manera de utilizarlo:

 <span class="filename">Filename: src/main.rs</span>

```rust
fn main() {
    let c = 'z';
    let z = 'ℤ';
    let nya = '😻';
}
```
### Tipo Compuesto

Los tipo compuesto pueden agrupar múltiples valores en una sola variable. Rust tiene dos tipos compuestos primitivos: tuples y arrays.

#### The Tuple Type

Un tuple es un medio general para agrupar un número de valores de diferentes tipos en un solo tipo compuesto. Creamos un tuple escribiendo una lista de valores separada por comas dentro de un paréntesis. 

<span class="filename">Filename: src/main.rs</span>

```rust

fn main() {
    let tup = (500, 6.4, 1);

    let (x, y, z) = tup;

    println!("The value of y is: {}", y);
}
```

#### El Tipo Array

Otro forma de tener colecciones de de múltiples valores es el ya tan conocido tipo array. Muy a diferencia del tuple, cada elemento del array debe ser del mismo tipo. En Rust, los valores van escritos separados por comas dentro del array dentro de paréntesis. 

<span class="filename">Filename: src/main.rs</span>

```rust
fn main() {
    let a = [1, 2, 3, 4, 5];
}
```

Para más detalles de los array y los tuples consultar la documentación oficial de Rust.En nuestro caso seguiremos ya que nuestra prioridad no es el estudio detallado de este lenguaje. 

## Cadenas

*No tienes idea de cuánto tiempo ha pasado, intuyes que mucho. Te comienzas a sentir algo aturdido ante las grandes cantidades de información, pero al mirar al frente logras ver orgulloso tus cadenas de números. El poder almacenar muchas cosas iguales en una variable mágica debía de ser muy útil.*

*«Mmmm, vas rápido. Las cadenas te permitirán producir hechizos más rápidos y que tomen en cuenta distintos tipos de variables sin que tengas que pensar en cada una.»*

*Miras a Cargo de frente y este parece notar tu desconcierto. Su conexión es cada vez más poderosa, es como si fuera capaz de leer tu propia mente.*

*«Lo entiendo, te preocupa que a la vez que estás haciendo hechizos más complejos te resulta más incómodo a cada momento. Tengo la solución.»*

*Un tomo de cuero comienza a sobrevolar por encima tuyo hasta descender y apoyarse en tu mesa de estudio. Se abre en una página muy específica. Sonríes complacido ante la solución de Cargo.* 


## Funciones

Las funciones engloban todo el código en Rust. Hemos visto ya una de las funciones más importantes en el lenguaje: la función `main`, que es el punto de entrada del programa. También habrás visto la palabra clave `fn`, que te permite declarar las funciones. El código en Rust usa el formato serpiente como el estilo convencional para las funciones y variables. Esto quiere decir que todas las letras deben ser minúsculas y estas separadas por un guión bajo. La excepción son las constantes que son escritas en mayúsculas.

<span class="filename">Filename: src/main.rs</span>

```rust

fn main() {
    println!("Hello, world!");

    another_function();
}

fn another_function() {
    println!("Another function.");
}
```

Para definir una función en Rust, se comienza con `fn` y tiene un par de paréntesis luego del nombre de la función. Las llaves le indican al compilador dónde comienza el cuerpo de la función y donde termina. Podemos llamar cualquier función que hayamos definido digitando su nombre y los paréntesis subsiguientes. Porque `another_function` es definido en el programa, puede ser llamado dentro de la función `main` en el código fuente.

```text
$ cargo run
   Compiling functions v0.1.0 (file:///projects/functions)
    Finished dev [unoptimized + debuginfo] target(s) in 0.28s
     Running `target/debug/functions`
Hello, world!
Another function.
```

### Paremetros en las Funciones

Las funciones en Rust, tal como en cualquier otro lenguaje tiene parámetros, que son variables especiales que son parte de la misma función. La siguiente versión reescrita de `another_function` muestra cómo funcionan: 

<span class="filename">Filename: src/main.rs</span>

```rust
fn main() {
    another_function(5);
}

fn another_function(x: i32) {
    println!("The value of x is: {}", x);
}
```

Corriendo este programa deberíamos tener la siguiente salida de datos: 

```text
$ cargo run
   Compiling functions v0.1.0 (file:///projects/functions)
    Finished dev [unoptimized + debuginfo] target(s) in 1.21s
     Running `target/debug/functions`
The value of x is: 5
```

### Valores de retornados. 

Todas las funciones de Rust retornan el último valor figurado en la estructura, exceptuando las declaraciones. Dicho de otro modo, esto no retornaria ningún valor:  

```rust
fn main() {
    let y = 6;
}
```

<span class="caption">Listing 3-1: A `main` funcion main donde el último término es una declaración de variable.</span>

Esto por otra parte en efecto retornaria algo. 

```rust
fn main() {
    let y = 6;
    y;
}
```

<span class="caption">Listing 3-2: A `main` funcion main donde el último término no es una declaracion y es retornado</span>

Para ver en detalle las funciones, consulta su muy completa sección al respecto en el Rust Book. Nosotros nos concentramos en sólo en lo esencial.

## Control Flow

Naturalmente también podemos decidir cuándo un código es corrido o no dentro de un condicional. Por ejemplo en un condicional if si la condición es verdadera se ejecutará el bloque de código que encierra el caso `true`. No nos detendremos mucho en el uso de los condicionales ya que su uso en Rust es completamente igual al resto de los lenguajes. Para controlar el flujo de nuestro programa tendremos cinco maneras diferentes dependiendo del caso: if, loop, while, for y match que es quizas el unico que no reconozcas; será abordado más adelante.

### Expresión `if`

La expresión `if` permite ramificar el código dependiendo de las condiciones. Tendrás que proveer una condición y su estado. “Si la condición es cumplida, corre este bloque de código. Si la condición no cumple, no corras este bloque de código.”  Creemos un nuevo proyecto para explorar esta función. 


```rust
fn main() {
    let number = 3;

    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
}
```
Además podemos incluir opcionalmente una expresión `else`, con la que decidiremos qué bloque de código se ejecutará en caso de que no se cumpla el `if`. Trata de correr este código; deberías tener este resultado.
 
```text
$ cargo run
   Compiling branches v0.1.0 (file:///projects/branches)
    Finished dev [unoptimized + debuginfo] target(s) in 0.31 secs
     Running `target/debug/branches`
condition was true
```
Si cambiamos el valor de `number` a un valor que haría la condición ser falsa tendríamos: 

```text
 $ cargo run
   Compiling branches v0.1.0 (file:///projects/branches)
    Finished dev [unoptimized + debuginfo] target(s) in 0.31 secs
     Running `target/debug/branches`
condition was false
```

También podemos manejar múltiples condiciones con `else if`:

```rust
fn main() {
    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
}
```

```text
$ cargo run
   Compiling branches v0.1.0 (file:///projects/branches)
    Finished dev [unoptimized + debuginfo] target(s) in 0.31 secs
     Running `target/debug/branches`
number is divisible by 3
```

## Condiciones

*Tocas una vez las mesa y entonces el libro salta, la tocas dos veces para que se abra en la página que especificaste, finalmente, tocas la mesa tres veces para que se cierre nuevamente. Ahora con el poder de dividir los hechizos en pequeños bloques de información has aprendido a realizar sortilegios complejos. Los condicionales forman gran parte del proceso al darte la capacidad de configurar tu magia de tal modo que reaccione a reglas del entorno.*

*«Bien, bien, si sigues así con los condicionales prontos podremos pasar a los ciclos para que puedas dejar tu magia funcionando indefinidamente.»*

*Asientes, al igual que Cargo, te sientes complacido por todo lo que has logrado. Con esta nueva habilidad puedes no solo activar efectos específicos según lo que se necesite, sino también puedes comparar y experimentar. Te encantaría poder quedarte un poco más practicándolo, pero el tiempo apremia, pasas al siguiente libro.*


### Repetición con Loops

Es más que usual ejecutar bloques de código más de una vez. Para esta tarea, Rust provee varios tipos de loops. Un loop se encarga de que todo el código que encierra se ejecute una y otra vez de forma inmediata. Hagamos un nuevo proyecto con ```cargo new``` para ilustrar esto mejor. 

#### Repitiendo código con `loop`

La palabra clave `loop` le dice a Rust que ejecute el bloque de código repetidamente para siempre o hasta que explícitamente le digas que se detenga. 

```rust
fn main() {
    loop {
        println!("again!");
    }
}
```
Cuando corramos este programa en la consola, veremos `again!` imprimiendose continuamente hasta que paremos el programa. La mayoría de las terminales soportan la combinación de teclas, ctrl-c, para interrumpir el programa que se habrá quedado atascado en el loop. ¡Démosle un probada!

```text
$ cargo run
   Compiling loops v0.1.0 (file:///projects/loops)
    Finished dev [unoptimized + debuginfo] target(s) in 0.29 secs
     Running `target/debug/loops`
again!
again!
again!
again!
^Cagain!
```
El símbolo `^C` representa donde presionas ctrl-c. Algo incómodo, ¿Verdad? Afortunadamente, Rust tiene una forma algo más apropiada de terminar el loop sin que nosotros tengamos que intervenir. Puedes posicionar la palabra clave `break` dentro del loop para decirle al programa cuando debe de dejar de repetirse. Esto lo usaremos más adelante.

 #### Retornando valores de los Loops. 

Uno de los usos de `loop`es repetir una operación que sepas que pueda fallar, cómo comprobar el progreso y asegurar que haya completado su tarea. Sin embargo, puedes querer pasar el resultado de la operación al resto de tu código. Para hacer esto, puede agregar el valor que quieres retornar después de `break` cuando detienes el loop; ese valor será retornado fuera del loop, así que puedes usarlo. 

```rust
fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {}", result);
}
```

Después del loop, declaramos una variable llamada `counter`y la inicializamos en `0`. Entonces declaramos la variable con el nombre de `result`para mantener el valor retornado del loop. En cada iteración agregamos `1` a la variable `counter`, y entonces revise el estado de contados hasta que llegue a `10`. Cuando eso suceda, nosotros usaremos el `break` con el valor `counter *2`. Finalmente, nosotros usaremos `println!` para imprimir el valor de `result`, que en este caso es 20.

#### Loops condicionales con `while`. 

Muy importante también son los programas que evalúan un condicional dentro de un loop.  Mientras la condición sea cierta, el loop sigue. Cuando la condición deje de ser cierta, el programa llama a `break` para detener el ciclo. Se podría decir que esto es la combinación entre `loop`, `if`, `else` y `break`; puedes hacer la prueba en el programa si quieres. 

No obstante, este patrón es tan común que los lenguajes de programación, no solo Rust, tiene un constructor para ello, llamado `while`. Aquí un ejemplo: 

```rust
fn main() {
    let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}
```
De este modo eliminar los innecesarios pasos que tendríamos que tomar en el caso de que quisiéramos usar ```loop```. 

#### Repitiendo a través de colecciones con `for`

Podrias usar `while`para construir un ciclo sobre elementos de una colección, como un array. Veamos este rápidamente. 

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }
}
```
Aqui, el código cuenta en adelante hasta el número de elementos en el array. Comienza en el index `0`, y entonces se repita hasta alcanzar el index final en el array (esto es cuando `index < 5` ya no es cierto). El resultado debe de ser el siguiente: 

```text
$ cargo run
   Compiling loops v0.1.0 (file:///projects/loops)
    Finished dev [unoptimized + debuginfo] target(s) in 0.32 secs
     Running `target/debug/loops`
the value is: 10
the value is: 20
the value is: 30
the value is: 40
the value is: 50
```

Todos los cinco valores del array aparecen en la terminal como se esperaba. Incluso si `index`alcanzará el valor `5` en algún punto, el loop dejará de ejecutarse luego de tratar registrar un sexto valor en el array.  Una alternativa más concisa en la que puedes usar `for` y ejecutar código por cada ítem en la colección sería la siguiente: 

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }
}
```

## Mozilla

*Eventualmente sales de la torre. ¿Ha cuantas habias subido ya? A muchas sin duda alguna. Mientras te estiras un poco miras a tu alrededor y te das cuenta de un detalles minúsculo.
No había más torres. Ni siquiera de la que habías salido hace un momento.*

*Te rodea una imperturbable planicie, solo tú, Cargo, tú y por supuesto la figura encapuchada en frente de ti… ¡La figura enfrente de ti! Rápidamente te pones en alerta desenfunda tu arma. La figura no se mueve. Te limitas a inclinar la cabeza y enmarcar una ceja al darte cuenta de otro detalle; no tienes menor idea de esgrima.*

*La figura comienza a reír a carcajadas mientras se descubria la cara hasta ese momento oculta.*

*─ ¡Eso fue simplemente único e inolvidable! ─Dijo entre risas.─ Gracias por esa maravillosa expresión.*

*» Permíteme presentarme, soy Mozilla, quien te enseñara las cosas verdaderamente importantes y de valor.*

*Cargo bufo, ¿podia bufar? Te pareció de lo más extraño. A la luz flotante no parecía agradarle nada la anciana. Finalmente habló:*

*«Sin mi nada podrías enseñarle, lo he preparado para que sea capaz de comprenderte»*

*Lo escuchabas solo en tu cabeza, pero asumes que Mozilla también podía hacerlo.*

*─ ¡Lo que digas lucecita! Veo la confusión en tus ojos, mi joven héroe, déjame explicarte. En esencia yo me encargare de enseñarte lo que viene a continuación, estudiaremos unas buenas prácticas más allá de los conocimientos de Cargo, y además, tengo unas cuantas cosas que enseñarte yo misma de Rust. Comenzaremos con explicarte como funciona el propio flujo de la magia, llamado Ownership ¡No nos demoremos!*

*Y no se demoró.*


## En resumen

¡Lo hicimos! Ha sido un entrenamiento muy arduo incluso para ti, héroe de la descentralización. Hemos aprendido acerca de variables, tipos de data, funciones, condicionales e incluso loops… Recuerda sin embargo que si quieres profundizar aún más tus conocimientos en Rust y en los temas dados en esta sección, esta guia no dejará de recomendarte la guia oficial donde los mismos desarrolladores te orientaran mejor que nadie en las artes místicas de Rust. 

Ahora nos moveremos a la última sección antes de nuestra primera actividad; Como Rust gestiona su memoria, el Ownership. De las funcionalidades más poderosas del lenguaje. 




