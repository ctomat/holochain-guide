# Cargo 

Es buen momento para conocer a nuestro acompañante en esta aventura. Cargo no solo es un gestor de paquetes, también es un constructor de sistemas que automatiza muchos procesos para que usted, gran héroe, pueda enfocarse exclusivamente en la parte del desarrollo. Cargo es capaz de encargarse de tareas tales como compilar el código, descargar las librerías de las cuales tu programa depende y configurarlas. 

Hasta el momento, el programa de Rust realizado no tiene ninguna dependencia, o sea, no hemos llamado ninguna librería. Esto quiere decir que si armamos el proyecto con Cargo, el programa unicamente utilizada la parte de Cargo en donde compila nuestro código. A medida que realices programas más complejos en Rust, irremediablemente se requerirán dependencias.  Usando Cargo este proceso es mucho más sencillo. 

En adelante utilizaremos Cargo para todos los ejemplos hechos en Rust por las comodidades que da. Si hiciste la instalacion normal de Rust, Cargo ya debe estar instalado en el sistema y listo para para ser usado. Si lo hiciste por otro medio consulta la página web donde podras encontrar solución a cualquier problema que se te presente. Sin embargo, vamos a comprobar de primera mano si Cargo se encuentra instalado o no. Ingresamos el siguiente comando. 

```text
$ cargo --version
```

Si la terminal o el CMD responden con un numero de version entonces con certeza el sistema tiene Cargo en la versión correspondiente de la instalación de Rust. En caso contrario, la consola lanzará un mensaje de “comando no encontrado” o similar. 

## Por Instinto 

*Te encuentras en una de las torres más próximas al centro dentro de la extensión poblada de aquellas estructuras en forma de picos. Habiéndote encaminado hacia lo alto de la torre te encuentras con más y más libros los cuales cargó te ayuda a comprender y estudiar. Toma tiempo, más del que sería de tu agrado, pero con tu creciente convicción logras dibujar en el aire un pequeño patrón de luz que reza «Hola Mundo» tal como si fuera respirar. Esto te saca una sonrisa y adviertes visible satisfacción de Cargo mientras revolotea alegremente.*

*«¡Excelente! Entiendes de manera casi intuitiva como funciona tu poder, a cualquier otra persona le hubiera tomado mucho más tiempo lograr lo que acabas de conseguir. Permíteme ilustrarte mejor sobre la naturaleza teórica de lo que acabas de hacer.»*

*Percibes que la conexión entre Cargo y tú se vuelve más intensa.*

### Usando Cargo

¡Que la magia comience! 

![alt text](https://media.giphy.com/media/azGJUrx592uc0/giphy.gif "Logo Title Text 1")

Vamos a pedirle ayuda a Cargo para que un nuevo proyecto en Rust, así veremos cómo difiere a hacerlo manual con respecto a nuestro primer ejemplo de hola mundo. A través de la consola, nos dirigimos a la carpeta de proyectos (el cómo hacer esto ya está en la sección anterior, no dudes en visitarla si tienes dudas) y entonces daremos la orden que correr al sistema:

```text
$ cargo new hello_cargo
$ cd hello_cargo
```

Con el primer comando no solo crearemos un nuevo directorio llamado “hello_cargo”, tambien le estamos dando nombre a nuestro proyecto donde cargo creará sus archivos. Echemos un  ojo al nuevo directorio; podemos hacerlo listando los archivos a través de la terminal o con la interfaz gráfica. Si todo ha salido bien, Cargo habrá generado dos archivos  y un directorio para nosotros. 

También habrá iniciado  un nuevo repositorio de Git con un archivo .gitignore. Si no sabes mucho de git, es buen momento para aprender, es una herramienta invaluable que servirá a lo largo de la guia. Abriremos Cargo.toml en nuestro editor de codigo de preferencia. Deberia verse similar al código del Listing 1-2. 

```toml
[package]
name = "hello_cargo"
version = "0.1.0"
authors = ["Your Name <you@example.com>"]
edition = "2018"

[dependencies]
```
<span class="caption">Listing 1-2: Contenido de *Cargo.toml* generado por `cargo
new`</span>

Este poderosísimo archivo determina el formato de configuración de Cargo. La primera línea [package], es el encabezado de la seccion que indica datos básicos de la aplicación tales como el nombre que le pusimos, su versión actual, el autor y su edición. Lo verdadera interesante es la última línea; [dependencies] es el comienzo de una completa sección donde se listará todas las dependencias del proyecto. En Rust, los paquetes de código tienen el nombre de “crates”. Ahora mismo no necesitamos ningún crate pero lo utilizaremos en breves. 

Ahora revisemos el src/main.rs y echemos un vistazo. 

<span class="filename">Filename: src/main.rs</span>

```rust
fn main() {
    println!("Hello, world!");
}

```
Cargo por sí mismo genero un “Hello, world!”. Es exactamente igual a lo que escribimos en la sección anterior de la guia. La unica diferencia es que aqui, el main.rs fue generado dentro de la carpera “src”. Si comenzaste un proyecto sin Cargo, sin problema alguno puedes convertirlo en un proyecto que sí lo use. Solo hay que mover el .rs a la carpeta “src” y crear un apropiado Cargo.toml.

Siempre podremos compilar nuestro código de Rust a través del comando utilizado en la sección pasada. Cargo sin embargo también pueda hacerlo por nosotros pero con la diferencia clave que no compilara el código antes de comprobar que no hay errores y ubicar cualquier mala práctica. Si una función que por ejemplo sea llamada así: “esta-es-una-función” será ubicada por el compilador y se imprimirá en la consola que la funcion deberia ser escrita asi: “esta_es_una_funcion”.

Para compilar con Cargo escribimos en la consola `cargo build`

```text
$ cargo build
   Compiling hello_cargo v0.1.0 (file:///projects/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 2.85 secs
```
Este comando crea el archivo ejecutable en  **target/debug/hello_cargo** (o **target\debug\hello_cargo.exe** en Windows). Puedes correr el ejecutable con este comando. 

```text
$ ./target/debug/hello_cargo # or .\target\debug\hello_cargo.exe on Windows
Hello, world!
```

Si todo ha ido según lo esperado, nuevamente se mostrará el mensaje de “Hello, world!” en la terminal. Correr por primera vez el comando de `cargo build` producirá que Cargo además cree un nuevo archivo llamado: **Cargo.lock**. Este archivo le lleva seguimiento a la versión exacta de las dependencias del proyecto. Esto se explicará más adelante, pero es un hecho que jamás tendrás que cambiar algo en dicho archivo; Cargo siempre lo hara por ti. Con Cargo también podemos compilar y correr el programa con una sola orden.

```text
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/hello_cargo`
Hello, world!
```

Si te fijas en la segunda línea, no dice que está compilando como cuando usamos `cargo build` esto se debe a que Cargo detectó que no ha habido cambios en el código y por lo tanto no es necesario volver a compilarlo. De este modo procede directamente a ejecutar el programa. ¿Y qué ocurre cuando solo queremos chequear en caso de errores o advertencias del compilador? En ese ese caso se utiliza `cargo check`.

```text
$ cargo check
   Checking hello_cargo v0.1.0 (file:///projects/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.32 secs
```
Por último, para trabajar en cualquier proyecto existente, puedes usar los siguientes comandos para comprobar clonar el repositorio de git y descargar las dependencias necesarias. 

```text
$ git clone someurl.com/projectname
$ cd projectname
$ cargo build
```
Para más información de Cargo, puedes revisar la documentación oficial de Cargo.

https://doc.rust-lang.org/cargo/

## Compañero

*Descubres que al igual que tú, Cargo puede usar las artes místicas de Rust. No es solo capaz de hacerlo más rápidamente, no conforme con lo anterior, invoca los sortilegios con mayor eficacia. Conoce la mejor manera de llevar a cabo cada movimiento y la entonación precisa de las palabras. Sin embargo, te explica que hay una limitante.*

*«Siempre debes recordar que yo represento un complemento a lo que tú puedes hacer. Hago lo que hago únicamente porque tú eres capaz de hacerlo, por tanto, antes de que te pueda ayudar a mejorar tus hechizos o elaborarlos yo, tienes que darme órdenes claras para que juntos podamos encontrar la mejor forma de lograrlo.»*

*Asientes enérgicamente. Cargo hace un rodeo rápido entorno a ti soltando pequeñas chispas de energía pura.*

*«¡Eso es! Sigamos a la siguiente torre, ahí encontraremos los escritos que vamos a necesitar a continuación»*


## En resumen

¡Alas gran guerrero! En esta sección ya has dado el verdadero primera paso en tu camino con Rust. En este capítulo aprendiste: 

Instalar la última versión estable de Rust usando `rustup`
Actualizar hasta la más nueva versión de Rust. 
Programar y ejecutar un Hola Mundo usando `rustc`.
Crear y correr un nuevo proyecto usando las conveniencias de Cargo. 

