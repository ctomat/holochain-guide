# Enums y Matching

En este capítulo veremos las enumeraciones, también conocidas como enums. Los enums nos permite definir un tipo de data enumerando sus posibles variantes. Primero definiremos y usaremos un enum para mostrar cómo esta funcionalidad puede codificar significados de la mano con la data. Posteriormente explicaremos enums particularmente útiles, llamados `Options`, que expresa que un valor puede ser algo o nada. Luego exploraremos las funciones de `match` que nos facilita correr diferentes códigos para diferentes valores de un enum. Finalmente, cubriremos como el constructo `if let` es otra variación conveniente y concisa que podemos utilizar. 

Hagámonos con el siguiente escenario; Queremos expresar en código y ver porque los enums son útiles e incluso más apropiado que las estructuras en muchos casos. Digamos por ejemplo que necesitamos trabajar con direcciones IP. Actualmente dos estándares se están utilizando: versión cuatro y versión seis. Estas son las únicas posibilidades para una dirección IP. Podemos **enumerar** todas las posibles variantes, de ahí el nombre de nuestra nueva herramienta. 

Cualquier IP puede ser versión cuatro o versión seis, pero jamás las dos al mismo tiempo. Esta característica de las direcciones IP lo hace perfecto para los enums, porque los valores de estos son pueden ser una de sus variantes. Podemos expresar este concepto en el código definiendo un `IpAddrKind` enum (al igual que las estructuras, la primera letra de cada palabra va en mayúscula) para seguir los posibles tipos de dirreccion IP. Vamos al codigo. 

```rust
enum IpAddrKind {
    V4,
    V6,
}
```

`IpAddrKind` es ahora un tipo de data personalizable que podemos usar en cualquier parte de nuestro código. 

## Silencio

*Más pronto que tarde consigues llegar al templo. En efecto era grande, muy grande. Tenía un símbolo en el marco superior de la colosal puerta. Exactamente el mismo símbolo que llevas en tu peto. Lo tocas tratando de sentir alguna conexión. Entonces, determinado decides entrar. 
Pero antes Mozilla te detiene.*

*Ibas a preguntar la razón, pero ella rápidamente se lleva el índice a los labios. «¡Silencio!» gesticularon sus arrugados labios. Regresas la mirada al templo y por un breve instante adviertes una luz roja asomando por una de las ventanas. Vacilante das un paso atras.*

*─ Tengo una idea. ─Susurro Mozilla.─ Te voy a enseñar un pequeño truco para que puedas plantarle cara a esas cosas. Son mi creación, los llamo Enums y es una estructura para tus hechizos que los hará aún más potente.*

*Asientes impaciente y comienzas a aprender como siempre has hecho desde que llegaste a este lugar.* 


## Valores de Enum

Podemos crear instancias para cada una de las dos variantes de `IpAddrKind` de este modo: 

```rust
   let four = IpAddrKind::V4;
   let six = IpAddrKind::V6;
```

Nótese que la variante del enum utiliza la sintaxis del doble punto para separar su identificador que son `v4` y `v6`. La razon detras de esto es que ahora ambos valores `IpAddrKind::V4` y `IpAddrKind::V6` son del mismo tipo que `IpAddrKind`. Podemos hasta definir funciones que tomen cualquier `IpAddrKind`

```rust
fn route(ip_kind: IpAddrKind) {}
```

Por tanto podemos llamar esta función en cualquiera de sus variantes. 

```
  route(IpAddrKind::V4);
  route(IpAddrKind::V6);
```

Utilizar Enums tienen aún más ventajas. Sigamos con la linea de pensamiento de las direcciones IP; de momento no tenemos manera de almacenar verdera data de direcciones IP, solo sabemos el tipo que es. Podemos optar por las estructuras que aprendimos en la sección pasada. 

```rust
   enum IpAddrKind {
        V4,
        V6,
    }

    struct IpAddr {
        kind: IpAddrKind,
        address: String,
    }

    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1"),
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };
```

Aquí definimos una estructura `IpAddr` que tiene dos entradas: una entrada `kind` referida al tipo de la dirección que ya definimos con `IpAddrKind` y una entrada llamada `address` que almacena `String`. Por último, tenemos dos instancias de esta estructura. La primera es `home` que tiene el valor `IpAddrKind::V4` como el `kind` con una dirección de data asociada `127.0.0.1`. La segunda instancia, `loopback`, tiene otras variantes de `IpAddrKind` siendo `kind` del tipo `v6`, teniendo la dirección `::1` asociada. Es una solución sin duda. 

Pero también es cierto que hay una forma mucho más directa de hacer esto. Podemos representar exactamente el mismo concepto en un modo bastante más conciso usando enum. En lugar de un enum dentro de una estructura, podemos poner el tipo de data directamente dentro de cada variación del enum. Esta nueva definición del `IpAddr` nos dice que tanto `v4` cómo `v6` tendrán un valor `String` asociado. 

```rust
   enum IpAddr {
        V4(String),
        V6(String),
    }

    let home = IpAddr::V4(String::from("127.0.0.1"));

    let loopback = IpAddr::V6(String::from("::1"));
``` 

Tenemos otra gran ventaja que respecto a los enums sobre los estructuras, este es la flexibilidad de que podemos almacenar diferentes tipos y cantidades de data asociadas. Si no conoces mucho de redes, quédate con lo siguiente; `V4` siempre tendrá cuatro componentes numéricos con un valor numérico entre 0 y 255. Podemos almacenar las direcciones `V4` como cuatro `u8` pero aun expresando `V6` como un solo `String`. Esto jamás sería posible con las estructuras y los enums lo manejan fácilmente. 

```rust
   enum IpAddr {
        V4(u8, u8, u8, u8),
        V6(String),
    }

    let home = IpAddr::V4(127, 0, 0, 1);

    let loopback = IpAddr::V6(String::from("::1"));
``` 

Visto esto, podemos discernir que a pesar de que las estructuras y los enums siendo bloques de data con variables asociadas tienen enfoques muy diferentes y conveniencias únicas. Para este caso utilizamos el ejemplo de las direcciones IP porque es un ejemplo tan apropiado que **el mismo lenguaje dispone de una definición específica de enum para las direcciones IP.** Es decir, Rust tiene contemplado que sus enums son excelentes para esta tarea y nos facilita una forma aún más sencilla de definir estas direcciones. 

```rust
 enum IpAddr {
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}
```

En esencia, este código nos ilustra que podemos poner cualquier tipo de data dentro en una de las variantes del enum; `String`, numeros o estructuras. ¡Puedes incluso poner otro enum! Además, las librerías propias del lenguaje (como lo que estamos estudiando ahora mismo) suelen ser bastante fáciles de entender. No te desalientes en indagar héroe, descubrir los secretos detrás de Rust también es parte de tu misión. 

Es importante resaltar que aunque la librería contiene una definición para `IpAddr`, fuimos capaces de crear y usar nuestras propias definiciones sin conflicto alguno. Esto es gracias a que no hemos llamado la definición estándar de la librería dentro del scope. Pronto veremos como hacer esto, solo espera un poco, tenemos más que desentrañar de los enums. Veamos el siguiente ejemplo referido a usar diferentes tipos de data en el mismo bloque. 

```rust
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}
``` 

Este enum tiene cuatro variantes con diferentes tipos, los cuales son: 
- `Quit` no tiene data en absoluto, es lo más cercano a nulo. 
- `Move` tiene una estructura anónima (una estructura que naturalmente no tiene nombre) dentro. 
- `Write` incluye un único `String`.
- `ChangeColor` includes three `i32` values.

Definiendo un enum con variantes como el ejemplo anterior es similar a definir diferentes tipos de estructuras con la diferencia que todas las variantes están agrupadas dentro de `Message`. La siguiente estructura sería el sinónimo del ejemplo pasado pero prescindiendo de enums. 

```rust
struct QuitMessage; // unit struct
struct MoveMessage {
    x: i32,
    y: i32,
}
struct WriteMessage(String); // tuple struct
struct ChangeColorMessage(i32, i32, i32); // tuple struct
```

Esto virtualmente hace el mismo cometido, pero tenemos el problema de que es tedioso por no decir incómodo. Por otra parte, hacerlo de este modo nos predispone a usar funciones para poder trabajar con toda la data. Usando enums perfectamente podemos tener toda la información en lugar y trabajar con métodos. 

```rust
   impl Message {
        fn call(&self) {
            // method body would be defined here
        }
    }

    let m = Message::Write(String::from("hello"));
    m.call();
``` 

Recordemos que el cuerpo del método aprovechara `self` para obtener valores que llamemos desde el método y que correspondan al enum. Lo que estamos haciendo es declarar una variable `m` que usa la variante `Write` del enum, el cual solo puede recibir `String`. Desde ese momento, lo que hagamos con dicha data depende de nosotros. 

## Caminos

Invocas el Enum y creas los parámetros de tu visibilidad y las de tus acompañantes, entonces modificas el valor a ninguno. Rápidamente la magia hace su efecto y los hace invisibles. Estás muy feliz de que realmente haya funcionado. Lentamente te acercas a la puerta del templo y la empujas tratando de hacer el menos ruido posible. 

─ Tenemos que entrar de un modo u otro. ─Explicó Mozilla.─  Dentro hay unos tomos que solo tú puedes leer y sin ellos estaremos todos condenados. 

Miras el interior del templo. Toda su arquitectura era curvilínea y repleta de detalles en formas de espirales. El piso de mármol al igual que las columnas en forma de tornado. Una escalera está delante de ti y dos pasillos te flanquean de cada lado. Decides comenzar recorriendo el pasillo de la izquierda al notar una luz a tu derecha en el cruce que se da en el otro pasillo. 


## Las ventajas de `Option` sobre los valores nulos.

En uno de los ejemplos pasados, se mencionó que una variante del enum era lo más cercano a null. Esta forma de definirlo no es negligencia sino una realidad porque en Rust no existe el valor nulo o `null`. Vamos a explorar ahora los usos posibles del tipo `Option`, el cual es un tipo específico de enum proveniente de la librería estándar de Rust. 

El tipo `Option` es utilizado en muchas situaciones gracias a que codifica un escenario bastante común en donde un valor podría ser algo o podría ser nada. Hacerlo de este modo le permite al compilador saber todos los casos posibles que tiene manejar, permitiendo así prevenir errores que son preocupantemente comunes en otros lenguajes de programación. Este error se repite con frecuencia porque los diseñadores del lenguaje están centrados en las funcionalidades que incluyen más allá de las que excluyen. 

El problema con los valores nulos es que si lo tratamos con un valor que no es nulo, lo más seguro es que tendrán no uno, sino varios errores. Las propiedades nulas y no nulas pueden irrumpir fácilmente con otras partes del programa hasta el punto de crear brechas de seguridad. Además, es muy fácil cometer errores. 
No obstante, el puro concepto de null es innegablemente útil: un valor nulo es un valor que es actualmente invalido o ausente por alguna razón. No es una discusión sobre el concepto sino más sobre la implementación. Rust no tiene nulos, pero tiene un enum que encapsula ese concepto de un valor estando presente o ausente. Vamos a verlo:

```rust
enum Option<T> {
    Some(T),
    None,
}
``` 

No se le da el suficiente aprecio a `Option<T>` por lo util que es. No es necesario traerlo al scope explícitamente, nada de llamamiento de librerías. Incluso podemos usar las dos variaciones que son `some` y `none` sin la necesidad de utilizar el prefijo `Option::`. La sintaxis de `<T>` es un parámetro genérico que de momento solo tienes que saber que la variante de `Some` dentro de `Option` puede almacenar cualquier pedazo de dato de cualquier tipo. Lo vemos aquí: 

```rust
   let some_number = Some(5);
    let some_string = Some("a string");

    let absent_number: Option<i32> = None;
``` 

Si usamos `None` en lugar de `Some`, es necesario decirle a Rust qué tipo de `Option<T>` tenemos, en caso contrario el compilador no podrá saber qué tipo de data tendrá su variación `Some` solo con `None`. De momento, no obstante, luce como si estuviéramos haciendo lo mismo que si utilizaramos null. Cuando es el valor `Some`, sabemos que el valor está presente y está almacenado en el susodicho. Por otra parte, con `None`, en cierto modo, significa lo mismo que null. Por tanto, ¿cuales son las ventajas por encima del null corriente?

En resumen, porque `Option<T>` y `T` (Donde `T` es cualquier tipo de data que hemos estudiado) son diferentes tipos, el compilador no nos permitiría usar a `Option<T>` como si fuera un valor válido definitivo. Un i32 no es lo mismo que un Option<i32> y por tanto no se pueden realizar operaciones entre ellos. Este ejemplo con toda certeza no compilara.  

```rust
   let x: i8 = 5;
 let y: Option<i8> = Some(5);

 let sum = x + y;
```

Aquí vemos el error. 

```
$ cargo run
   Compiling enums v0.1.0 (file:///projects/enums)
error[E0277]: cannot add `std::option::Option<i8>` to `i8`
 --> src/main.rs:5:17
  |
5 |     let sum = x + y;
  |                 ^ no implementation for `i8 + std::option::Option<i8>`
  |
  = help: the trait `std::ops::Add<std::option::Option<i8>>` is not implemented for `i8`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `enums`.

To learn more, run the command again with --verbose.
``` 

¡Estos poderes arcanos son cada vez más interesantes! En efecto, el error del compilador no confirma que Rust no entiende la suma entre estos dos tipos de data, porque son diferentes. Es exactamente como tratar de multiplicar un entero con un string. Cuando tenemos un tipo de valor como `i8` en Rust, el compilador se asegurará de que siempre estemos usando su contenido. Podemos proceder confiados sin tener que revisar si es nulo o no antes de llamarlo porque no es posible tal caso. Únicamente cuando tenemos `Option<i8>` (o cualquier tipo de valor con el que estemos trabajando) tenemos que preocuparnos de la posibilidad de usar una variable que no tiene valor alguno, y aun así el compilador cuidará de que manejemos esta posibilidad antes de usar el valor. 

Dicho de otro modo, tenemos que convertir `Option<T>` a `T` antes de ser capaces de realizar operaciones `T` con él. Generalmente, esto ayuda a prevenir uno de los problemas más comunes con null, asumiendo claro, que algo no es null cuando realmente lo es. 

Al no tenernos que preocupar por incorrectamente asumir un valor no nulo ayuda a ser mucho más seguros en nuestro código. Para tener un valor que pueda llegar a ser nulo, nosotros tenemos que hacerlo manualmente y declararlo de forma muy explícita con la sintaxis de `Option<T>`. En cualquier parte donde un valor sea de un tipo excluyendo `Option<T>`, podemos asumir de forma segura que ese valor no es y jamás será nulo. 

Esto nos hace preguntar, ¿cómo podemos obtener el valor `T` de una variación `Some` cuando tenemos un valor de tipo `Option<T>` de forma que podamos usarlo? El enum `Option<T>` tiene una muy extensa lista de métodos que son muy útiles en situaciones variables. Esto siempre lo podemos revisar en la documentación aquí. Ni de lejos hablaremos de todos los métodos pues no es nuestra prioridad pero vamos hablar de cierta expresión vital para los enums. Dicho esto, es recomendable, héroe, que te familiarices con tus grandes poderes que se te harán sumamente útiles en tu viaje con Rust. 

En general, para usar el valor `Option<T>`, queras un código que pueda manejar cada variación. Además, querrás un código que solo funcione cuando tengamos un valor Some(T) y que nos permita usar el interno `T`. Necesitarás otro código que corra si tienes un `None`, y que ese código no tenga un valor `T` disponible. Para todo esto existe la expresión `match`, constructor controlador de flujo (o control flow). Esta expresión ejecutará diferente código dependiendo en cual variante del enum se encuentra,  el código por supuesto puede utilizar la data dentro de `match`.

## Comparar

Llegas finalmente a una habitación sobrenaturalmente grande y laberíntica llena de pasillos y puertas. Decides explorar a profundidad el extraño sitio. Pasa el tiempo y te sientes cada vez más perdido. No parece que vayas a ir a ninguna parte solo andando. 

─ Héroe, para algo aprendiste Rust, creo que hay cierto tipo de sortilegio que te puede servir. 
Mozilla callo esperando a ver si tú mismo logras dar con la respuesta. La idea cruzó por tu mente como un relámpago seguido de un estruendoso trueno compuesto de inspiración. No era necesario comprobar cada pasillo y puerta. Solo había que compararlas para conocer el camino correcto. 
Match era la respuesta. 


## El controlador de flujo `match`

Rust posee un extremadamente poderoso control de flujo llamado `match` que nos permite comparar un valor con una serie de patrones y entonces ejecutar código basado en con qué patrón concuerda. Los patrones pueden estar compuestos de valores literales, nombres de variables y muchas otras cosas que pueden ser estudiadas en la documentación oficial del lenguaje. Aquí el capítulo específico de la documentación de Rust si quieres indagar inmediatamente antes de seguir. 

El poder de match viene de lo expresivo que pueden llegar a ser sus patrones y que el compilador asegura que todos los posibles casos serán manejados. En `match` todos los valores atraviesan cada patrón y el primero que cumpla es el bloque de código que será ejecutado por el programa durante su corrida. ¡Hagamos una prueba con monedas para apreciar el poder de `match`! Podemos escribir funciones que puedan tomar una moneda desconocida y, en un modo similar a una máquina contadora de monedas, determinar qué moneda es y retornar el valor en céntimos. 

```rust
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}

``` 
Veamos esto por partes para captar cada pequeño detalle. Primero, listamos las palabras claves dentro de `match` seguido por una expresión, la cual en este caso es el valor `coin`. Esto luce bastante similar a una expresión utilizada con `if`, la expresión necesita retornar un valor booleano, pero aquí, puede ser cualquier tipo. El tipo de `coin` en este ejemplo es el enum `Coin` que definimos al principio. 

Luego tenemos los diferentes caminos de `match` o secciones. Las secciones tienen básicamente dos partes: el patrón y el código. La primera sección aquí tiene un patrón que su valor es `Coin::Penny` y entonces el operador `=>` que separa el patrón del código a ejecutar. El código en esta caso es solo el retornado del valor `1`. Cada sección es separada por la siguiente coma. 

Cuando la expresión `match` es ejecutada, se compara el valor resultante con el patrón de cada sección, en orden. Si el patrón concuerda con el valor, el código asociado con ese patrón es ejecutado. Si ese patrón no concuerda con el valor, la ejecución continúa con la siguiente sección. Podemos tener tantas secciones como necesitemos. El código asociado con cada sección es una expresión, y el valor resultante de dicha expresión en la sección concordante es el valor retornado del `match` entero. 

Las llaves no son utilizadas típicamente si el código de cada sección dentro del `match` es corto. Tal es el caso de nuestro actual ejemplo donde el código es solamente el retorno de un valor. Si necesitamos ejecutar múltiples líneas de código en una sección del `match` entonces podemos aprovechar las llaves. Dando un caso, `Coin::Penny` puede no solo retornar el valor `1`, sino que además puede imprimir “Lucky penny!” cada vez que es ejecutada su sección. 

```rust
 fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}
```

## Patrones y valores anclados

Otra utilidad bastante versátil de las secciones `match` es que pueden tener ancladas partes de valores que concuerden con el patrón. No te alarmes, sé que esa última explicación pudo no ser nada esclarecedora. Pongamos de ejemplo las monedas de un cuarto de Estados Unidos, ahí dependiendo de cada estado dicha moneda es diferente. ¿Es conveniente programar una sección para cada estado si lo queremos tener en cuenta? Para nada, para eso esta esta funcionalidad. 

```rust
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}
```

En el código dentro de la sección de `Coin::Quarter` agregaremos una variables llamada `state`. Cuando `Coin::Quarter` se ejecute, es decir, concuerde, la variable `state` se anclará a la sección que ahora podrá usar ese valor. En el código resulta más fácil de ver.

```rust
fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}
```

Si llamamos `value_in_cents(Coin::Quarter(UsState::Alaska))`, `coin` realmente sera `Coin::Quarter(UsState::Alaska)`. Cuando comparamos el valor con cada una de las secciones, ninguna de las opciones concordara hasta que alcancemos `Coin::Quarter(state)`. En este punto, el anclaje para `state` será el valor `UsState::Alaska`. Podemos entonces usar el anclaje en la expresión `println!` y así tener no solo el valor de la moneda sino también su estado de procedencia. 

Vaya, estamos estudiando muchos conceptos seguidos que no son precisamente fáciles, ¡no te rindas héroe! Estamos muy cerca de poder concluir el aprendizaje básico de Rust. Ahora emplearemos junto a `match`un concepto que aprendimos recientemente y que en conjunto nos dan una poderosa funcionalidad. Aprovecharemos `match` para explotar todo el potencial de `Option<T>`.

## `Match` con `Option<T>`

En la sección donde explicamos `Option<T>`, se asomo la idea de obtener el valor interno de `T` del `Some` usando `Option<T>`. Con `match` podemos manejar `Option<T>` así como hicimos con el enum de las monedas. En este caso, sin embargo, en lugar de comparar monedas, compararemos las diferentes variaciones de `Option<T>`, pero de un modo que las expresiones de `match` retornen lo mismo. 

Digamos que queremos escribir una función que tome un `Option<i32>` y, si no hay un valor dentro, la función debería retornar el valor `none` y no tratar de realizar operación algunas. Elaboremos una función que en sí misma es bastante simple gracias al poder de `match`.

```rust
   fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            None => None,
            Some(i) => Some(i + 1),
        }
    }

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
``` 

Examinemos esto al detalle. A primera vista luce confuso, pero es lo más sencillo que hemos hecho durante el transcurso de esta guía. Si examinamos la declaración función `plus_one` notaremos que; primero, recibe un valor x de tipo `Option<i32>`; segundo, retornara un valor del mismo tipo. Entonces comienza el `match` con dos secciones bastantes claras: en caso de que x sea igual a `none`, retornara el mismo exacto valor, por otra parte, si el valor es `Some` que en el ejemplo es cualquier número, se retornara el número con un valor sumado de `1`.

Las siguientes variables no son otra cosa más que el provecho que podemos sacar de esta nueva función. Con `five` declaramos un `Option<i32>` donde el `i32` o `T` es `5`. `six` por otra parte, es una variable que almacena el valor retornado de nuestra función `plus_one`, a la que le enviamos `five` y por ende, al ser algo o `Some`, se nos debería retornar nuestro valor `5` más `1`, o sea, `6`. 

`none` como es lógico, busca ejecutar la otra sección de nuestro match, donde únicamente lo que sucederá es que se nos retornara el mismo `none` que enviamos a la función. Lo emocionante de esto último es que el programa no pierde el tiempo comparando las otras expresiones, es rápido y certero; sabe que en el momento que una de las secciones se dispara, no debe ejecutar lo demás. Otra de las razones por las que los programas que elaboremos en Rust estarán tan bien optimizados. 

Unir `match` y enums es útil en muchas situaciones.  Veremos estas combinaciones muchas veces en el código hecho con Rust: `match` con enums, variables ancladas a la data interna, y el código ejecutado basado en este hecho. Nuevamente, si se te hace un poco enrevesado, recuerda que es tu primera toma de contacto con estos conceptos que rozan lo excéntrico si tomamos de punto de comparación otros lenguajes de programación. 

## `Match` es exhaustivo. 

Un par de aspectos más que tenemos que hablar. Tomemos esta versión de nuestra función `plus_one` que tiene un error y con seguridad no compilara. 

```rust
   fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            Some(i) => Some(i + 1),
        }
    }
```

¿Lo ves? En este caso no hemos incluido el cómo manejar el caso `none`, dicho de otro modo, no escribimos la sección que se ejecutaría si la función recibiera un valor `none`. Naturalmente habrá un error en el compilado. Afortunadamente, es un error que Rust sabe detectar a la perfección. Revisemos que nos arroja la terminal si tratamos de compilar el código.
 
```
$ cargo run
   Compiling enums v0.1.0 (file:///projects/enums)
error[E0004]: non-exhaustive patterns: `None` not covered
 --> src/main.rs:3:15
  |
3 |         match x {
  |               ^ pattern `None` not covered
  |
  = help: ensure that all possible cases are being handled, possibly by adding wildcards or more match arms

error: aborting due to previous error

For more information about this error, try `rustc --explain E0004`.
error: could not compile `enums`.

To learn more, run the command again with --verbose.
```

Rust sabe que no consideramos cada posible caso e incluso que patrón hemos olvidado. ¡Por esto `match` en Rust es exhaustivo! Debemos cubrir hasta la última posibilidad para que el código sea válido y compile. Especialmente en los casos de `Option<T>`, cuando Rust nos previene de no olvidar codificar explícitamente la parte donde se maneje el `None`, nos protege de asumir que tenemos un valor cuando en realidad podríamos tener un null, un problema que cargan muchísimos lenguajes de programación, pero no Rust. 

### El _ Guión Bajo

Por último respecto a `match`, tenemos el equivalente al `else` cuando trabajamos con `if`. Este patrón es empleado cuando no queremos listar todos los posibles valores. Por ejemplo, un `u8` puede almacenar valores válidos de `0` hasta `255`. Si solo nos importa los valores `1`, `3`, `5`, y `7`, no hay razón por la que deberíamos listar secciones para `0`, `2`, `4`, `6`, `8`, `9` hasta `255`. Solo tenemos que utilizar el patron especial `_` en su lugar. 

```rust
   let some_u8_value = 0u8;
    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        5 => println!("five"),
        7 => println!("seven"),
        _ => (),
    }

```

Tal como dijimos, escribimos expresiones para los patrones que nos interesaban. Al nosotros poner de último `_` le decimos a Rust que en caso de que no encuentre una concordancia en alguno de los casos anteriores, simplemente ejecute la última expresión. El resultado de esto, tal como lo codificamos, es que retornara un valor unitario `()` que no hará nada, por lo cual nada ocurrirá en el patrón `_`. Esto nos permite declarar que no queremos que ocurra nada si no es uno de los cuatro valores que nos importan. 

Solo queda una cosa por ver, pues si bien `match` es una poderosísima herramienta, puede ser demasiado verboso en situación donde solo nos interese un patrón. Para estos problemas Rust nos provee de `if let`. 

### Controles de flujo concisos con `if let`

La sintaxis `if let` nos permite combinar dos conceptos tales como `if` y `let` en una forma menos verbosa de manejar valores con `match` con un patrón mientras ignoramos el resto. Considerando el programa en el siguiente ejemplo donde solo sucederá algo si el valor del match es `3`. 

```rust
   let some_u8_value = Some(0u8);
    match some_u8_value {
        Some(3) => println!("three"),
        _ => (),
    }
```

Queremos hacer algo con una coincidencia de `Some(3)` y nada más, cualquier otro caso no hará que que algo suceda; ni otro valor `Some<u8>` ni siquiera `None`. Para satisfacer el `match`, recordemos que tenemos que al menos agregar `_ => ()` luego de procesar una única variante, lo cual es mucho código tedioso que agregar. En su lugar, podemos escribir una forma sintáctica acortada bastante más usando `if let`. Tomemos el mismo ejemplo de antes. Con esta nueva herramienta podemos convertirlo en esto: 

 ```rust
   if let Some(3) = some_u8_value {
        println!("three");
 }
```

La sintaxis toma un patrón y una expresión separados por el símbolo de igual. Funciona exactamente del mismo modo que `match` donde la expresión es recibida por el `match` y el patrón es la primera de las secciones.  Usar `if let` significa muchas cosas tales como menos tipeo, menos indentación, y por supuesto, menos código innecesario. En contraposición, perdemos la mirada exhaustiva del `match` estable. Elegir entre `match` y `if let` depende enteramente de las necesidades que se nos presenta y cómo queremos afrontarlas nosotros. 
En otras palabras, podemos pensar en `if let` como una sintaxis endulzada de `match` que ejecuta código cuando solo hay una variación posible que sea capaz de disparar una función o cualquier tipo de suceso dentro del programa. Así tendremos la certeza que ignorara cualquier otro valor.  

Podemos también incluir un `else` con un `if let`. En dado caso, lo que se encuentre dentro de `else` aplicaría de igual manera que si usaramos `_` dentro de un `match`. Rescatemos el enum de las monedas en donde tomamos en cuenta el estado de procedencia dentro del territorio estadounidense. Si quisiéramos contar todas las monedas que no sean cuartos, y además ver de qué estado proceden cuando sí lo sean, podríamos hacerlo con un `match` así: 

```rust
   let mut count = 0;
    match coin {
        Coin::Quarter(state) => println!("State quarter from {:?}!", state),
        _ => count += 1,
    }
```

O podríamos usar un `if let` y `else` de este modo: 

```rust
   let mut count = 0;
    if let Coin::Quarter(state) = coin {
        println!("State quarter from {:?}!", state);
    } else {
        count += 1;
    }

```

Si se te presenta la situación que la lógica de tu programa es muy verbosa siendo expresada con un `match`, recuerda que `if let` es una herramienta de Rust también. 

## En resumen

Hemos cubierto en su totalidad los enums y como usarlo para crear tipos de datos personalizables que puede ser un grupo de valores enumerados. Exploramos cómo el tipo de data perteneciente a la librería estándar de Rust conocida como `Option<T>` ayuda sublimemente para prevenir los bien conocidos errores con valores nulos. Además, al disponer de los enums también podemos usar `match` o `if let` para extraer y usar dichos valores, dependiendo en cuantos casos necesitamos tomar en cuento. 

Con nuestros programas en Rust, podemos expresar conceptos en los dominios propios usando estructuras y enums. Creando diferentes tipos de data para fortalecer la seguridad de nuestros API, el compilador se cerciorará de que nuestras funciones reciban solo valores del tipo que esperan… Ahora, solo nos queda dar el siguiente paso y comenzar con Holochain. ¡Muy bien hecho héroe! Estás listo para tomar el destino entre tus manos. 

Si sientes que aun te faltan muchas cosas por saber del lenguaje de programación Rust, es cierto. Solo cabe acotar que aunque pasemos a la sección de Holochain no dejaremos de tocar conceptos de Rust. Hemos establecido las bases para que sea posible retener la información que viene a continuación. Sin más que decir, continuemos. 

## Mejor que atendieras a mis leccioness

«Te estás volviendo muy bueno con Match. Haces bien, será una parte esencial de tus poderes.»
La realidad es que te sorprendió bastante el poder de ese tipo de sortilegio. Se estaba volviendo tu favorito. Los glifos que describiste con tu dedo hace unos minutos finalmente te guiaron hacia una puerta específica dentro de ese laberinto. La abres y lejos de cualquier lógica, ante ti se mostraba un pasillo largo, amplio, que llegaba a unas escaleras que subían hasta lo que parecía un pedestal. 

Te adentras y sientes un aura distinta a tu alrededor. Era como si los colores dentro fueran más reales, no el artificial morado de los páramos que habías estado recorriendo por horas o incluso días. También te parece que el símbolo en tu peto está brillando curiosamente. Tu espera vibraba inquietamente mientras comenzabas a subir las escaleras. 

En la cima, había de hecho, un pequeño pedestal donde reposaba un libro de un material extraño y liso. Sus hojas eran como nubes pero podías leer con claridad su contenido. Podias leer con claridad su contenido.

Su portada rezaba  «Holochain»

─ Debe ser eso porque no entiendo nada de lo que está escrito en esas páginas. ─Comentó Mozilla. 
Te dispusiste a leer la primera página, pero antes de que pudieras hacer cualquier otra cosa, escuchaste algo detrás de ti. Te habían encontrado. 

─ Espero que atendieras a mis lecciones… creo que las vamos a necesitar. 
Entonces te pusiste en guardia protegiendo el libro detrás de ti. 










