# Estructuras

En esta sección trataremos con las estructuras, un tipo de data sumamente útil y moldeable que te permite almacenar diferentes tipos de variables que guarden alguna relación contextual entre ellos. Para todo aquel que venga de lenguajes de programación como Java se le hará sumamente familiar, ¡Guarda un gran parecido con la Programación Orientada a Objetos! Junto a los Enums (De los cuales hablaremos más adelante) son los bloques de construcción que usaremos más al momento de desarrollar nuestras aplicaciones.

Las estructuras guardan una gran similitud con los tuples, pero son sumamente dinámicos a diferencia de su contraparte. Como los tuples, pueden guardar diferentes tipos de data, pero cada una de estas piezas se declara con un nombre, por tanto se puede discernir la naturaleza de la variable inmediatamente. Otro beneficio de las estructuras radica es que al tener nombre todos sus valores internos, no hay que obedecer un orden de la data como sucedería con los tuples. 

>Si tienes dudas respecto a esta explicación y la naturaleza de los tuples, siempre puedes dar clic aquí para ser redirigido a la explicación de estos dentro de la guía. 

Para definir las estructuras, tenemos que usar la palabra clave `struct` y nombrar toda la estructura en sí. Posteriormente tenemos que declarar el nombre de las variables que va a almacenar y el tipo de data que recibirá. Hay que pensar que en esta parte solo estamos haciendo una abstracción, otra ventaja de las estructuras es que son reutilizables. Permíteme ilustrarlo. 

```rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

A partir de ahora, con esta abstracción, podemos declarar múltiples estructuras User a lo largo de nuestro programa si lo queremos. No tienes que llenar las estructuras en el orden que declaraste las variables, recuerda que lo que construimos antes es un esquema nada más. Hagamos una prueba. 

```rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn main() {
       let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };
     println!("{}", user1.email);
}
```

Si prestaste atención, habrás notado que hice la abstracción o la instancia como también es conocida fuera de cualquier función, de este modo puedo aprovechar las estructuras User en cualquier función de mi código. Es importante acotar también que el nombre de las instancias siempre se declaran con la primera letra mayúscula, preferiblemente siendo una sola palabra. 

Seguramente también notaste que para acceder a una variable específica de la estructura usar la notación de un punto de este modo: `user1.email`. Es exactamente del mismo modo si fuera un Objeto en Java. Cabe preguntar entonces, ¿podemos tener estructuras mutables? La respuesta es sí. 

```rust
   let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    user1.email = String::from("anotheremail@example.com");
```

Obviamente las instancias no son mutables o inmutables, sólo las estructuras declaradas pueden serlo, además, una estructura no puede tener una combinación de variables mutable e inmutables. Tiene que ser en su totalidad de un modo u otro. Sin embargo, esto no nos priva de hacer Shadowing así como crear nuevas instancias en funciones, y retornarlas para que así podamos crear estructuras aún más fácilmente. 
> Shadowing explicado aquí.

```rust
    fn main() {
        let user2 = build_user(‘someone@example.com’, ‘someusername’)
}

    fn build_user(email: String, username: String) -> User {
        User {
            email: email,
            username: username,
            active: true,
            sign_in_count: 1,
    }
}

``` 

Es perfectamente lógico nombrar los parámetros con los que llenarás la estructura del mismo exacto modo que la variable donde serán almacenados, pero hay incluso una forma menos tediosa de hacer esto que nos puede ahorrar muchísimo tiempo. Basta con hacerlo del siguiente modo:


```rust
fn build_user(email: String, username: String) -> User {
   User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}
```

Este es un atajo sintáctico el cual nos permite solo poner el parámetro y Rust lo entenderá tal como en el ejemplo pasado. ¡Y este no es el único de los atajos! ¿Recuerdas que te dije que podemos hacer shadowing con las estructuras? ¿Qué me dirías si te contase que puedes hacer shadowing con las estructuras rellenandolas con data de otras estructuras? Si esto comienza a resultar muy enrevesado no te preocupes, es normal estar desconcertado la primera vez, miremos un ejemplo. 

```rust
       let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        active: user1.active,
        sign_in_count: user1.sign_in_count,
    };
  ```

Estamos usando valores específicos de `user1` para crear nuevas instancias de `active` y `sign_in_count`. ¡Esto es totalmente posible y muy útil cuando usas muchas estructuras de la misma composición! Nos ayuda a evitar ser redundantes. Incluso, podemos hacer esto mismo pero solo la mitad del código. 

```rust
   let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        ..user1
    };
```

A Partir de `..user1` las variables siguiente contendrán los exactos mismos valores de user1. Esto último explicado solo funciona con estructuras que se originen de la misma abstracción. 

## Las estructuras y Ownership

En esta sección deliberadamente se evitó el uso de referencias. Esto es porque sería más amigable explicar el funcionamiento de las estructuras de datos con variables que le pertenecen enteramente y dejen de ser válidas junto a esta. El uso de referencias en las estructuras aunque totalmente posible, requiere conceptos avanzados que no necesitaremos para nuestra meta final que es Holochain. Dicho esto, aquí el caso en que tratemos de usar referencias como acostumbramos hasta ahora. 

```rust
struct User {
    username: &str,
    email: &str,
    sign_in_count: u64,
    active: bool,
}

fn main() {
    let user1 = User {
        email: "someone@example.com",
        username: "someusername123",
        active: true,
        sign_in_count: 1,
    };
}
```

```
$ cargo run
   Compiling structs v0.1.0 (file:///projects/structs)
error[E0106]: missing lifetime specifier
 --> src/main.rs:2:15
  |
2 |     username: &str,
  |               ^ expected lifetime parameter

error[E0106]: missing lifetime specifier
 --> src/main.rs:3:12
  |
3 |     email: &str,
  |            ^ expected lifetime parameter

error: aborting due to 2 previous errors

For more information about this error, try `rustc --explain E0106`.
error: could not compile `structs`.

To learn more, run the command again with --verbose.

```

> Si tienes un interés específico en este apartado del lenguaje, el capítulo 10 de la documentación oficial explica sublimemente este apartado. Puedes verlo aquí. 

## Momento de probarse

*Esquivas de un lado a otro tratando de pensar que hacer a continuación, los ataques de Mozilla te siguen capeando haciendo incapaz de alcanzarla. La bruja ríe y ríe mientras te ve tratando de hacer algo. Nuevamente tratas de aproximarte a ella sin exito alguno.*

*─ ¡Tienes que usar tu magia si quieres salir de este embrollo! Recuerda lo que has aprendido. Si no puedes conmigo entonces tampoco podrás con El.*

*Te concentras tanto como puedes y mientras corres alrededor de Mozilla comienzas a dibujar glifos. Cargo siempre acompañandote asiente preparado para ayudarte tanto como sea posible.*


## ¡En guardia!

¡Rápido! Tenemos que defendernos de la bruja, por desgracia, mantiene la distancia suficiente como para que sea imposible blandir tu arma. ¡Habrá que recurrir a un sortilegio! Para entender mejor cómo funcionan las estructuras, escribiremos un programa que te permite lanzar una poderosa bola de fuego y así alcanzar a Mozilla. 

Crearemos un proyecto con cargo llamado *Fire!* donde tomaremos los hidrocarburos y el oxígeno para generar una combustión. La suma de estos deben ser mayor a la vitalidad de Mozilla o de lo contrario el hechizo será muy débil, ¡Démonos prisa! 

Primero crearemos la instancia de nuestras estructura que almacenara el oxígeno y los hidrocarburos. Además generamos una constante que corresponderá a la vitalidad de Mozilla. 

```rust
MOZILLA_HEALTH = 300;
struct Burning {
    hydrocarbons: u32
    oxygen: u32
}
```

Declaramos `hydrocarbons` como u32 pues no nos interesa tener valores negativos en dichas variables, no podemos tener oxígeno o hidrocarburos negativos y queremos que Rust se asegure de eso. Ahora desarrollaremos la función main. 

```rust
fn main() {
    let fire_ball = create_burning(150, 100)
     println!(
        "The area of the rectangle is {} square pixels.",
        burning_impact(fire_ball.hydrocarbons, fire_ball.oxygen)
        );
}

Generamos `fire_ball` con una funcion llamada `create_burning()` que recibirá los valores de hidrocarburos y el oxígeno y retornara la estructura recién generada, esto nos permite que podamos modificar los valores más fácilmente y así jugar con el programa, llamamos una segunda función llamada `burning_impact()` que comprobará con condicionales si la combustión (es decir, la suma de los dos componentes) es mayor a `MOZILLA_HEALTH` y por ende bastante poderosa como para alcanzar a la bruja. Veremos a continuación estas dos funciones:

```rust
fn create_burning(hydrocarbons: u32, oxygen: u32) -> Burning{
   Burning {
        hydrocarbons,
        oxygen,
    }
}

fn burning_impact(hydrocarbons: u32, oxygen: u32){
    let mut result : str = “The fireball miss!”
    if (hydrocarbons + oxygen <= MOZILLA_HEALTH) {
        result = “The fireball was a success!”
    }
result
}
``` 
Tal como en los ejemplos pasados, al momento de crear `create_burning()` pasamos de parámetros las valores que nos interesa insertar en la estructura. Esta función retornara la estructura a la función `main()` tal como estipulamos al escribir `-> Burning`. Finalmente, en `burning_impact` sólo regresamos un string literal que complete el texto al final de `main()` diciéndonos si nuestro hechizo tuvo éxito o de lo contrario fallo. 
Juntando todo el código tenemos el siguiente programa: 

```rust
MOZILLA_HEALTH = 300;
struct Burning {
    hydrocarbons: u32
    oxygen: u32
}

fn main() {
    let fire_ball = create_burning(150, 100)
     println!(
        "The area of the rectangle is {} square pixels.",
        burning_impact(fire_ball.hydrocarbons, fire_ball.oxygen));
}

fn create_burning(hydrocarbons: u32, oxygen: u32) -> Burning{
   Burning {
        hydrocarbons,
        oxygen,
    }
}

fn burning_impact(hydrocarbons: u32, oxygen: u32){
    let mut result : str = “The fireball miss!”
    if (hydrocarbons + oxygen <= MOZILLA_HEALTH) {
        result = “The fireball was a success!”
    }
result
}
``` 

## ¡Vuelve aqui!

*─ ¡JA! Lo lograste, de verdad lo lograste. ─Exclamó Mozilla en el suelo.─ Supongo que esa bola de luz y lo que te he enseñado han servido de algo.*

*Ayudas a Mozilla a levantarse. Te sientes más fuerte, más seguro de lo que estás haciendo. Miras a Cargo y ya los pensamientos no son necesarios, ambos saben lo que están sintiendo como si fueran uno. Examinas tu alrededor; planicies moradas con un cielo perpetuamente oscurecido, contando la leve luz de los hilos que cruzan las alturas hasta precipitarse en el horizonte.*

*A la distancia máxima que pueden alcanzar tus ojos, logras dilucidar un templo que a la distancia lucía pequeño, pero si lo podías ver desde tan lejos, lo más seguro es que fuera colosal. Sus paredes eran recias y de color verde claro muy similar al de tu arma.*

*─ Eso es justo lo que estábamos buscando. ─Acotó Mozilla.─ Lo mejor es que sigamos. Mientras tanto, tendremos tiempo para que te enseñe unas cuantas cosas más sobre Rust.*

*Miras con un atisbo de terror a Mozilla. Ella te responde con un bufido.*

*─ ¡Puedes relajarte! No te voy a volver a atacar… por ahora. ¡Hey! No corras. ¡He dicho que vuelvas aquí!*


## Métodos

Por último en la sección de estructuras o `struct` estudiaremos acerca de los métodos. Estos se pueden confundir fácilmente con las funciones porque su cometido es virtualmente el mismo; ejecutar un bloque de código concreto, no obstante, los métodos son especiales porque están directamente implicados con las estructuras asi como con los enums que juegan también un papel muy importante en la siguiente y última sección de Rust.

### Definiendo Métodos.  

La mejor forma posible de explicar los métodos es implementarlos a un proyecto que ya hayamos hecho, por lo que en este caso utilizaremos justamente el código que escribimos para derrotar a Mozilla. Si estudiamos bien nuestro programa, seremos capaces de reconocer que ambas funciones están fuertemente ligadas con nuestra estructura llamada `Burning`, por lo que a partir de lo que sabemos de los métodos, podríamos decir que es posible implementarlo. 

La sintaxis de los métodos es sencilla, solo es un leve cambio con respecto a las funciones. Crearemos un espacio donde alojar todos nuestros métodos relacionados con la estructura a la que le queremos dar funcionalidades. Utilizaremos la palabra clave `imp` seguido del nombre de nuestra estructura, en este caso `Burning`; entonces abriremos unas llaves y dentro de ese scope ubicaremos nuestros métodos. Este proceso hay que realizarlo con cada método como demostraremos a continuación. 

```rust
#[derive(Debug)]
struct Burning {
    hydrocarbons: u32
    oxygen: u32
}
imp Burning {
fn create_burning(hydrocarbons: u32, oxygen: u32) -> Burning{
           Burning {
            hydrocarbons,
            oxygen,
        }
    }
}
imp Burning {
    fn burning_impact(&self){
        let mut result : str = “The fireball miss!”
        if (self.hydrocarbons + self.oxygen <= MOZILLA_HEALTH) {
            result = “The fireball was a success!”
        }
        result
    }
}
``` 

Las funciones no se vieron afectadas en modo alguno más allá que ahora son métodos, no funciones porque están contextualmente ligados a `Burning`... Aunque eso no es cierto, ¿verdad? Hay otra diferencia, una referencia y esa es `&self` ¿Qué es esta sintaxis extraña? La respuesta es bastante sencilla. Ya no estamos llamando la data desde la estructura hacia una función, ahora seguimos dentro del mismo bloque de información, por tanto, para acceder a todas las variables de `Burning` tenemos que usar `&self`.

Además de todo, usamos la referencia porque no deseamos el ownership de la estructura, solo queremos leer los datos para hacer una breve suma y compararlos. En el caso de que quisiéramos que el método pueda modificar variables, se usaria la versión mutable de la misma sintaxis, es decir, `mut &self`, no obstante, a nivel práctico es muy poco frecuente que quieras modificar la estructura, normalmente será preferible hacer una copia y transformar esa nueva variable. 

El mayor beneficio referido a utilizar métodos en lugar de funciones es meramente por organización y eficiencia, haciendo el código mucho más legible y fácil de moldear, para ti, héroe, que harás proyectos cada vez más grandes. 

## En resumen

Las estructuras nos permiten crear tipos customizables bajo tu completo control. Usando structs, puedes mantener asociadas las piezas de data conectadas una con otra y darles nombre para tener un código limpio. Aunque te sorprenderá escuchar que las estructuras no son la única forma en la que puedes crear tipos de data (o bloques de información) personalizables, a continuación hablaremos de los enums, la última sección de Rust antes de pasar a la razón por la cual estamos aquí, y esa razon es Holochain. 



