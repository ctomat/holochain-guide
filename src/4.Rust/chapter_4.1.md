# Te presento a Rust

<img src="./img/Rust-programming-language.jpg" />

Creo que ya es redundante decir que Rust es un lenguaje de programación, no solo porque ya en varias partes de la guía lo he descrito como tal, sino porque decir que solo es eso es quedarse corto. Rust es empoderamiento para todo el mundo para crear software eficiente y de alta confianza. Actualmente se encuentra en su versión 1.4.1.1. Rust fue originalmente diseñado por Graydon Hoare en Mozilla Research con la contribución de Dave Herman, Brendan Eich, y otros. Rust ha sido el “lenguaje de programación más amado” en el censo de desarrollador de Stack Overflow desde 2016.

Rust esta pensado para ser un lenguaje destinado a sistemas altamente concurrentes y seguros, además de la programación en el proceso, que es creando y manteniendo lineamientos que preservan a la larga la integridad de dicho sistema. Esto lo consigue a través de una gama de funciones establecidas con un énfasis especial en la seguridad, control de la salida de memoria y la concurrencia. Todo esto es gracias a su propio manejo de la memoria que le evita la necesidad de estar constantemente utilizar recolectores de basura, y su increíble gestor cargo que maneja muchas tareas por el desarrollador.  

Si bien, Rust tiene como sector objetivo software del sistemas, micro-código y en general cosas de bastante bajo nivel como C++ o C, no se ve limitado únicamente por esta área. El mejor ejemplo de esto es con Holochain, donde aprovechando sus factores de bajo nivel se pueden lograr aplicaciones web potentes y seguras. De hecho, te contare un secreto: Esta documentación que estas leyendo, ¡su núcleo esta escrito en Rust! ¡Así que también es sumamente versátil! Habiéndote presentado Rust, ¿qué tal si lo instalamos y damos el primer paso hacia Holochain? 

