# ¿Qué es Ownership? 

Al hablar de lenguajes de programación y software en general, dependiendo del lenguaje en el que está construido, podemos discernir cómo dicho programa se encarga de la memoria, algo fundamental para asegurar el buen funcionamiento del producto. Algunos lenguajes de programación (tal es el caso de C++) utilizan un recolector de basura, esta herramienta constantemente está buscando por memoria sin utilizar. Por otra parte, hay lenguajes de programación que explícitamente requieren que el mismo desarrollador localice y libere la memoria… Rust usa un tercer enfoque. 

Ownership es un concepto relativamente nuevo tanto para programadores avanzados como principiantes porque es una forma de manejar la memoria que solo vemos en Rust, por lo que en primera instancia puede parecer confuso, sin embargo, al hacerse con la idea es incluso más sencillo que las formas de manejar la memoria más comunes ya que solo es necesario conocer las tres reglas que rigen esta funcionalidad y que son infranqueables. Podrás notar que esta forma también es sorprendentemente eficiente, así que presta atención. 

Tal como se dijo con anterioridad, Rust se maneja con el sistema de memoria Ownership que está regido por una serie de reglas estrictas que son comprobadas por el compilador constantemente. Una vez terminado el compilador sin errores o advertencias y creado el ejecutable podemos estar seguros de que nuestro código es eficiente y seguro. Comprobaremos esto en los ejemplos siguientes centrándonos en un tipo de estructura de data muy común; el string.  

Antes de poder adentrarnos en los poderosos secretos arcanos de Ownership tenemos que hablar de un concepto particularmente confuso cuando se está comenzando en el área de desarrollo de software y que ha sido un obstáculo hasta para los héroes de mayor prestigio. Se trata de las Pilas y Colas. 
Como sabemos, las Pilas y Colas son estructuras de memoria donde el primero está basado en el modelo LIFO -Last in First Out- que llevado al español, significa que el último dato en entrar es el primero en salir. Por otra parte, el segundo (la Cola) es lo contrario al ser FIFO -First in First Out- que quiere decir que la primera pieza de data entrar, a su vez será la primera en salir.

Las Pilas son mucho más eficientes que las Colas pues tienen una cantidad de espacio determinada y es alocado conscientemente; muy diferente  a la Cola que la cantidad de memoria que ocupará es indeterminada al igual que el sitio donde lo hará, por esto cuando lo utilizamos se nos retorna un puntero para posteriormente ser capaz de ubicarlo. Es más o menos como un grupo de oficinas; aunque el cliente no sepa dónde se encuentra tu despacho, puede preguntárselo a la recepcionista que le señalara donde estas. 

Usar las Pilas por otra parte aunque puede resultar más complicado, si se consigue, es bastante más eficaz porque no se requiere tal proceso de búsqueda, la nueva información siempre estará de primera. Aun así hay que considerar que a pesar de las ventajas, requiere a cambio que se le localice en primer lugar un espacio de memoria lo bastante grande (ya que no buscará dicho espacio como la Cola) y contabilizando para la siguiente entrada de información. 

as Colas son más lentas que las pilas por el simple hecho de tener que seguir un puntero. Los procesadores actuales pueden verse ralentizados si debe moverse de un espacio de memoria a otro y luego volver. Lo cual es recurrente con las Colas. Volviendo al ejemplo de las oficinas, una situación que ocurría antes con frecuencia es que había que moverse de una oficina a otra para realizar un único trámite por el intrincado sistema de atención que tenía el sistema. El equivalente de las Pilas es cuando muchos de estos procesos de papeleo se digitalizaron, volviéndose de acceso inmediato e instantáneamente ubicables. 

Cuando declaramos una función, los valores pasan dentro de su scope y las variables locales de esta son empujadas a la Pila. Una vez finalizado el proceso, esas mismas variables son removidas de la Pila. Mantener el seguimiento de qué partes de la data son empleadas en la Cola o en la Pila, minimizar la cantidad de data duplicada y eliminar todo rastro de información ya no necesario son los problemas por los que Ownership fue creado y así resolverlos. 

## Las Reglas de Ownership

Hemos mencionado numerosas veces las reglas de Ownership pero no las hemos explicado en ningún momento, por lo que ya es hora de ilustrarlos. Las reglas son tres: 

1. Cada valor en Rust tiene una variable llamada su owner o dueño. 
2. Cada valor solo puede tener un dueño a la vez. 
3. Cuando el dueño sale del scope de la función, el valor es desechado.

## Comprendiendo

*La luz que produces con tu magia luce cada vez más estable, menos errática. Te sientes en control al comprender mejor cómo la energía fluye a tu alrededor. Ownership lo llamó Mozilla.*

*─ ¡No, no y no! Así no es. ─Hasta hace un momento creías que lo hacías bien.─ Rust es una magia poderosa pero caprichosa como ninguna. Construimos poco a poco cada hechizo para que cuando esté lista sea su mejor versión y la más estable. Haremos un repaso rápido a todas las variables que conoces y como Ownership actúa sobre ella.*


## Scope de las Variables

Veamos un ejemplo sencillo de scopes por si esa parte no ha quedado lo bastante clara. Tenemos el siguiente ejemplo de una función: 

```rust
fn one function() { //Comienza el scope
    let value = 5 //La variable es declara y puedes utilizarla
    //En este punto puedes hacer cosas con la variable.
}//Termina el scope, la variable ya no es válida. 
```
De momento, al menos en apariencia, esto es idéntico al resto de los lenguajes de programación en donde tenemos que utilizar la variable mientras está en el scope. 

## El Tipo String

Para demostrar lo mejor posible como funciona Ownership, emplearemos el tipo de data String, sin embargo, no será el mismo tipo que hemos estudiado en la sección pasada. El string que ya conocemos es conocido como el string literal y este se aloja en la pila. Este string en ocasiones es poco práctico al ser inmutable y muy estricto a la hora de determinar cuánto espacio de memoria ocupa. Por esto, y para poder probar lo que queremos, es pertinente trabajar con un tipo de data que se aloje en la Cola, y ese es el String. Nótese la mayúscula. 

Con String somos capaces de almacenar una cantidad de texto que es desconocida para nosotros, algo sumamente conveniente cuando desarrollamos programas que reciben información del usuario y que es imposible predecir. Además es mutable y tiene varias funcionalidades convenientes para la manipulación de cadenas de caracteres. Aquí lo podemos ver:

```rust
fn main() {
    let s = String::from("hello");
}
```

Los doble puntos es un método sintáctico que de momento no nos interesa, solo debes quedarte con el hecho que es una foma elegante de ahorrar el escribir variables como `string_from_s` que sería bastante más engorroso. Demostremos que es mutable: 

```rust
fn main() {
    let mut s = String::from("hello");

    s.push_str(", world!"); // push_str() nos permite concatenar strings literales

    println!("{}", s); // Esto imprimirá `hello, world!`
}
```

Es interesante que uno tipo es mutable y el otro dos, pero lo verdaderamente importante es cómo lidian diferentemente con la memoria. 

## Memoria y Localización 

Cuando utilizamos los string literales, tal como ya se ha mencionado en varias ocasiones, conocemos el tamaño exacto de la cadena de texto antes de compilar el programa. Es por esto que son los más rápidos y eficientes además de por ser inmutables. Por desgracia, no es un tipo de dato que nos sirve para la gran mayoría de los casos y no podemos codificar cada variación posible que puede darse con el string, por ello necesitamos String. 

Para qué String pueda ser mutable y soporte una cantidad creciente de texto, tenemos que alojarlo en un espacio de memoria ubicado en la Cola y siendo la cantidad desconocida durante el compilado. Recordando la explicación de las Pilas y Colas, esto significa dos cosas: 

1. Necesitamos que esa parte específica de la memoria pueda ser solicitada de donde fue alojado.
2. Necesitamos una forma de liberar esa memoria alojada una vez hayamos terminado con el String. 

Lo primero ya lo logramos cuando llamamos `String::from()`. Es algo ya contemplado en todos los lenguajes de programación habidos y por haber. Lo segundo sin embargo sí representa un problema. Al no tener un recolector de basura que compruebe constantemente la memoria y los espacios desusados, recae sobre nosotros la responsabilidad de darle mantenimiento a la memoria. Si se nos olvida, estamos desperdiciando memoria, si lo hacemos demasiado pronto invalidaremos variables aún requeridas. 

Rust toma un sendero único; cuando la variable sale del scope, la memoria es retornada automáticamente, es decir, el valor es deshechado. 

```rust
fn main() {
       let s = String::from("hello"); // s es válido a partir de ahora. 

        // Aquí puedes hacer cosas con s
}// el scope llegó a su fin, ahora la memoria es retornada. 
``` 

## ¡Asi no es! 

Tratas de pasar una variable hacia una función como la llamaba Mozilla, refiriéndose a los bloques de ordenes que separabas para hacer el hechizo más fácil de manejar y reutilizable. Moviendo las variables y tratando de usarlas por un momento pierdes la coordinación y los glifos de luz a tu alrededor desaparecen. Notas a Mozilla asentir apreciativamente. 

─ Veo lo que tratas de hacer, bien pensado, pero te faltan unos cuantos datos antes de poder hacer eso. No necesitas mover las variables todo el tiempo entre funciones, muchas veces basta con referenciarlas. Acércate, te mostraré cómo.


Naturalmente, antes de que finalice el scope nosotros podemos retorna el string antes de que `s` salga del scope y sea desechado, solo hay que alojarlo. Cuando la variable sale del scope, Rust de forma automática llama una función especial conocida como `drop` para así retornar el espacio de memoria. También es el momento para que el desarrollador retorne el valor que quiera utilizar. 
Esto puede tener un impacto abrumador en cómo escribir el código en Rust aunque no lo parezca en una primera impresión. De momento el código presentado es simple, pero a la hora de escribir programas más complejos, puede llegar a resultados inesperados en donde otros lenguajes arrojarian el resultado esperado. Revisemos una de las más conocidas. 

### Formas en las que la data y variables interactúan.

#### Mover.

Múltiples variables pueden interactuar con la misma data de muchas formas. 

```rust
fn main() {
    let x = 5;
    let y = x;
}
```

Tratemos de discernir lo que está sucediendo aquí; declaramos una variable `x` a la cual le asignamos el valor de 5. Luego declaramos otra variable, la cual es `y` que a su vez es igual a `x`. Podemos suponer entonces que `y` lo que hará es una copia del valor de `x`, lo cual es absolutamente correcto. Ahora veamos un caso un poco distinto. Utilicemos finalmente los Strings. 

```rust
fn main() {
    let s1 = String::from("hello");
    let s2 = s1;
}
```

Se podría decir que lo mismo ha de ocurrir, y sin embargo, está lejos de ser así por una diferencia fundamental al momento de trabajar con esta data; el ejemplo anterior fue con un entero, una pieza de información inmutable que se almacena en la Pila, pero esto es diferente ya que desde un inicio hemos aclarado que los Strings se almacenan en la Cola. Tratemos lo siguiente: 

```rust
fn main() {
       let s1 = String::from("hello");
          let s2 = s1;

       println!("{}, world!", s1);
}
```

Y entonces…

```
$ cargo run
   Compiling ownership v0.1.0 (file:///projects/ownership)
error[E0382]: borrow of moved value: `s1`
 --> src/main.rs:5:28
  |
2 |     let s1 = String::from("hello");
  |         -- move occurs because `s1` has type `std::string::String`, which does not implement the `Copy` trait
3 |     let s2 = s1;
  |              -- value moved here
4 | 
5 |     println!("{}, world!", s1);
  |                            ^^ value borrowed here after move

error: aborting due to previous error

For more information about this error, try `rustc --explain E0382`.
error: could not compile `ownership`.
To learn more, run the command again with --verbose.
```

No te sientes abrumado, lo que ocurrió es más simple de lo que se refleja en la terminal. Recordemos que cuando utilizamos tipos de data almacenados en la Cola, la información es alojada en un espacio libre y es retornado el puntero. Por tanto, al inicio de nuestro programa, nuestra variable s1 almacenaba dicho puntero. 

El caso ahora, es que durante la ejecución, cuando tratamos de duplicar s1 con s2, lo que en realidad ocurre es que se copia el puntero y Ownership desecha s1 para evitar data duplicada y mantener el control sobre los punteros. Si en realidad copiaramos tendríamos un nuevo puntero señalando a un espacio diferente de memoria con exactamente la misma información. Es redundante explicar porque esto es tan poco eficiente. 

Pero incluso en el escenario donde realmente queramos hacer lo descrito con anterioridad, véase, copiar puntero y data, es posible con la función `clone()` y es sumamente fácil de utilizar.
 
 ```rust
fn main() {
     let s1 = String::from("hello");
     let s2 = s1.clone();

     println!("s1 = {}, s2 = {}", s1, s2);
}
```
Esto funcionará perfectamente bien de la forma explícita que normalmente se esperaría. La función `clone()` tiene otro objetivo de índole visual.Cuando veas esta función sabrás de antemano que algo arbitrariamente diferente está sucediendo y puede ser perjudicial para el rendimiento del software. 

Entonces, ¿Cómo sé que se almacena en la Pila y que en la Cola? Por lo general, cualquier tipo de data de valor escalable simple como los enteros y que no requieren alojamiento como los String es almacenado en la Pila. Unos ejemplos de esto puede ser: 

- Todos los tipos enteros.
- Los booleanos que los valores `true`y `false`
- Todos los tipos flotante. 
- Los tipo char que solo almacena un carácter. 
- Tuples si su contenido es de uno de los tipos mencionados anteriormente. 

Todo tipo de dato que se almacena en la Pila es copiado de forma automática como el primer ejemplo sin la necesidad de `copy()`. Al contrario, todos los tipos de datos que se almacenan en la Cola transferirán su puntero al menos que se utilice explícitamente `copy()`

## Ownership y Funciones

Lo explicado antes se aplica de igual modo en las funciones cuanto se trata de data que se almacena en la Pila o Cola. Cuando llamamos a una función que reciba de parámetro un valor como un entero, lo que hará es realizar una copia del valor dentro de la variable. Podemos anticipar entonces que si llamamos otra función pero que recibe de parámetro un String, el puntero es transferido a la función y deja de ser valida en su origen. 

Ilustremos mejor con un ejemplo:

```rust
fn main() {
    let s = String::from("hello"); // s entra en el scope

    takes_ownership(s); // el puntero de s en movido a la función takes_ownership(s)
    // ... desde aqui s ya no es válido pues ahora se encuentra en la otra función. 

    let x = 5;                      // x entra en el scope

    makes_copy(x); // una copia de x es automáticamente enviada debido a que es un entero y estos son almacenados en la Pila. 
}//Aqui x sale del scope y deja de ser válido. La memoria es retornada. 

fn takes_ownership(some_string: String) { // some_string entra en el scope
    println!("{}", some_string);
} // some_string sale del scope y la memoria que ocupa finalmente es retornada. 

fn makes_copy(some_integer: i32) { // some_integer entra en el scope
    println!("{}", some_integer);
} // Desde aqui, some_integer que tenía la data copiada de x sale del scope y la memoria es retornada. 
```

Podemos apreciar que las variables gracias a Ownership siguen un patrón muy sencillo de seguir que nos permite llevar control exhaustivo de nuestra memoria a lo largo de la ejecución del programa. Esto aun asi nos presenta una nueva cuestión cuando deseamos trabajar con solo valor de una variable. Constantemente tener que mover el ownership de nuestras variables es tedioso y existen mejores formas.
Una de esas mejores formas son las referencias. 

## Referencias y Préstamos

El problema cuando dependemos enteramente de enviar y retornar variable es que no hará escribir mucho más código del que nos gustaría, código que está lejos de ser necesario teniendo a nuestra disposición las referencias. Utilicemos Referencias en un pequeño ejemplo. Las referencias son tan simples de utilizar como colocar la sintaxis de `&` antes del nombre de la variable. 

```rust
fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

```

Lo primero que te llamará la atención al ejecutar este pequeño programa es que `s1` sigue siendo válido aun después de haber sido un parámetro para `calculate_lenght()`. Esto es gracias a que `s1` realmente jamás fue un parámetro de la segunda función. Al nosotros referenciar `s1` tomamos directamente el valor de la variable para realizar la tarea fuera del main. Ya no estamos haciendo la nefasta práctica de copiar un puntero, estamos tomando únicamente lo que necesitamos por el tiempo que necesitamos. 

Con esto damos lugar a otra pregunta muy importante: ¿podemos cambiar el valor de una variable referenciada? 

Comprobemos: 

```rust
fn main() {
    let s = String::from("hello");

    change(&s);
}

fn change(some_string: &String) {
    some_string.push_str(", world");
}

```

```
$ cargo run
   Compiling ownership v0.1.0 (file:///projects/ownership)
error[E0596]: cannot borrow `*some_string` as mutable, as it is behind a `&` reference
 --> src/main.rs:8:5
  |
7 | fn change(some_string: &String) {
  |                        ------- help: consider changing this to be a mutable reference: `&mut std::string::String`
8 |     some_string.push_str(", world");
  |     ^^^^^^^^^^^ `some_string` is a `&` reference, so the data it refers to cannot be borrowed as mutable

error: aborting due to previous error

For more information about this error, try `rustc --explain E0596`.
error: could not compile `ownership`.

To learn more, run the command again with --verbose.
```

¡Oh no! Otra vez algo salió mal. Nuevamente, la explicación es bastante sencilla. Las referencias no son el owner de la variable, por lo tanto no tienen propiedad sobre esta y no pueden modificarla. Además de esto, `s` es inmutable. Esto por fortuna también está contemplado. Para lograr cambiar el valor de la variable podríamos “pedirlo prestado” y retornarlo con la modificación. A esto se le conoce como Borrowing y lo usaremos a continuación:

```rust
fn main() {
    let mut s = String::from("hello");

    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```

El primer paso es convertir a `s` en mutable lógicamente para permitir así cualquier cambio. Lo siguiente es crear una referencia mutable con la que podamos pedir prestado el valor de la variable y modificarlo. Hay que tener en cuento un factor muy importante, una regla fundamental de las referencias mutables; solo puedes tener una por scope. 

```rust
fn main() {
    let mut s = String::from("hello");

    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```

Esto dará error.

```
$ cargo run
   Compiling ownership v0.1.0 (file:///projects/ownership)
error[E0499]: cannot borrow `s` as mutable more than once at a time
 --> src/main.rs:5:14
  |
4 |     let r1 = &mut s;
  |              ------ first mutable borrow occurs here
5 |     let r2 = &mut s;
  |              ^^^^^^ second mutable borrow occurs here
6 | 
7 |     println!("{}, {}", r1, r2);
  |                        -- first borrow later used here

error: aborting due to previous error

For more information about this error, try `rustc --explain E0499`.
error: could not compile `ownership`.

To learn more, run the command again with --verbose.
```

Te lo dije.

Esta estricta restricción como otros aspectos del lenguaje al principio resultan incómodos viniendo de otros lenguaje donde uno puede modificar variables sin resistencia alguna. En estos momento hay que recordar porque estamos aqui; queremos estas restricciones porque a la par nos da control de lo que hacemos, y entre más control dispongamos de nuestro desarrollo, elaboraremos programas más efectivos. 

En el caso de las referencias mutables, no podemos tener más de una por scope para evitar una carrera de data, un evento donde sucede una de las tres cosas:

- Dos o más punteros acceden a la misma data al mismo tiempo.
- Al menos uno de los punteros está siendo utilizado para escribir data. 
- No hay ningún mecanismo siendo utilizado para sincronizar el acceso de la data. 

Cualquiera de los tres escenarios romperá con certeza nuestro código. Peor aún, cuando una carrera de data es el problema, es muy difícil de diagnosticar y reparar cuando tratas de localizarlo durante la depuración. Rust corta de raíz esta situación haciendo imposible que ocurra al rechazar al ni siquiera compilar ante la posibilidad de una carrera de data. 

Esto no quiere decir que no podamos usar llaves para crear múltiples referencias mutables es sólo imposible que ocurran simultáneamente. 

```rust
   let mut s = String::from("hello");

    {
        let r1 = &mut s;
    } // r1 sale del scope y es descartado, por lo que podemos crear una nueva referencia sin ningún error. 

    let r2 = &mut s;
```

Obviamente tampoco podemos combinar referencias normales con referencias mutables. 

```rust
   let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    let r3 = &mut s; // BIG PROBLEM

    println!("{}, {}, and {}", r1, r2, r3);
```

Por favor, no hagas esto. De lo contrario recibirás el siguiente error. 


```
$ cargo run
   Compiling ownership v0.1.0 (file:///projects/ownership)
error[E0502]: cannot borrow `s` as mutable because it is also borrowed as immutable
 --> src/main.rs:6:14
  |
4 |     let r1 = &s; // no problem
  |              -- immutable borrow occurs here
5 |     let r2 = &s; // no problem
6 |     let r3 = &mut s; // BIG PROBLEM
  |              ^^^^^^ mutable borrow occurs here
7 | 
8 |     println!("{}, {}, and {}", r1, r2, r3);
  |                                -- immutable borrow later used here

error: aborting due to previous error

For more information about this error, try `rustc --explain E0502`.
error: could not compile `ownership`.
To learn more, run the command again with --verbose.
```

Es posible tener varias referencias inmutables dentro del mismo scope gracias a que ninguna de estas será capaz de modificar los valores de su variable de origen, por lo cual estamos fuera de peligro. Por otra parte, cuando utilizamos una referencia mutable **solo** podemos usar esa referencia a partir de ese punto. Por lo tanto, podemos tener referencias inmutables y mutables dentro del mismo scope, pero una vez declarada la mutable no podremos usar más las mutables. 

```rust
   let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{} and {}", r1, r2);
    // r1 and r2 are no longer used after this point

    let r3 = &mut s; // no problem
    println!("{}", r3);
```

Esto con toda certeza funcionará, ¡Ponlo a prueba!

## Cosas un poco más dificeles

*Y entonces llegas a la última lección. Mozilla sugirió que comenzaran a andar hacia el siguiente destino y no perder más tiempo en el mismo sitio. El caminar y practicar a la vez no te resulta para nada difícil. Te sientes como pez dentro del agua con las artes místicas de Rust. Sin embargo, si eras un pez, definitivamente Mozilla era un tiburón con sus complejos glifos y hechizos multifuncionales. Ella agregaba estructuras a su magia para organizar aún mejor los símbolos que trazaba en el aire.*

*─ ¡Espero estés prestando atención, héroe! Esto es lo que te atañe ahora. Dentro de Rust hay una pequeña variedad de estructuras. Son muy poderosas y facilitan mucho nuestro trabajo, una vez que las domines te preguntarás cómo has vivido sin ellas.*

*Al pronunciar la última palabra materializó una poderosa energía que lanzó sobre ti. Con unos impecables reflejos te mueves a un lado totalmente desconcertado.*

*─ ¡Ja! Se acabaron los juegos de niños. ¿Quieres aprender más? Pues hora de hacerte las cosas un poco más difíciles. ¡En guardia!*

## En Resumen

Finalmente, para resumir todo lo aprendido, lo cual ha sido muchísimo, héroe, repasemos las reglas fundamentales de las referencias.
- Solo puedes tener al mismo tiempo una referencia mutable o ilimitadas referencias inmutables.
- Las referencias siempre deben ser validas. 






