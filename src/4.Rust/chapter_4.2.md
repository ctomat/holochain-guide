# Primeros Pasos

<img src="./img/knight.gif" />

*Miras a Cargo mientras convierte su gran masa de luz a un pequeño destello, sobrevolando toda la habitación hasta posicionarse sobre su cabeza. Sientes como una variada cantidad de palabras, específicamente órdenes, son grabadas en tu mente, el conocimiento se expande y te sientes capaz de hacer algo completamente nuevo. Sientes las artes antiguas de Rust fluyendo de la punta de tus dedos hasta tus pies. Es así como la primera orden se manifiesta.*

*«¡Excelente! Eres muy rápido asimilando la información, aunque ciertamente es lo que cabría esperar siendo lo que eres. Es simple, para utilizar Rust solo requieres primero invocar el catalizador que te permitirá filtrar ese poder. Se llama Rustup y se encuentra muy dentro de ti, solo debes buscar con especial empeño. ¡Vamos! Trata»*

*Buscas a Rustup en la profundidades de tu ser.* 

## Invocando a Rustup

Parece ser que nuestro héroe nos necesita, tenemos que ayudarlo a invocar a Rustup, el instalador y el control de versiones de Rust. Verás, tal como Node.js, no es raro para un desarrollador tener que trabajar con versiones varias de Rust dependiendo del proyecto. Estos programas seguramente cuentas con unas dependencias específicas y está corriendo sobre una version de Rust en particular, actualizar dicha aplicación a la versión actual de Rust junto a los paquetes empleado en sí puede ser un trabajo de mantenimiento, pero esto debe ser minucioso, pues tomando a la ligera, podríamos quebrar un sistema que era perfectamente estable.

¡Seguramente has ido a la página del lenguaje de programación mucho antes de siquiera llegar aquí! Pero por si acaso, es momento de dirigirse a la página oficial de [Rust.](https://www.rust-lang.org/)

![alt text](./img/rust_page.png "Logo Title Text 1")

Nos encontraremos con esto. Te invito a leer toda la página de bienvenida porque los desarrolladores de Mozilla hicieron un trabajo maravilloso resumiendo todo lo que es Rust en menos de dos párrafos. Si quieres consultar la sublime documentación del lenguaje solo tienes que dar clic en “Learn”. ¡Pero vamos a concentrarnos en lo que nos concierne aquí y ahora. “Install”. Si estás en Mac, Linux o cualquier sistema Unix, la instalación es tan simple como abrir la terminal e ingresar la siguiente línea: 

```css
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Sin embargo te invito a revisar directamente en la página para asegurarte que esta línea no haya sido actualizada. Es necesario que tengas instalado en tu sistema [curl](https://curl.haxx.se/) para que funcione. ¿Y qué es curl? Es una herramienta que nos permite transferir data desde URLs a través de la línea de comandos. Lo que estamos haciendo entonces es descargando todos los archivos necesarios para disponer de Rustup. La instalacion es muy intuitiva y bastara con elegir las opciones por defecto, o sea, la primera. 

<img src="./img/rust-install.webp" />

Si eres un usuario de Windows tienes que usar otro medio diferente a curl pues no está disponible en dicha plataforma. Existen gestores de paquetes en Windows que te permitirán instalarlo también con un comando, pero no es necesario porque la misma pagina te proporcionará un .exe que abre el CMD del sistema y se te presentará exactamente lo mismo que vería alguien en Mac o Linux. Nuevamente, basta con las acciones por defecto.

¡Y con eso seria todo! Tipea ‘rustup’ en la terminal o el CMD, esta debería responderte con lo siguiente: 

![alt text](./img/rustup.png "Logo Title Text 1")

Si esto no ha funcionado dirígete al final de esta sección para saber agregar el PATH de rustup a tu línea de comandos. 

Si todo ha salido correctamente, ¡felicitaciones! Haz invocado correctamente a Rustup, y por ende, las artes místicas de Rust. ¡Siente el poder! 

## Despertar 

*Sientes el poder corriendo por cada una de tus fibras, la fuerza te llena y rápidamente esta fuerza se exterioriza, una plateada armadura de gran resistencia cubre todo tu cuerpo, un escudo aparece en tu mano izquierda, sabes su nombre, ownership. En tu mano derecha aparece una espada cuanto menos particular, de colores verdes y azulados apagados, dos hojas se enroscan entre sí hasta formar una misma punta.* 

*Te sientes como todo un caballero, o más bien… como un héroe. Cargo se mueve de lado a lado notablemente emocionado.* 

*«¡Sí! ¡Sí! De eso estaba hablando, fue perfecto. Definitivamente eres el héroe de las leyendas sin lugar a dudas. Ven, no hay más tiempo que perder, sigamos a las otras torres para continuar con tu preparacion, ademas, tienes que conocer a la gran maestra. »*

*Das un paso, pero una imagen a tu izquierda llama tu atención. Te detienes a verla y tan pronto como la ves en detalle, descubres que es un espejo.* 

<img src="./img/assets-01.png" alt="Logo Title Text 1" width="300"/>

*Y la imagen eres tú.*






