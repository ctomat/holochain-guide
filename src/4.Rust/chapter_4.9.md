# Dominando las Artes Arcanas de Rust

## Práctica Final de Rust

¡Demos un paso hacia adelante listos para enfrentar este nuevo reto! En este capítulo a la vez que enfrentamos a la criatura emplearemos todos los conceptos de Rust que hemos estudiado a lo largo de la guia. No ceñiremos totalmente a la perspectiva práctica de cada función, por lo que es importante que hayas pasado por las secciones anteriores a esta. Desarrollaremos un juego de adivinar el número, en donde dar con el número correcto será atinar en el blanco a tu oponente con tus ataques. 

El programa generará un número entero aleatorio entre `1` y `100`. Luego le pedirá al jugador que ingrese una suposición. Después de ingresar una conjetura, el programa indicará si la conjetura es demasiado baja o demasiado alta. Si la suposición es correcta, el juego imprimirá un mensaje de felicitación y saldrá. ¡Tal como si tratases de apuntarle a alguien con tus hechizos. Cada vez que fallas tratas de ajustar la trayectoria de tu magia. El juego de adivinar funciona exactamente igual. 

## Configurando nuestro nuevo proyecto.

```
  $ cargo new guessing_game
  $ cd guessing_game
``` 

Como recordaremos, el primer comando es para crear los archivos iniciales para nuestro proyecto. El segundo comando es nada más para acceder a la carpeta del proyecto. Miremos un momento al archivo llamado *Cargo.toml*. 

```toml
  [package]
name = "guessing_game"
version = "0.1.0"
authors = ["Your Name <you@example.com>"]
edition = "2018"

[dependencies]
``` 

Para cualquier desarrollador será útil esta información por si quiere trabajar por encima de nuestro programa. Asegurate que si la información del autor que Cargo obtiene de tu entorno sea correcto, caso contrario, corrigelo y guarda los cambios. 

Si entramos en el `main.rs` encontraremos el “Hello, world!” que cargo creo por nosotros. 

```rust
   fn main() {
    println!("Hello, world!");
}
```

Siempre es una buena práctica correr nuestro programa para cerciorarnos de que nuestros archivos iniciales fueron creados apropiadamente.  

```
$ cargo run
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 1.50s
     Running `target/debug/guessing_game`
Hello, world!
```

El comando `run` basa su función en la necesidad que podemos tener de compilar y ejecutar rápidamente nuestro proyecto. 

## Procesar una Suposición

La primera parte en la que nos enfocaremos será de codificar la función para que el programa pueda aceptar números ingresado por el mismo usuario. Lo podemos hacer del siguiente modo: 

```rust
use std::io;

fn main() {
    println!("Guess the number!");

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```

Vale, seguramente estas viendo muchas cosas que te son atípicas. ¿Por qué entonces no lo estamos viendo hasta esta práctica? Pues porque son funciones que debemos conocer, pero no requiere de un estudio tan profundo ya que lo que nos atañe es aprender Holochain. Entonces lo veremos todo a lo largo de la práctica. 

Este código contiene bastante información, por lo que lo veremos linea por linea. Para obtener una entrada de datos y luego imprimir el resultado en nuestra terminal, necesitamos llamar a la librería `io` (input/output) en nuestro scope. La librería `io` proviene de la librería estándar de Rust (que es conocido como `std`.

```rust
    use std::io;
```   

Por defecto, Rust solo trae algunos tipos de librería en todos los programas para que inicialmente nuestro código solo disponga de lo que necesita. Necesitaremos traer `io` de forma explícita con la palabra clave `use`. 

Posteriormente viene la función `main` que tan bien conocemos ya. 

```rust
    fn main() {
```

Justo por debajo usamos el macro `println!` que nos permite imprimir los strings. 

```rust
    println!("Guess the number!");

    println!("Please input your guess.");
```

## Almacenando los valores en nuestra variable

Creamos un lugar donde almacenar los datos introducidos de este modo:

```rust
   let mut guess = String::new();
```

Como antes, estamos introduciendo una variable mutable llamada `guess` que contiene un resultado llamado `String::new` que es equivalente a tener un `String` vacío esperando a ser llenado por el usuario. Encontradas varios tipos de data capaz de aprovechar la funcion asociada `new`. 

Con `io` disponemos de una serie de funcionalidades como es el caso de `stdin` que nos permitira hacer lo que nos proponemos que es que el programa sea capaz de leer los datos que ingrese el usuario. La función está divida en dos partes; `.read_line()` que recibe nuestra variable `guess` y `.expect()` que atajara cualquier error que se pueda producir durante el ingreso de la información. Cuando introducimos métodos como los que son estos, es una buena práctica siempre hacerlo en la línea subsiguiente como esta en el código de arriba por razones de buenas prácticas. 

El trabajo de `read_line()` es tomar cualquier tipo de data del usuario introducido a través de cualquier input y colocarlo dentro del string como un argumento. El argumento del string necesita ser mutable para que el metodo pueda cambiar el contenido. 

El `&` indicada que el argumento es una referencia, la cual nos da un modo para permitir que múltiples partes de nuestro código pueda acceder al mismo pedazo de data sin tener que copiarlo múltiples veces dentro de la memoria. Presta atención a que lo estamos usando del modo `&mut guess` por lo que Rust permitirá que podamos modificar la variable origen desde la referencia. 

## Imprimiendo valores con el macro `println!`

Finalmente, tomaremos nuestro la variable `guess` que ya contendrá la data del usuario y la imprimimos. 

```rust
println!("You guessed: {}", guess);
```

## Testeando la primera parte

Ahora podemos probar la primera parte de nuestro programa. 

```
$ cargo run
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 6.44s
     Running `target/debug/guessing_game`
Guess the number!
Please input your guess.
6
You guessed: 6
``` 

## Usando Crate para disponer de más funcionalidades

Ya tenemos una buena parte funcional del programa, pero necesitamos la capacidad de generar números aleatorios. Esto lo haremos con crate y rand. Recordemos que un crate no es otra cosa que una colección de archivos con código fuente por parte de Rust. El crate `rand` es una librería crate, que contiene código creado para el uso de otros programas. 

El uso de Cargo de crates externos es cuando verdaderamente brilla. Antes de que podamos usar `rand` que es el crates que nos permitirá generar el número aleatorio. Para esto, necesitaremos modificar el archivo *Cargo.toml* para incluir `rand` como dependencia. Abriremos el archivo y agregaremos la siguiente línea en la parte inferior por debajo de `[dependencies]`. 

```
  [dependencies]
rand = "0.5.5"
```

En el archivo Cargo.toml, todo lo que sigue a un encabezado es parte de una sección que continúa hasta que comienza otra sección. La sección `[dependencies]` es donde le dice a Cargo de qué crates depende el proyecto y qué versiones de esos crates necesita. En este caso, especificaremos el crate `rand` con el especificador de versión semántica `0.5.5.` Cargo comprende el control de versiones semántico (a veces llamado SemVer), que es un estándar para escribir números de versión. El número `0.5.5` es en realidad una abreviatura de `^ 0.5.5`, que significa "cualquier versión que tenga una API pública compatible con la version 0.5.5.”.

Ahora, sin modificar nada, compilemos el proyecto. 

```rust
$ cargo build
    Updating crates.io index
  Downloaded rand v0.5.5
  Downloaded libc v0.2.62
  Downloaded rand_core v0.2.2
  Downloaded rand_core v0.3.1
  Downloaded rand_core v0.4.2
   Compiling rand_core v0.4.2
   Compiling libc v0.2.62
   Compiling rand_core v0.3.1
   Compiling rand_core v0.2.2
   Compiling rand v0.5.5
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 2.53s
```

Es posible que vea diferentes números de versión (¡pero todos serán compatibles con el código, gracias a SemVer!) y las líneas pueden estar en un orden diferente.

Ahora que tenemos una dependencia externa, Cargo obtiene las últimas versiones de todo desde el registro, que es una copia de los datos de Crates.io. Crates.io es donde las personas del ecosistema Rust publican sus proyectos Rust de código abierto para que otros los usen.

Después de actualizar el registro, Cargo verifica la sección `[dependencies]` y descarga los crates que aún no tiene. En este caso, aunque solo enumeramos rand como una dependencia, Cargo también tomó libc y rand_core, porque rand depende de que funcionen. Después de descargar los crates, Rust los compila y luego compila el proyecto con las dependencias disponibles.

Si vuelves a ejecutar inmediatamente `cargo build` sin realizar ningún cambio, no obtendrás ningún resultado aparte de la línea de acabado. Cargo sabe que ya ha descargado y compilado las dependencias, y no ha cambiado nada sobre ellas en su archivo Cargo.toml. Cargo también sabe que no ha cambiado nada sobre su código, por lo que tampoco lo vuelve a compilar. Sin nada que hacer, simplemente sale.

Cargo tiene un mecanismo que garantiza que pueda reconstruir el mismo artefacto cada vez que nosotros o cualquier otra persona compile el código: Cargo utilizará solo las versiones de las dependencias que especificamos hasta que se indique lo contrario. Por ejemplo, ¿qué sucede si la próxima semana la versión 0.5.6 del crate `rand` sale y contiene una corrección de error importante pero también contiene una regresión que romperá tu código?

La respuesta a este problema es el archivo Cargo.lock, que se creó la primera vez que ejecutamos `cargo build` y ahora está en su directorio de guessing_game. Cuando construye un proyecto por primera vez, Cargo determina todas las versiones de las dependencias que se ajustan a los criterios y luego las escribe en el archivo Cargo.lock. Cuando construya su proyecto en el futuro, Cargo verá que el archivo Cargo.lock existe y usará las versiones especificadas allí en lugar de hacer todo el trabajo de averiguar las versiones nuevamente. Esto le permite tener una compilación reproducible automáticamente. En otras palabras, su proyecto permanecerá en 0.5.5 hasta que actualice explícitamente, gracias al archivo Cargo.lock.

## Generando un numero aleatorio

Ahora que agregamos el `rand` crate a Cargo.toml, comenzaremos a usarlo. 

```rust
use std::io;
use rand::Rng;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_number);

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
``` 

Primero, agregamos la función que nos interesa que es `Rng` utilizando la palabra clave `use` y accediendo a la librería que importamos hace un momento. Esto se consigue escribiendo `use rand::Rng`. `Rng` define métodos que implementan el poder generar números aleatorios.

A continuación, agregamos dos lineas en el medio. Una es `rand::thread_rng` que es una función y nos dará un numero aleatorio particular que usaremos: uno que sea local al hilo de ejecución actual y sembrado por el sistema operativo. Luego llamamos al método `gen_range` en el generador de números aleatorios. Este método está definido por el rasgo `Rng` que trajimos al alcance con la declaración use` rand :: Rng`. El método `gen_range` toma dos números como argumentos y genera un número aleatorio entre ellos. Es inclusivo en el límite inferior pero exclusivo en el límite superior, por lo que debemos especificar "1" y "101" para solicitar un número entre "1" y "100".

> Nota: No solo sabrá qué características usar y qué métodos y funciones llamar desde un crate. Las instrucciones para usar un crate se encuentran en la documentación de cada libreria. Otra característica interesante de Cargo es que puede ejecutar `cargo doc --open command`, que creará la documentación proporcionada por todas sus dependencias localmente y la abrirá en el navegador. Si está interesado en otras funciones del crate `rand`, por ejemplo, ejecuta `cargo doc --open` y haz clic en `rand` en la barra lateral de la izquierda.

La segunda línea que agregamos a la mitad del código imprime el número secreto. Esto es útil mientras estamos desarrollando el programa para poder probarlo, pero lo eliminaremos de la versión final. ¡No es un gran juego si el programa imprime la respuesta tan pronto como comienza!
Intenta ejecutar el programa varias veces:

```
$ cargo run
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 2.53s
     Running `target/debug/guessing_game`
Guess the number!
The secret number is: 7
Please input your guess.
4
You guessed: 4

$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
     Running `target/debug/guessing_game`
Guess the number!
The secret number is: 83
Please input your guess.
5
You guessed: 5
``` 

Deberías obtener diferentes números aleatorios, y todos deberían ser números entre 1 y 100. 

¡Excelente trabajo!

> Nota: No solo sabrá qué características usar y qué métodos y funciones llamar desde un crate. Las instrucciones para usar un crate se encuentran en la documentación de cada libreria. Otra característica interesante de Cargo es que puede ejecutar `cargo doc --open command`, que creará la documentación proporcionada por todas sus dependencias localmente y la abrirá en el navegador. Si está interesado en otras funciones del crate `rand`, por ejemplo, ejecuta `cargo doc --open` y haz clic en `rand` en la barra lateral de la izquierda.

Ahora que tenemos la data ingresada por el usuario y el numero aleatorio, lo siguiente sera compararlos. Usaremos algo que debes de recordar muy bien de la ultima lección: `match`.

```rust
use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // --snip--

    println!("You guessed: {}", guess);

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("You win!"),
    }
}
```

El primer bit nuevo aquí es otra declaración `use`, que trae un tipo llamado `std :: cmp :: Ordering` al alcance de la biblioteca estándar. Al igual que `Result`, `Ordering` es otra enumeración, pero las variantes de `Ordering` son `Less`, `Greater` y `Equal`. Estos son los tres resultados que son posibles cuando compara dos valores.

Luego agregamos cinco líneas nuevas en la parte inferior que usan el tipo de pedido. El método`cmp` compara dos valores y se puede llamar a cualquier cosa que se pueda comparar. Toma una referencia a lo que sea que quieras comparar: aquí está comparando la suposición con el `secret_number`. Luego devuelve una variante de la enumeración `Ordering` que incluimos en el alcance con la declaración de uso. Usamos una expresión `match` para decidir qué hacer a continuación en función de qué variante de `Ordering` se devolvió desde la llamada a `cmp` con los valores `guess` y `secret_number`.

Una expresión de `match` se compone de brazos. Un brazo consta de un patrón y el código que debe ejecutarse si el valor dado al comienzo de la expresión `match` se ajusta al patrón de ese brazo. Rust toma el valor dado para igualar y mira a través del patrón de cada brazo por turno.

Veamos un ejemplo de lo que sucedería con la expresión de `match` utilizada aquí. Supongamos que el usuario ha adivinado `50` y el número secreto generado aleatoriamente esta vez es `38`. Cuando el código comparamos `50` con `38`, el método `cmp` devolverá `Ordering :: Greater`, porque `50` es mayor que `38`. La expresión `match` obtiene el valor `Ordering :: Greater` y comienza a comprobar el patrón de cada brazo. Observa el patrón del primer brazo, `Ordering :: Less`, y ve que el valor `Ordering :: Greater` no coincide con `Ordering :: Less`, por lo que ignora el código en ese brazo y pasa al siguiente brazo . El patrón del siguiente brazo, `Ordering::Greater`, coincide con  `Ordering::Greater!` El código asociado en ese brazo se ejecutará e imprimirá `Too big!` en la pantalla. La expresión `match` finaliza porque no es necesario mirar el último brazo en este escenario… 

```
$ cargo build
   Compiling libc v0.2.51
   Compiling rand_core v0.4.0
   Compiling rand_core v0.3.1
   Compiling rand v0.5.6
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
error[E0308]: mismatched types
  --> src/main.rs:22:21
   |
22 |     match guess.cmp(&secret_number) {
   |                     ^^^^^^^^^^^^^^ expected struct `std::string::String`, found integer
   |
   = note: expected reference `&std::string::String`
              found reference `&{integer}`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0308`.
error: could not compile `guessing_game`.

To learn more, run the command again with --verbose.
```

El núcleo del error indica que hay tipos que no coinciden. Rust tiene un sistema de tipo estático fuerte. Sin embargo, también tiene inferencia de tipos. Cuando escribimos `let mut guess = String :: new ()`, Rust puede inferir que `guess` debería ser un `String` y no nos obligó a escribir el tipo. El `secret_number`, por otro lado, es un tipo de número. Algunos tipos de números pueden tener un valor entre `1` y `100:i32`, un número de `32 bits`; `U32`, un número de 32 bits sin signo; `i64`, un número de `64 bits`; así como otros. Rust tiene por defecto un `i32`, que es el tipo de `secret_number` a menos que agregue información de tipo en otro lugar que haría que Rust infiera un tipo numérico diferente. La razón del error es que Rust no puede comparar una cadena y un tipo de número.

En última instancia, queremos convertir el `String` que el programa lee como entrada en un tipo de número real para poder compararlo numéricamente con el número secreto. Podemos hacerlo agregando otra línea al cuerpo de la función principal:

```rust
use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_number);

    println!("Please input your guess.");

    // --snip--

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let guess: u32 = guess.trim().parse().expect("Please type a number!");

    println!("You guessed: {}", guess);

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("You win!"),
    }
}
```
La línea es:

```rust
let guess: u32 = guess.trim().parse().expect("Please type a number!");
```

Creamos una variable llamada `guess`. Pero espera, ¿el programa no tiene ya una variable llamada `guess`? Lo hace, pero Rust nos permite *sombrear* el valor anterior de `guess` con uno nuevo. Esta función se utiliza a menudo en situaciones en las que desea convertir un valor de un tipo a otro. *Shadowing* nos permite reutilizar el nombre de la variable de `guess` en lugar de obligarnos a crear dos variables únicas, como `guess_str` y `guess`, por ejemplo.

Vinculamos `guess` a la expresión `guess.trim().parse()`. `guess` en la expresión se refiere al `guess` original que era un `String` con la entrada en ella. El método `trim` en una instancia de `String` eliminará cualquier espacio en blanco al principio y al final. Aunque `u32` puede contener solo caracteres numéricos, el usuario debe presionar enter para satisfacer `read_line`. Cuando el usuario presiona enter, se agrega un carácter de nueva línea a la cadena. Por ejemplo, si el usuario escribe `5` y presiona enter, `guess` se verá así: `5 \ n`. El `\ n` representa una "nueva línea", el resultado de presionar enter. El método `trim` elimina` \ n`, lo que da como resultado solo `5`.

El método `parse` convierte un string en algún tipo de número. Debido a que este método puede convertir una variedad de tipos de números, necesitamos decirle a Rust el tipo de número exacto que queremos usando `let guess: u32`. Los dos puntos (`:`) después de `guess` le indican a Rust que anotaremos el tipo de variable. Rust tiene algunos tipos de números integrados; el `u32` que se ve aquí es un entero de 32 bits sin signo. Es una buena opción predeterminada para un número positivo pequeño. Además, la anotación `u32` en este programa de ejemplo y la comparación con `secret_number` significa que Rust inferirá que `secret_number` debería ser un `u32` también. ¡Así que ahora la comparación será entre dos valores del mismo tipo!

La llamada a `parse` fácilmente podría causar un error. Si, por ejemplo, la cadena contuviera `A👍% `, no habría forma de convertir eso en un número. Debido a que puede fallar, el método `parse` devuelve un tipo `Result`, al igual que el método `read_line`. Trataremos este `Result` de la misma manera usando el método de espera nuevamente. Si `parse` devuelve una variante de `Result` de `Err` porque no pudo crear un número a partir de la cadena, la llamada de espera bloqueará el juego e imprimirá el mensaje que le damos. Si `parse` puede convertir con éxito la cadena en un número, devolverá la variante `Ok` de `Result`, y esperamos que devuelva el número que queremos del valor `Ok`.

```
$ cargo run
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 0.43s
     Running `target/debug/guessing_game`
Guess the number!
The secret number is: 58
Please input your guess.
  76
You guessed: 76
Too big!
``` 

Tenemos la mayor parte del juego funcionando ahora, pero el usuario solo puede hacer una suposición. ¡Cambiemos eso agregando un bucle!

## Permitir múltiples suposiciones con bucle

The `loop` keyword creates an infinite loop. We’ll add that now to give users more chances at guessing the number: La palabra clave `loop` crea un bucle infinito. Agregaremos eso ahora para brindar a los usuarios más oportunidades de adivinar el número:

```rust
   // --snip--

    println!("The secret number is: {}", secret_number);

    loop {
        println!("Please input your guess.");

        // --snip--

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => println!("You win!"),
        }
    }
}
``` 

Como podemos ver, hemos movido todo en un ciclo desde el mensaje de entrada de adivinanzas en adelante. Ahora hay un nuevo problema porque el programa está haciendo exactamente lo que le dijimos que hiciera: ¡solicita otra conjetura para siempre! ¡No parece que el usuario pueda salir!
El usuario siempre puede interrumpir el programa usando el atajo de teclado ctrl-c. Pero hay otra forma de escapar de este monstruo insaciable, como se menciona en la discusión de `parse`: si el usuario ingresa una respuesta que no sea un número, el programa se bloqueará. El usuario puede aprovechar eso para salir, como se muestra aquí:

```
$ cargo run
   Compiling guessing_game v0.1.0 (file:///projects/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 1.50s
     Running `target/debug/guessing_game`
Guess the number!
The secret number is: 59
Please input your guess.
45
You guessed: 45
Too small!
Please input your guess.
60
You guessed: 60
Too big!
Please input your guess.
59
You guessed: 59
You win!
Please input your guess.
quit
thread 'main' panicked at 'Please type a number!: ParseIntError { kind: InvalidDigit }', src/libcore/result.rs:999:5
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace.
``` 

## Salir después de una conjetura correcta.

Programemos el juego para que se cierre cuando el usuario gane agregando una declaración de interrupción: 
``` 
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
```

Agregar la línea de ruptura después de ganar hace que el programa salga del ciclo cuando el usuario adivina el número secreto correctamente. Salir del bucle también significa salir del programa, porque el bucle es la última parte de main.

## Manejo de entrada no válida

Para refinar aún más el comportamiento del juego, en lugar de bloquear el programa cuando el usuario ingresa un no numero, hagamos que el juego ignore un no número para que el usuario pueda seguir adivinando. Podemos hacer eso modificando la línea donde `guess` se convierte de un `String` a un `u32`

```rust
       // --snip--

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        // --snip--
```

¡Increíble! Con un pequeño ajuste final, terminaremos el juego de adivinanzas. Recuerde que el programa todavía está imprimiendo el número secreto. Eso funcionó bien para las pruebas, pero arruina el juego. ¡Eliminemos el `println!` que genera el número secreto!

```rust
use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
``` 

## Verdadero Camino

*La criatura cae a tus pies completamente derrotada.*

*Cuando llegaste a este sitio pensabas que no podrías cansarte, ni siquiera tenías necesidad de respirar, sin embargo, ahora te sientes totalmente agotado. Lo que te mantiene firme es la satisfacción de que has trabajado muy duro para lograr aquella hazaña que yacía frente a ti. Aun precavido, te giras en dirección al libro.*

*Le echas una mirada rápida pasando sus páginas, examinandolas. Estuviste a punto de comenzar tus estudios inmediatamente pero Cargó te detuvo.*

*«Creo que lo mejor que podemos hacer, héroe, es retirarnos a un lugar más seguro, preferiblemente más apartado.»*

*Lo meditas por un momento y posteriormente cierras el libro para ponerlo bajo la protección de tu brazo. Te dispones a abandonar el sitio.*

*«Ve con cuidado, no sabemos hay más de esas cosas.»*

*Te tomas tu tiempo para abrirte paso por el gran templo hasta la salida. No tuviste mayores problemas más allá de moverte de regreso por el laberinto.Ya en el exterior escuchas otra vez a Cargo*

*«Muy bien, iremos por ahí, luego buscaremos una cueva lo bastante escondida y entonces...»*

*─ No. ─Dijo Mozilla.─ Vamos a ir por aquí.*

*No espero por aprobación, camino prácticamente en la dirección contraria que Cargo había indicado. La luz y tú se miraron con expresiones perplejas, luego regresaron a la visión de Mozilla y antes de que se pudiera alejar más la siguieron.*


## En Resumen

Este proyecto fue una forma práctica de unificar todos los conceptos que hemos estudiado en Rust en pro de dar la última pincelada a los conocimientos que hemos desarrollados. Desde este momento en adelante nos enfocaremos expresamente en Holochain. 

