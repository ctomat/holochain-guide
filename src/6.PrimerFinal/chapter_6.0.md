# Primer Final

*La luz de El-Que-Todo-Lo-Controla parpadea, se debilita ante ti. Su luz vuelve a ser celeste y entonces se torna a un verde acorde a tus colores. Miras por el ventanal de la gran torre y ves como las luces de la ciudad cambian de igual modo. Percibes como el control de todo a tu alrededor es traspasado a ti.*

*La habitación queda en silencio.*

*«¡Lo hizo!»*

*En efecto, lo lograste. ¿Por qué entonces no te sentías satisfecho?*

*Porque aún había mucho trabajo que hacer.*

*«Lo sé, ahora habrá que regresar el orden en este lugar. Enseñarles la alternativa que eres. Bueno, el lado positivo es que este será un buen lugar para que sigas mejorando.»*

*Asientes.*

*Esperabas que aquello fuera el fin del camino, pero en realidad fue solo el principio.*

## Un Final

*Y con esto llegamos al primer final de la guia. Eventualmente me gustaría expandir el material según vayan saliendo actualizaciones de Holochain ahora que tenemos una base sólida de conceptos. Por esto mismo no le daré un punto y final a esta guia de momento, sino que marcare este “Primer Final”.*

*Ha sido un trabajo bastante extenso pero estoy feliz con él, y espero, que para aquel que esté leyendo estas palabras le haya sido de utilidad cada lección. Todavía queda mucho que aprender de Holochain y ver que alternativas no depara el futuro, en donde quizás podamos ver el internet y las conexiones de otro modo.*

*¡Gracias por leer!*
