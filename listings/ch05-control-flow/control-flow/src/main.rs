fn main() {
    conditional_if(6);
    conditional_elseif(8);
    conditional_iflet(false);
}

fn conditional_if(number:u8) {
    const NUMBER : u8 = 5;
    
    if number > NUMBER {
        println!("Oh good, it's so BIG!");
    }
    else {
        println!("Jaja, small pp");
    }
}

fn conditional_elseif(number:u8) {
    if number % 8 == 0 {
        println!("number is divisible by 8");
    }
    else if number % 6 == 0 {
        println!("number is divisible by 6");
    }
    else if number % 4 == 0 {
        println!("number is divisible by 4");
    }
    else if number % 2 == 0 {
        println!("number is divisible by 2");
    }
}

fn conditional_iflet(condition:bool) {
    let number = if condition {
        5
    }
    else {
        3
    };
    println!("The number is: {}", number); 
}
