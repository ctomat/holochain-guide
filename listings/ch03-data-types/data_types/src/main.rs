fn main() {
    tuple_function();
    
    months_function();
}

fn tuple_function() {
    let tup : (u32, u32, u32) = (31, 45, 57);
    println!("This is the tuple: {}, {}, {}", tup.0, tup.1, tup.2);
}

fn months_function() {
    let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
    println!("This is the array: {}", months[0]);
}