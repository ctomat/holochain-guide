fn main() {
    let s1 = String::from("Hello!");

    let len = calculate_length(&s1);

    println!("{} contaien {} chars", s1, len);
}

fn calculate_length(s2 : &String) -> usize {
    s2.len()
}
