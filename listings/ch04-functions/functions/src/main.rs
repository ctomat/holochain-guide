fn main() {
    another_function(5, 6);
    let x = five(5);
    println!("This is five: {}", x);
}

fn another_function(x:u8, y:u8) {
    println!("The numbers are: {}, {}",x, y);
}

fn five(y:u8) -> u8 {
    y + 1
}