fn main() {
    let mut x = 5;
    println!("The number is: {}", x);
    x = 6;
    println!("Now, the number is: {}", x);

    let y = 5;
    let y = y + 1;
    let y = y * 2;
    println!("And this is the shadowed variable: {}", y);

    let space = "     ";
    let space = space.len();
    println!("Finally, this is the space len: {}", space);
}
